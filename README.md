# README #

This web application was created in training purposes, for EPAM java training. 

### Guest Mode ###

When in guest mode, You can search for films by means of search bar. You can visit the Movies page where You can see the title and the current rating of the movies. Also the references to the exact movie pages are provided. You can visit other's user page if he or she has submitted a comment for the movie You are currently on the page of. 4 kinds of sorting movies are open for every visitor.

### User Mode ###

When in user mode, that means You're logged in. You are able to do everything You've been able in guest mode + You can submit marks and comments for the movies. The amount of comments You can place for an exact film is unlimited. As for marks. You can place only one. Surely You can delete everything You've placed. Every user is free to change his own password.

### Admin mode ###

When in admin mode, besides User mode abilities, You can delete the comments and the marks of other users, except other administrators. Moreover, You are free to change the status of others users. You can ban other user as well. The main feature of Admin mode is a privilege to add a new movie to the existing list.

### How do I get set up? ###

Import database from file movie_rating.sql and check file database.properties.
I used Java 7, remotely Tomcat 8.0.30, Locally - Tomcat 8.5.51. 
You're able to visit [movierating.me](http://movierating.me) where application is deployed.
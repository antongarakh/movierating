<%@tag description="Header Tag" pageEncoding="UTF-8" %>

<%@include file="/WEB-INF/jspf/imports.jspf" %>

<div class="row" ng-app="app">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Movie Rating</a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li><a href="${root}/controller?command=home"><fmt:message key="navbar.home"/></a></li>
                    <li><a href="${root}/controller?command=films"><fmt:message key="navbar.movies"/></a></li>
                    <li><a href="${root}/controller?command=forward&page=about"><fmt:message key="navbar.about"/></a>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <c:choose>
                        <c:when test="${empty user}">
                            <li><a href="${root}/controller?command=forward&page=registration">
                                <span class="glyphicon glyphicon-user"></span> <fmt:message key="navbar.sign-up"/></a>
                            </li>
                            <li><a href="${root}/controller?command=forward&page=login"><span
                                    class="glyphicon glyphicon-log-in"></span> <fmt:message key="navbar.login"/></a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <c:choose>
                                <c:when test="${user.isAdmin}">
                                    <li><a href="${root}/controller?command=admin&userContentType=ownPage"><span
                                            class="glyphicon glyphicon-user"></span> <fmt:message
                                            key="navbar.admin-page"/></a>
                                    </li>
                                    <li><a href="${root}/controller?command=logout"><span
                                            class="glyphicon glyphicon-log-out"></span> <fmt:message
                                            key="navbar.logout"/></a></li>
                                </c:when>
                                <c:otherwise>
                                    <li><a href="${root}/controller?command=user&userContentType=ownPage"><span
                                            class="glyphicon glyphicon-user"></span> <fmt:message
                                            key="navbar.user-page"/></a>
                                    </li>
                                    <li><a href="${root}/controller?command=logout"><span
                                            class="glyphicon glyphicon-log-out"></span><fmt:message
                                            key="navbar.logout"/></a></li>
                                </c:otherwise>
                            </c:choose>

                        </c:otherwise>
                    </c:choose>
                    <li>
                        <form action="controller" method="POST">
                            <input type="hidden" name="uri" value="${uri}"/>
                            <input type="hidden" name="command" value="refresh"/>
                            <div class="form-group">
                                <select name="locale" id="locale" class="form-control" onchange="submit()">
                                    <option value="en_US" ${locale == 'en_US' ? 'selected' : ''}>En</option>
                                    <option value="ru_RU" ${locale == 'ru_RU' ? 'selected' : ''}>Ru</option>
                                </select>
                            </div>
                        </form>
                    </li>

                    <li ng-controller="GarakhController">
                        <ui-select class="select-ui" ng-model="selected.value">
                            <ui-select-match id="search" placeholder="search...">
                                <span ng-bind="$select.selected.name"></span>
                            </ui-select-match>
                            <ui-select-choices refresh="funcAsync($select.search)" refresh-delay="0"
                                               repeat="item in (itemArray | filter: $select.search) track by item.id">
                                <a href="controller?command=film_page&id={{item.id}}" ng-bind="item.name"></a>
                            </ui-select-choices>
                        </ui-select>
                    </li>
                </ul>
            </div>
        </div>

    </nav>
</div>


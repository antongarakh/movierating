var module = angular.module('app', ['ui.select', 'ngSanitize']);

angular.module('app').controller('GarakhController', ['$scope', '$http', function ($scope, $http) {
    $scope.itemArray = [];

    $scope.funcAsync = function (query) {
        $http.get('/controller?command=search&term=' + query).then(function (response) {
            $scope.itemArray = [];
            angular.forEach(response.data, function (value, key) {
                $scope.itemArray.push({id: key, name: value});
            });
            console.log($scope.itemArray);
        }, function (err) {
            console.log(err.message);
        });
    };


}]);
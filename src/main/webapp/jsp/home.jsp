<%@ page contentType="text/html; charset=UTF-8" language="java" isELIgnored="false" pageEncoding="UTF-8" %>

<%@include file="/WEB-INF/jspf/imports.jspf" %>
<%@ taglib prefix="mytags" tagdir="/WEB-INF/tags" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Movie Rating</title>
    <mytags:styles/>
</head>
<body>
<div class="container">
    <div class="row">
        <mytags:navbar/>
    </div>
    <div class="row">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <div>
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>
            </div>
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <div class="carousel-caption">
                        <h3><fmt:message key="home.carousel.rate"/></h3>
                    </div>
                    <img src="../img/main1.jpg" alt="beautiful_mind">
                </div>
                <div class="item">
                    <div class="carousel-caption">
                        <h3><fmt:message key="home.carousel.read"/></h3>
                    </div>
                    <img src="../img/main2.jpg" alt="fight_club">
                </div>
                <div class="item">
                    <div class="carousel-caption">
                        <h3><fmt:message key="home.carousel.write"/></h3>
                    </div>
                    <img src="../img/main3.jpg" alt="pulp_fiction">
                </div>
            </div>
            <div>
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>


    </div>
</div>

<mytags:scripts/>


</body>
</html>
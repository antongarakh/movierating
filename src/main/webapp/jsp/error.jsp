<%@ page contentType="text/html; charset=UTF-8" language="java" isELIgnored="false" pageEncoding="UTF-8" %>
<%@include file="/WEB-INF/jspf/imports.jspf" %>
<%@ taglib prefix="mytags" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Movie Rating</title>
    <mytags:styles/>
</head>
<body>
<div class="container">
    <div class="row opacity-row">
        <div class="col-sm-8 col-sm-offset-2 ">
            <h2><fmt:message key="error.title"/></h2>
            <span class="huge-text">${errorCode}</span>
            <span>${errorMessage}</span>
            <c:if test="${not empty errorMessageKey}">
                <p class="errorPageMessage"><fmt:message key="${errorMessageKey}"/></p></c:if>
            <div><a href="${root}/controller?command=forward&page=home" class="btn btn-default"><fmt:message
                    key="login.button.home"/></a></div>
        </div>


    </div>
</div>
<mytags:scripts/>
</body>
</html>
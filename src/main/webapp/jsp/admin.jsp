<%@ page contentType="text/html; charset=UTF-8" language="java" isELIgnored="false" pageEncoding="UTF-8" %>
<%@ taglib prefix="mytags" tagdir="/WEB-INF/tags" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
    <title>Title</title>
</head>
<body>
<a href="${root}/controller?command=users" class="btn btn-info" role="button"><fmt:message
        key="admin.button.users"/></a>
<a href="${root}/controller?command=add_film" class="btn btn-info" role="button"><fmt:message
        key="admin.button.add"/></a>
</body>
</html>

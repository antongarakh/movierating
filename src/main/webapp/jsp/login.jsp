﻿<%@ page contentType="text/html; charset=UTF-8" language="java" isELIgnored="false" pageEncoding="UTF-8" %>

<%@ taglib prefix="mytags" tagdir="/WEB-INF/tags" %>
<%@include file="/WEB-INF/jspf/imports.jspf" %>

<!DOCTYPE html>
<html lang="en">
<head>

    <title>Movie Rating</title>
    <mytags:styles/>
</head>
<body>
<div class="page-header">
    <h1><strong>MOVIE RATING</strong></h1>
</div>
<div class="container">
    <div class="row">
        <div class="form-box">
            <div class="form-top">
                <span class="login-heading"><fmt:message key="login.form.title"/></span>
                <p><fmt:message key="login.form.subtitle"/></p>
                <c:if test="${not empty errorMessageKey}">
                    <p class="errorMessage"><fmt:message key="${errorMessageKey}"/></p></c:if>
                <c:if test="${not empty expirationDate}">
                    <p class="errorMessage">${expirationDate}</p></c:if>

            </div>
            <div class="form-bottom">
                <form action="${root}/controller" method="post" accept-charset="UTF-8" class="registration-form">
                    <input type="hidden" name="command" value="login">
                    <div class="form-group">
                        <label class="sr-only" for="formLogin"></label>
                        <input type="text" maxlength="16" name="formLogin" required="required"
                               placeholder="<fmt:message key="login.form.username"/>"
                               value="${fn:escapeXml(param.formLogin)}"
                               class="formLogin form-control"
                               id="formLogin">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="formPassword"></label>
                        <input type="password" name="formPassword" required="required"
                               value="${fn:escapeXml(param.formPassword)}"
                               pattern="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])\S{8,}$"
                               title="<fmt:message key="login.form.requirement"/>"
                               placeholder="<fmt:message key="login.form.password"/>"
                               class="formPassword form-control" id="formPassword">
                    </div>
                    <button type="submit" class="btn"><fmt:message key="login.button.login"/></button>
                    <a href="${root}/controller?command=forward&page=home" class="btn btn-default"><fmt:message
                            key="login.button.home"/></a>
                </form>
            </div>
        </div>
    </div>
</div>
<mytags:scripts/>
</body>
</html>
<%@ page contentType="text/html; charset=UTF-8" language="java" isELIgnored="false" pageEncoding="UTF-8" %>
<%@include file="/WEB-INF/jspf/imports.jspf" %>
<%@ taglib prefix="mytags" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Movie Rating</title>
    <mytags:styles/>
</head>
<body>

<c:set var="offset" scope="request" value="${usersOffset}"/>
<c:if test="${empty offset or offset < 0}">
    <c:set var="offset" value="0"/>
</c:if>

<c:set var="users" scope="request" value="${users}"/>
<c:set var="experienced_user" value="EXPERIENCED"/>
<c:set var="guru_user" value="GURU"/>


<div class="container">

    <div class="row">
        <mytags:navbar/>
    </div>

    <div class="row opacity-row">
        <div class="col-md-offset-2 col-md-8">
            <c:if test="${not empty success}">
                <h4>${success}</h4>
                <c:remove var="success" scope="session"/>
            </c:if>
            <table class="table table-striped ">

                <caption>
                    <h3><fmt:message key="users.title"/></h3>
                </caption>
                <thead class="text-center">
                <tr class="text-center">
                    <th class="text-center"><fmt:message key="users.table.user"/></th>
                    <th class="text-center"><fmt:message key="users.table.role"/></th>
                    <th class="text-center"><fmt:message key="users.table.status"/></th>
                    <th class="text-center"><fmt:message key="users.table.change_status"/></th>
                    <th class="text-center"><fmt:message key="users.table.ban_status"/></th>
                </tr>
                </thead>
                <tbody class="text-center">
                <c:forEach items="${users}" var="current">
                    <tr>
                        <td>
                            <a href="${root}/controller?command=user&userContentType=adminView&id=${current.id}"><c:out
                                    value="${current.username}"/></a>
                        </td>
                        <c:choose>
                            <c:when test="${current.isAdmin}">
                                <td><fmt:message key="user.table.admin"/></td>
                                <td>${current.status}</td>
                                <td><fmt:message key="users.table.cant_change"/></td>
                                <td><fmt:message key="users.table.cant_ban"/></td>
                            </c:when>
                            <c:otherwise>
                                <td><fmt:message key="user.table.user"/></td>
                                <td>${current.status}</td>
                                <td class="col-md-3">
                                    <c:choose>
                                        <c:when test="${current.status eq experienced_user}">
                                            <form action="${root}/controller" method="post" class="form-horizontal"
                                                  id="statusForm" role="form">
                                                <input type="hidden" name="command" value="change_status">
                                                <input type="hidden" name="id" value="${current.id}">
                                                <input type="hidden" name="kind" value="up">
                                                <input type="hidden" name="page" value="users">
                                                <button type="submit" class="btn btn-default" data-toggle="tooltip"
                                                        title=<fmt:message
                                                        key="users.button.upstatus"/>><span
                                                        class="glyphicon glyphicon-arrow-up"></span></button>
                                            </form>
                                            <form action="${root}/controller" method="post" class="form-horizontal"
                                                  id="statusForm" role="form">
                                                <input type="hidden" name="command" value="change_status">
                                                <input type="hidden" name="id" value="${current.id}">
                                                <input type="hidden" name="kind" value="down">
                                                <input type="hidden" name="page" value="users">
                                                <button type="submit" class="btn btn-default" data-toggle="tooltip"
                                                        title=<fmt:message
                                                        key="users.button.downstatus"/>><span
                                                        class="glyphicon glyphicon-arrow-down"></span></button>
                                            </form>
                                        </c:when>
                                        <c:otherwise>
                                            <c:choose>
                                                <c:when test="${current.status eq guru_user}">
                                                    <form action="${root}/controller" method="post"
                                                          class="form-horizontal" id="statusForm" role="form">
                                                        <input type="hidden" name="command" value="change_status">
                                                        <input type="hidden" name="id" value="${current.id}">
                                                        <input type="hidden" name="kind" value="down">
                                                        <input type="hidden" name="page" value="users">
                                                        <button type="submit" class="btn btn-default"
                                                                data-toggle="tooltip" title=<fmt:message
                                                                key="users.button.downstatus"/>><span
                                                                class="glyphicon glyphicon-arrow-down"></span></button>
                                                    </form>
                                                </c:when>
                                                <c:otherwise>
                                                    <form action="${root}/controller" method="post"
                                                          class="form-horizontal" id="statusForm" role="form">
                                                        <input type="hidden" name="command" value="change_status">
                                                        <input type="hidden" name="id" value="${current.id}">
                                                        <input type="hidden" name="kind" value="up">
                                                        <input type="hidden" name="page" value="users">
                                                        <button type="submit" class="btn btn-default"
                                                                data-toggle="tooltip" title=<fmt:message
                                                                key="users.button.upstatus"/>><span
                                                                class="glyphicon glyphicon-arrow-up"></span></button>
                                                    </form>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td>
                                    <c:choose>
                                        <c:when test="${not empty current.expirationDate}">
                                            <a href="${root}/controller?command=ban&kind=reset&page=users&id=${current.id}"
                                               class="btn btn-default"><span><fmt:message
                                                    key="users.button.reset_ban"/></span></a>
                                        </c:when>
                                        <c:otherwise>
                                            <div class="dropdown">
                                                <a class="btn btn-default dropdown-toggle" type="button" id="menu1"
                                                   data-toggle="dropdown"><fmt:message key="users.select.title"/>
                                                    <span class="caret"></span></a>
                                                <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                                    <li role="presentation"><a role="menuitem" tabindex="-1"
                                                                               href="${root}/controller?command=ban&kind=set&page=users&days=1&id=${current.id}">
                                                        <fmt:message key="users.select.day"/></a></li>
                                                    <li role="presentation"><a role="menuitem" tabindex="-1"
                                                                               href="${root}/controller?command=ban&kind=set&page=users&days=7&id=${current.id}">
                                                        <fmt:message key="users.select.week"/></a></li>
                                                    <li role="presentation"><a role="menuitem" tabindex="-1"
                                                                               href="${root}/controller?command=ban&kind=set&page=users&days=30&id=${current.id}">
                                                        <fmt:message key="users.select.month"/></a></li>
                                                </ul>
                                            </div>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                            </c:otherwise>
                        </c:choose>


                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

    </div>

    <div class="row opacity-row" align="center">
        <ul class="pagination">
            <c:if test="${offset - 1 >= 0}">
                <li><a href="${root}/controller?command=users&usersOffset=${offset - 1}"><<</a>
                </li>
            </c:if>
            <li class="disabled"><a>${offset}</a></li>
            <c:if test="${users.size()==10}">
                <li><a href="${root}/controller?command=users&usersOffset=${offset + 1}">>></a>
                </li>
            </c:if>
        </ul>
    </div>

</div>
<mytags:scripts/>
</body>
</html>
<%@ page contentType="text/html; charset=UTF-8" language="java" isELIgnored="false" pageEncoding="UTF-8" %>
<%@include file="/WEB-INF/jspf/imports.jspf" %>
<%@ taglib prefix="mytags" tagdir="/WEB-INF/tags" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Movie Rating</title>
    <mytags:styles/>
</head>
<body>

<c:set var="userContentType" scope="request" value="${userContentType}"/>
<c:if test="${empty userContentType}">
    <c:set var="userContentType" scope="session" value="${userContentType}"/>
</c:if>
<c:choose>
    <c:when test="${(not empty otherUser) && (otherUser.id != user.id)}">
        <c:set var="username" scope="request" value="${otherUser.username}"/>
        <c:set var="status" scope="request" value="${otherUser.status}"/>
        <c:set var="isAdmin" scope="request" value="${otherUser.isAdmin}"/>
    </c:when>
    <c:otherwise>
        <c:if test="${otherUser.id eq user.id}">
            <c:set var="userContentType" scope="request" value="ownPage"/>
        </c:if>
        <c:set var="username" scope="request" value="${user.username}"/>
        <c:set var="status" scope="request" value="${user.status}"/>
        <c:set var="isAdmin" scope="request" value="${user.isAdmin}"/>

    </c:otherwise>
</c:choose>
<c:set var="list" scope="request" value="${commentsFilms}"/>
<c:set var="password" scope="request" value="${user.password}"/>
<c:set var="mark_list" scope="request" value="${marksFilms}"/>
<c:set var="ownPage" value="ownPage"/>
<c:set var="adminView" value="adminView"/>
<c:set var="experienced_user" value="EXPERIENCED"/>
<c:set var="guru_user" value="GURU"/>

<div class="container">

    <div class="row">
        <mytags:navbar/>
    </div>

    <div class="row opacity-row">
        <div class="col-sm-8 col-sm-offset-2 ">
            <c:if test="${not empty success}">
                <h4><fmt:message key="${success}"/></h4>
                <c:remove var="success" scope="session"/>
            </c:if>

            <h2><fmt:message key="user.title"/> <c:out value="${username}"/></h2>

            <c:choose>
                <c:when test="${userContentType eq ownPage}">
                    <a href="#edit-form" id="to-edit-form" class="btn btn-info" role="button"><fmt:message
                            key="user.button.edit"/></a>
                </c:when>
            </c:choose>

            <c:if test="${isAdmin && userContentType eq ownPage}">
                <%@include file="/jsp/admin.jsp" %>
            </c:if>

            <table class="table table-responsive">
                <tbody>
                <tr>
                    <td class="user-info-key"><fmt:message key="user.table.status"/></td>
                    <td>${status}</td>
                    <td></td>
                </tr>
                <tr>
                    <td class="user-info-key"><fmt:message key="user.table.role"/></td>
                    <td>
                        <c:choose>
                            <c:when test="${isAdmin}">
                                <fmt:message key="user.table.admin"/>
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="user.table.user"/>
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td></td>
                </tr>
                <c:choose>
                    <c:when test="${list.size() > 0}">
                        <tr>
                            <td class="user-info-key"><fmt:message key="user.table.comments"/></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <c:forEach items="${list}" var="current">
                            <tr>
                                <td><fmt:message key="user.table.movie"/> <c:out value="${current.filmName}"/></td>
                                <td><fmt:message key="user.table.comment"/><c:out value="${current.comment}"/></td>
                                <td><c:choose>
                                    <c:when test="${(userContentType eq ownPage) || ((userContentType eq adminView) && (!otherUser.isAdmin)) }">
                                        <a href="${root}/controller?command=delete_comment&id=${current.commentId}&userId=${current.userId}"
                                           class="btn btn-info"
                                           role="button"><fmt:message key="user.button.delete"/></a>
                                    </c:when>
                                </c:choose>
                                </td>


                            </tr>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        <tr>
                            <td><fmt:message key="user.table.nocomments"/></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </c:otherwise>
                </c:choose>

                <c:choose>
                    <c:when test="${mark_list.size() > 0}">
                        <tr>
                            <td class="user-info-key"><fmt:message key="user.table.marks"/></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <c:forEach items="${mark_list}" var="current">
                            <tr>
                                <td><fmt:message key="user.table.movie"/> <c:out value="${current.filmName}"/></td>
                                <td><fmt:message key="user.table.mark"/> ${current.mark}</td>
                                <td><c:choose>
                                    <c:when test="${((userContentType eq ownPage) || (userContentType eq adminView ) && (otherUser.isAdmin != user.isAdmin))}">
                                        <c:choose>
                                            <c:when test="${not empty otherUser}">
                                                <a href="${root}/controller?command=delete_mark&filmId=${current.filmId}&userId=${otherUser.id}"
                                                   class="btn btn-info"
                                                   role="button"><fmt:message key="user.button.delete"/></a>
                                            </c:when>
                                            <c:otherwise>
                                                <a href="${root}/controller?command=delete_mark&filmId=${current.filmId}&userId=${user.id}"
                                                   class="btn btn-info"
                                                   role="button"><fmt:message key="user.button.delete"/></a>
                                            </c:otherwise>
                                        </c:choose>

                                    </c:when>
                                </c:choose>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        <tr>
                            <td><fmt:message key="user.table.nomarks"/></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </c:otherwise>
                </c:choose>
                </tbody>
            </table>
        </div>
    </div>


    <c:choose>
        <c:when test="${userContentType eq ownPage}">
            <div class="row opacity-row">
                <div class="col-sm-8 col-sm-offset-2 ">
                    <h3><fmt:message key="user.form.title"/></h3>

                    <form action="${root}/controller" method="post" class="form-horizontal" id="edit-form" role="form">
                        <input type="hidden" name="command" value="edit_profile">
                        <div class="form-group">
                            <label class="col-md-3 control-label"><fmt:message key="user.form.old_password"/></label>
                            <div class="col-md-8">
                                <input name="editOldPassword" class="form-control" type="password"
                                       value="${fn:escapeXml(param.editOldPassword)}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><fmt:message key="user.form.password"/></label>
                            <div class="col-md-8">
                                <input name="editPassword" class="form-control" required="required"
                                       pattern="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])\S{8,}$"
                                       value="${fn:escapeXml(param.editPassword)}"
                                       title="<fmt:message key="login.form.requirement"/>" type="password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><fmt:message key="user.form.confirm"/></label>
                            <div class="col-md-8">
                                <input name="editConfirmPassword" class="form-control" required="required"
                                       pattern="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])\S{8,}$"
                                       value="${fn:escapeXml(param.editConfirmPassword)}"
                                       title="<fmt:message key="login.form.requirement"/>" type="password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"></label>
                            <div class="col-md-8">
                                <button type="submit" class="btn btn-primary"><fmt:message
                                        key="user.button.save"/></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </c:when>
    </c:choose>

    <c:if test="${not empty user && user.isAdmin && not (userContentType eq ownPage)}">
        <div class="row opacity-row">
            <div class="col-md-offset-2 col-md-8">
                <table class="table table-striped ">
                    <caption>
                        <h3><fmt:message key="user.subtitle"/></h3>
                    </caption>
                    <thead class="text-center">
                    <tr class="text-center">
                        <th class="text-center"><fmt:message key="users.table.change_status"/></th>
                        <th class="text-center"><fmt:message key="users.table.ban_status"/></th>
                    </tr>
                    </thead>
                    <tbody class="text-center">
                    <tr>
                        <c:choose>
                            <c:when test="${otherUser.isAdmin}">
                                <td><fmt:message key="users.table.cant_change"/></td>
                                <td><fmt:message key="users.table.cant_ban"/></td>
                            </c:when>
                            <c:otherwise>
                                <td class="col-md-3">

                                    <c:choose>
                                        <c:when test="${otherUser.status eq experienced_user}">
                                            <form action="${root}/controller" method="post" class="form-horizontal"
                                                  id="statusForm" role="form">
                                                <input type="hidden" name="command" value="change_status">
                                                <input type="hidden" name="id" value="${otherUser.id}">
                                                <input type="hidden" name="kind" value="up">
                                                <input type="hidden" name="page" value="user">
                                                <button type="submit" class="btn btn-default" data-toggle="tooltip"
                                                        title=<fmt:message
                                                        key="users.button.upstatus"/>><span
                                                        class="glyphicon glyphicon-arrow-up"></span></button>
                                            </form>
                                            <form action="${root}/controller" method="post" class="form-horizontal"
                                                  id="statusForm" role="form">
                                                <input type="hidden" name="command" value="change_status">
                                                <input type="hidden" name="id" value="${otherUser.id}">
                                                <input type="hidden" name="kind" value="down">
                                                <input type="hidden" name="page" value="user">
                                                <button type="submit" class="btn btn-default" data-toggle="tooltip"
                                                        title=<fmt:message
                                                        key="users.button.downstatus"/>><span
                                                        class="glyphicon glyphicon-arrow-down"></span></button>
                                            </form>
                                        </c:when>
                                        <c:otherwise>
                                            <c:choose>
                                                <c:when test="${otherUser.status eq guru_user}">
                                                    <form action="${root}/controller" method="post"
                                                          class="form-horizontal" id="statusForm" role="form">
                                                        <input type="hidden" name="command" value="change_status">
                                                        <input type="hidden" name="id" value="${otherUser.id}">
                                                        <input type="hidden" name="kind" value="down">
                                                        <input type="hidden" name="page" value="user">
                                                        <button type="submit" class="btn btn-default"
                                                                data-toggle="tooltip" title=<fmt:message
                                                                key="users.button.downstatus"/>><span
                                                                class="glyphicon glyphicon-arrow-down"></span></button>
                                                    </form>
                                                </c:when>
                                                <c:otherwise>
                                                    <form action="${root}/controller" method="post"
                                                          class="form-horizontal" id="statusForm" role="form">
                                                        <input type="hidden" name="command" value="change_status">
                                                        <input type="hidden" name="id" value="${otherUser.id}">
                                                        <input type="hidden" name="kind" value="up">
                                                        <input type="hidden" name="page" value="user">
                                                        <button type="submit" class="btn btn-default"
                                                                data-toggle="tooltip" title=<fmt:message
                                                                key="users.button.upstatus"/>><span
                                                                class="glyphicon glyphicon-arrow-up"></span></button>
                                                    </form>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="col-md-3">
                                    <c:choose>
                                        <c:when test="${not empty otherUser.expirationDate}">
                                            <a href="${root}/controller?command=ban&kind=reset&page=user&id=${otherUser.id}"
                                               class="btn btn-default"><span><fmt:message
                                                    key="users.button.reset_ban"/></span></a>
                                        </c:when>
                                        <c:otherwise>
                                            <div class="dropdown">
                                                <a class="btn btn-default dropdown-toggle" type="button" id="menu1"
                                                   data-toggle="dropdown"><fmt:message key="users.select.title"/>
                                                    <span class="caret"></span></a>
                                                <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                                    <li role="presentation"><a role="menuitem"
                                                                               href="${root}/controller?command=ban&kind=set&page=user&days=1&id=${otherUser.id}">
                                                        <fmt:message key="users.select.day"/></a></li>
                                                    <li role="presentation"><a role="menuitem"
                                                                               href="${root}/controller?command=ban&kind=set&page=user&days=7&id=${otherUser.id}">
                                                        <fmt:message key="users.select.week"/>
                                                    </a></li>
                                                    <li role="presentation"><a role="menuitem"
                                                                               href="${root}/controller?command=ban&kind=set&page=user&days=30&id=${otherUser.id}">
                                                        <fmt:message key="users.select.month"/>
                                                    </a></li>
                                                </ul>
                                            </div>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                            </c:otherwise>
                        </c:choose>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </c:if>
</div>
<mytags:scripts/>
</body>
</html>

﻿<%@ page contentType="text/html; charset=UTF-8" language="java" isELIgnored="false" pageEncoding="UTF-8" %>

<%@include file="/WEB-INF/jspf/imports.jspf" %>
<%@ taglib prefix="mytags" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>

    <title>Movie Rating</title>
    <mytags:styles/>
</head>
<body>
<div class="page-header">
    <h1><strong>MOVIE RATING</strong></h1>
</div>
<div class="container">
    <div class="row">
        <div class="form-box">
            <div class="form-top">
                <span class="sing-up-heading"><fmt:message key="signup.form.title"/></span>
                <p><fmt:message key="signup.form.subtitle"/></p>
                <c:if test="${not empty errorMessageKey}">
                <p class="errorMessage"><fmt:message key="${errorMessageKey}"/></c:if></p>
            </div>
            <div class="form-bottom">

                <form action="${root}/controller" accept-charset="UTF-8" method="post" class="registration-form">
                    <input type="hidden" maxlength="16" name="command" value="register">
                    <div class="form-group">
                        <label class="sr-only" for="formLogin"></label>
                        <input type="text" name="formLogin" required="required"
                               placeholder="<fmt:message key="signup.form.username"/>"
                               class="formLogin form-control"
                               value="${fn:escapeXml(param.formLogin)}"
                               id="formLogin">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="formPassword"></label>
                        <input type="password"  name="formPassword" required="required"
                               pattern="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])\S{8,}$"
                               value="${fn:escapeXml(param.formPassword)}"
                               title="<fmt:message key="login.form.requirement"/>"
                               placeholder="<fmt:message key="signup.form.password"/>"
                               class="formPassword form-control" id="formPassword">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="formPassword"></label>
                        <input type="password" required="required" name="confirmFormPassword"
                               pattern="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])\S{8,}$"
                               title="<fmt:message key="login.form.requirement"/>"
                               value="${fn:escapeXml(param.confirmFormPassword)}"
                               placeholder="<fmt:message key="signup.form.confirmation"/>"
                               class="formPassword form-control" id="confirmFormPassword">
                    </div>
                    <button type="submit" class="btn login-btn"><fmt:message key="signup.button.signup"/></button>
                    <a href="${root}/controller?command=forward&page=home" class="btn btn-default"><fmt:message
                            key="signup.button.home"/></a>
                </form>
            </div>
        </div>
    </div>
</div>
<mytags:scripts/>
</body>
</html>
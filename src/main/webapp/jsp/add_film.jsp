﻿<%@include file="/WEB-INF/jspf/imports.jspf" %>
<%@ taglib prefix="mytags" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html>
<head>
    <title>Movie Rating</title>
    <mytags:styles/>
</head>
<body>

<c:set var="genres" scope="request" value="${allGenres}"/>
<c:set var="countries" scope="request" value="${allCountries}"/>
<c:set var="years" scope="request" value="${allYears}"/>
<c:set var="success_message" scope="request" value="${success}"/>

<div class="container">

    <div class="row">
        <mytags:navbar/>
    </div>

    <div class="row opacity-row">
        <div class="col-sm-8 col-sm-offset-2 ">
            <c:if test="${not empty success}">
                <h4><fmt:message key="${success}"/></h4>
                <c:remove var="success" scope="session"/>
            </c:if>
            <h3><fmt:message key="add_film.title"/></h3>

            <form action="${root}/uploader" method="post" class="form-horizontal" id="add-film"
                  enctype="multipart/form-data">

                <div class="form-group">
                    <label class="col-md-3 control-label"><fmt:message key="add_film.form.name"/></label>
                    <div class="col-md-8">
                        <input id="username-input" maxlength="45" name="addName" required="required" title="
                        <fmt:message key="add_film.form.fill"/>"
                               class="form-control" type="text"
                               value="${fn:escapeXml(param.addName)}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"><fmt:message key="add_film.form.duration"/></label>
                    <div class="col-md-8">
                        <input name="addDuration"  class="form-control" type="text" title=
                                "<fmt:message key="add_film.duration.placeholder"/>"
                               required="required" pattern="\d+"
                               value="${fn:escapeXml(param.addDuration)}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"><fmt:message key="add_film.form.description"/></label>
                    <div class="col-md-8">
                        <input name="addDescription" maxlength="1000" class="form-control" required="required" title=
                                "<fmt:message key="add_film.form.fill"/>"
                               type="text" value="${fn:escapeXml(param.addDescription)}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"><fmt:message key="add_film.form.year"/></label>
                    <div class="col-md-8">
                        <select required name="addReleaseYear" title="Fill in" class="form-control">
                            <option><fmt:message key="add_film.select.year"/></option>
                            <c:forEach items="${years}" var="current">
                                <option value="${current}">${current}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label"><fmt:message key="add_film.form.genres"/></label>
                    <div class="col-md-8">
                        <select name="addGenres" required class="form-control chosen" multiple>
                            <c:forEach items="${genres}" var="current">
                                <option value=${current.id}>${current.name}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label"><fmt:message key="add_film.form.countries"/></label>
                    <div class="col-md-8">
                        <select name="addCountries" required class="form-control chosen" multiple>
                            <c:forEach items="${countries}" var="current">
                                <option value=${current.countryCode}>${current.countryName}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <label for="selphoto" class="col-sm-3 control-label"><fmt:message key="add_film.form.file"/></label>
                    <div class="col-sm-8">
                        <input type="file" required="required" class="inputstl" id="selphoto" name="sentFile">
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label"></label>
                    <div class="col-md-8">
                        <button type="submit" title="Fill in all the fields appropriately" class="btn btn-default">
                            <fmt:message key="add_film.button.add"/>
                        </button>
                    </div>
                </div>


            </form>

        </div>
    </div>

</div>

<mytags:scripts/>
</body>
</html>
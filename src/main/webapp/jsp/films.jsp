<%@ page contentType="text/html; charset=UTF-8" language="java" isELIgnored="false" pageEncoding="UTF-8" %>
<%@include file="/WEB-INF/jspf/imports.jspf" %>
<%@ taglib prefix="mytags" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Movie Rating</title>
    <mytags:styles/>
</head>
<body>


<c:set var="films" scope="request" value="${films}"/>
<c:set var="offset" scope="request" value="${filmsOffset}"/>
<c:if test="${empty offset or offset < 0}">
    <c:set var="offset" value="0"/>
</c:if>

<div class="container">

    <div class="row">
        <mytags:navbar/>
    </div>

    <div class="row opacity-row">

        <div class="col-md-offset-3 col-md-7">
            <table class="table table-striped ">

                <h3 class="movies-header"><fmt:message key="movies.title"/></h3>
                <div class="dropdown movies-sort">
                    <a class="btn btn-default dropdown-toggle" type="button" id="menu2"
                       data-toggle="dropdown"><fmt:message key="movies.sort.title"/>
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="menu2">
                        <li role="presentation"><a role="menuitem" tabindex="-1"
                                                   href="${root}/controller?command=films&sort=name">
                            <fmt:message key="movies.sort.name"/></a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1"
                                                   href="${root}/controller?command=films&sort=year">
                            <fmt:message key="movies.sort.year"/></a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1"
                                                   href="${root}/controller?command=films&sort=rating">
                            <fmt:message key="movies.sort.rating"/></a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1"
                                                   href="${root}/controller?command=films&sort=duration">
                            <fmt:message key="movies.sort.duration"/></a></li>
                    </ul>
                </div>
                <thead class="text-center">
                <tr>
                    <th><fmt:message key="movies.movie"/></th>
                    <th><fmt:message key="movies.rating"/></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${films}" var="current">
                    <tr>
                        <td><a href="${root}/controller?command=film_page&id=${current.id}"><c:out
                                value="${current.name}"/></a></td>
                        <td><fmt:formatNumber type="number"
                                              maxFractionDigits="1" value="${current.rating}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

    </div>

    <div class="row opacity-row" align="center">
        <ul class="pagination">
            <c:if test="${offset - 1 >= 0}">
                <li><a href="${root}/controller?command=films&filmsOffset=${offset - 1}"><<</a>
                </li>
            </c:if>
            <li class="disabled"><a>${offset}</a></li>
            <c:if test="${films.size()==10}">
                <li><a href="${root}/controller?command=films&filmsOffset=${offset + 1}">>></a>
                </li>
            </c:if>
        </ul>
    </div>

</div>
<mytags:scripts/>
</body>
</html>

﻿<%@include file="/WEB-INF/jspf/imports.jspf" %>
<%@ taglib prefix="mytags" tagdir="/WEB-INF/tags" %>

<c:choose>
    <c:when test="${not empty user}">
        <c:set var="isAdmin" value="${user.isAdmin}"/>
        <c:choose>
            <c:when test="${isAdmin}">
                <c:set var="userContentType" scope="session" value="adminView"/>
            </c:when>
            <c:otherwise>
                <c:set var="userContentType" scope="session" value="userView"/>
            </c:otherwise>
        </c:choose>
    </c:when>
    <c:otherwise>
        <c:set var="userContentType" scope="session" value="userView"/>
    </c:otherwise>
</c:choose>


<!DOCTYPE html>
<html>
<head>
    <title>Movie Rating</title>
    <mytags:styles/>
</head>
<body>

<div class="container">

    <div class="row">
        <mytags:navbar/>
    </div>

    <div class="row opacity-row">
        <div class="col-sm-8 col-sm-offset-2 ">
            <c:if test="${not empty success}">
                <h4><fmt:message key="${success}"/></h4>
                <c:remove var="success" scope="session"/>
            </c:if>
            <h1 class="text-center"><c:out value="${film.name}"/></h1>
            <img src="${film.url}" class="img-responsive center-block"
                 alt="${film.url}">
            <p><c:out value="${film.description}"/></p>

            <table class="table table-striped ">
                <tbody>
                <tr>
                    <td class="user-info-key"><fmt:message key="movie.table.duration"/></td>
                    <td><c:out value="${film.duration}"/> <fmt:message key="movie.table.minutes"/></td>
                </tr>
                <tr>
                    <td class="user-info-key"><fmt:message key="movie.table.year"/></td>
                    <td><c:out value="${film.releaseYear}"/></td>
                </tr>
                <tr>
                    <td class="user-info-key"><fmt:message key="movie.table.rating"/></td>
                    <td><fmt:formatNumber type="number"
                                          maxFractionDigits="1" value="${film.rating}"/></td>
                </tr>
                <tr>
                    <td class="user-info-key"><fmt:message key="movie.table.genres"/></td>
                    <td>
                        <c:forEach items="${genres}" var="current">
                            <span class="label label-info"> ${current.name}</span>
                        </c:forEach>
                    </td>
                </tr>
                <tr>
                    <td class="user-info-key"><fmt:message key="movie.table.countries"/></td>
                    <td>
                        <c:forEach items="${countries}" var="current">
                            <span class="label label-info"> ${current.countryName}</span>
                        </c:forEach>
                    </td>
                </tr>
                </tbody>
            </table>


        </div>

    </div>
    <c:choose>
        <c:when test="${not empty user}">
            <div class="row opacity-row">
                <form action="${root}/controller" id="comment-form"
                      method="post">
                    <input type="hidden"  maxlength="1000" name="command" value="submit_comment">
                    <div class="form-group col-sm-5 col-sm-offset-2 ">
                        <label for="comment"><fmt:message key="movie.comment.title"/></label>
                        <textarea required="required" name="formSubmitComment"
                                  form="comment-form" class="form-control"
                                  id="comment">${fn:escapeXml(param.formSubmitComment)}</textarea>
                        <button type="submit" class="btn btn-default"><fmt:message key="movie.button.comment"/></button>
                    </div>
                </form>
                <c:choose>
                    <c:when test="${not empty mark}">
                        <div class="form-group col-sm-3">
                            <label for="sel2"><fmt:message key="movie.mark.title"/></label>
                            <select id="sel2" class="form-control" disabled
                                    title=<fmt:message key="movie.mark.already"/>></select>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <form action="${root}/controller" method="post">
                            <input type="hidden" name="command" value="submit_mark">
                            <div class="form-group col-sm-3">
                                <label for="sel1"><fmt:message key="movie.mark.title"/></label>
                                <select required name="formSubmitMark" class="form-control" id="sel1">
                                    <option value="" disabled selected>Select your option</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                    <option>6</option>
                                    <option>7</option>
                                    <option>8</option>
                                    <option>9</option>
                                    <option>10</option>
                                </select>


                                <button type="submit" class="btn btn-default "><fmt:message
                                        key="movie.button.mark"/></button>
                            </div>
                        </form>
                    </c:otherwise>
                </c:choose>


            </div>
        </c:when>
    </c:choose>
    <c:choose>
        <c:when test="${not empty comments}">
            <div class="row opacity-row">
                <div class="col-sm-8 col-sm-offset-2">
                    <table class="table table-striped ">
                        <caption>
                            <h3><fmt:message key="movie.comments.title"/></h3>
                        </caption>
                        <tbody>
                        <c:forEach items="${comments}" var="current">
                            <tr>
                                <td class="user-info-key">
                                    <a href="${root}/controller?command=user&userContentType=${userContentType}&id=${current.userId}">${current.userName}</a>
                                </td>
                                <td><c:out value="${current.comment}"/></td>
                                <c:if test="${user.id eq current.userId}">
                                    ${userContentType = ownPage}
                                </c:if>
                                <td><c:choose>
                                    <c:when test="${(userContentType eq ownPage) || (user.isAdmin && !current.isUserAdmin)  }">
                                        <a href="${root}/controller?command=delete_comment&id=${current.commentId}&filmId=${film.id}"
                                           class="btn btn-info"
                                           role="button"><fmt:message key="movie.button.delete"/></a>
                                    </c:when>
                                </c:choose>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>


                </div>

            </div>
        </c:when>
    </c:choose>
</div>
<mytags:scripts/>
</body>
</html>

<%@ page contentType="text/html; charset=UTF-8" language="java" isELIgnored="false" pageEncoding="UTF-8" %>
<%@ taglib prefix="mytags" tagdir="/WEB-INF/tags" %>
<%@include file="/WEB-INF/jspf/imports.jspf" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Movie Rating</title>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <mytags:styles/>
</head>
<body>
<div class="container">
    <div class="row">
        <mytags:navbar/>
    </div>
    <div class="row opacity-row">
        <div class="col-sm-8 col-sm-offset-2 ">
            <h2><fmt:message key="about.title"/></h2>
            <p><fmt:message key="about.text.intro"/>
            <h4><fmt:message key="about.title.guest"/></h4>
            <fmt:message key="about.text.guest"/>
            <h4><fmt:message key="about.title.user"/></h4>
            <fmt:message key="about.text.user"/>
            <h4><fmt:message key="about.title.admin"/></h4>
            <fmt:message key="about.text.admin"/>
            </p>
        </div>
    </div>
</div>

<mytags:scripts/>
</body>
</html>
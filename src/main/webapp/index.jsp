<%@ page contentType="text/html; charset=UTF-8" language="java" isELIgnored="false" pageEncoding="UTF-8" %>

<%@include file="/WEB-INF/jspf/imports.jspf" %>

<html>
<head>
    <title>Index</title>
    <link rel="icon" href="../favicon.ico"/>
</head>
<body>
<jsp:forward page="/jsp/home.jsp"/>
</body>
</html>
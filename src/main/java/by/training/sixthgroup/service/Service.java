package by.training.sixthgroup.service;

import by.training.sixthgroup.constant.*;
import by.training.sixthgroup.controller.SessionRequestContent;
import by.training.sixthgroup.exception.RequestParameterException;
import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.util.manager.PathManager;

import java.util.Enumeration;
import java.util.Objects;

public abstract class Service {

    /**
     * initializes concrete Service object
     *
     * @throws ServiceException
     */
    public abstract void initialize() throws ServiceException;

    /**
     * releases connection to db
     */
    public abstract void releaseConnection();


    public String prepareErrorPage(SessionRequestContent content, ErrorType errorType) {
        if (errorType == null || errorType.equals(ErrorType.INTERNAL_SERVER)) {
            content.setRequestAttribute(RequestAttributeType.ERROR_CODE.getName(), Constants.ERROR_SERVER_CODE);
            content.setRequestAttribute(RequestAttributeType.ERROR_MESSAGE.getName(), Constants.ERROR_SERVER);
        } else if (errorType.equals(ErrorType.NOT_FOUND)) {
            content.setRequestAttribute(RequestAttributeType.ERROR_CODE.getName(), Constants.ERROR_NOT_FOUND_CODE);
            content.setRequestAttribute(RequestAttributeType.ERROR_MESSAGE.getName(), Constants.ERROR_NOT_FOUND);
        } else if (errorType.equals(ErrorType.BAD_REQUEST)) {
            content.setRequestAttribute(RequestAttributeType.ERROR_CODE.getName(), Constants.ERROR_BAD_REQUEST_CODE);
            content.setRequestAttribute(RequestAttributeType.ERROR_MESSAGE.getName(), Constants.ERROR_BAD_REQUEST);
        }
        return PathManager.getPagePath(PageType.ERROR.getPageName());
    }

    public String getUriOfRequest(SessionRequestContent content) {
        StringBuilder stringBuilder = new StringBuilder(content.getRequestURI());
        stringBuilder.append("?");
        Enumeration enumeration = content.getRequestParameterNames();
        while (enumeration.hasMoreElements()) {
            String key = (String) enumeration.nextElement();
            String value = content.getRequestParameterValues(key)[0];
            if (key.equals(SessionAttributeType.LOCALE.getName())) {
                continue;
            }
            stringBuilder.append(key).append("=").append(value).append("&");
        }

        if (content.getRequestParameterMap().size() > 0) {
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        }

        return stringBuilder.toString();
    }

    private boolean checkLocaleMaintenance(String locale) throws RequestParameterException {
        if (locale == null || Objects.equals(locale, "")) return false;
        if (!(locale.equals(Constants.LOCALE_RU) || locale.equals(Constants.LOCALE_EN))) {
            throw new RequestParameterException();
        }
        return true;
    }

    public String getPageName(SessionRequestContent content) {
        String uri = (String) content.getSessionAttribute(SessionAttributeType.URI.getName());
        if (uri != null && !Objects.equals(uri, "")) {
            return (String) content.getSessionAttribute(RequestParameterType.URI.getName());
        } else {
            return "/";
        }
    }

    public boolean checkOffsetAppropriate(String offset) {
        try {
            int offsetValue = Integer.parseInt(offset);
            if (offsetValue < 0) return false;
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public String processOffset(String offset, PageType pageType, SessionRequestContent content) throws ServiceException {
        if (offset == null || Objects.equals(offset, "")) {
            switch (pageType) {
                case FILMS:
                    offset = (String) content.getSessionAttribute(SessionAttributeType.FILMS_OFFSET.getName());
                    if (offset == null || Objects.equals(offset, "")) return "0";
                    else {
                        content.setRequestAttribute(RequestAttributeType.FILMS_OFFSET.getName(), offset);
                        return offset;
                    }
                case USERS:
                    offset = (String) content.getSessionAttribute(SessionAttributeType.USERS_OFFSET.getName());
                    if (offset == null || Objects.equals(offset, "")) return "0";
                    else {
                        content.setRequestAttribute(RequestAttributeType.USERS_OFFSET.getName(), offset);
                        return offset;
                    }
                default:
                    throw new ServiceException();
            }
        } else return offset;
    }
}

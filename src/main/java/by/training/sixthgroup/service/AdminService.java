package by.training.sixthgroup.service;

import by.training.sixthgroup.constant.*;
import by.training.sixthgroup.controller.SessionRequestContent;
import by.training.sixthgroup.db.dao.*;
import by.training.sixthgroup.db.pool.ConnectionPool;
import by.training.sixthgroup.exception.DatabaseException;
import by.training.sixthgroup.exception.RequestParameterException;
import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.Country;
import by.training.sixthgroup.model.Genre;
import by.training.sixthgroup.model.User;
import by.training.sixthgroup.model.UserStatus;
import by.training.sixthgroup.util.manager.PathManager;

import java.sql.Connection;
import java.util.List;

/**
 * provides operations with db which are specific for admin mode
 */
public class AdminService extends Service {

    private CommentDAO commentDAO;
    private UserDAO userDAO;
    private MarkDAO markDAO;
    private FilmDAO filmDAO;
    private ConnectionPool connectionPool;
    private Connection connection;

    @Override
    public void initialize() throws ServiceException {
        DAOFactory daoFactory = DAOFactory.newInstance();
        connectionPool = daoFactory.getConnectionPool();
        try {
            connection = connectionPool.getConnection();
            commentDAO = daoFactory.getCommentDAO(connection);
            userDAO = daoFactory.getUserDAO(connection);
            markDAO = daoFactory.getMarkDAO(connection);
            filmDAO = daoFactory.getFilmDAO(connection);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void releaseConnection() {
        if (connection != null) {
            connectionPool.releaseConnection(connection);
        }
    }

    public void changeBanStatus(String kind, String userId, String numberOfDays) throws ServiceException, RequestParameterException {
        try {
            switch (kind) {
                case Constants.SET:
                    userDAO.setBan(Integer.parseInt(userId), Integer.parseInt(numberOfDays));
                    break;
                case Constants.RESET:
                    userDAO.resetBan(Integer.parseInt(userId));
                    break;
                default:
                    throw new RequestParameterException();
            }
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    public User findUser(int id) throws ServiceException {
        try {
            return userDAO.findUser(id);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    public String prepareUsersPage(SessionRequestContent content, AdminService adminService) throws ServiceException {
        String offset = content.getRequestParameter(RequestParameterType.USERS_OFFSET.getName());
        offset = adminService.processOffset(offset, PageType.USERS, content);
        if (adminService.checkOffsetAppropriate(offset)) {
            List<User> users = adminService.findLimitedUsers(Integer.parseInt(offset));
            content.setRequestAttribute(RequestAttributeType.USERS.getName(), users);
        } else throw new ServiceException();
        return PathManager.getPagePath(PageType.USERS.getPageName());
    }

    public String prepareUserPage(SessionRequestContent content, String successMessage, User userToChange) throws ServiceException {
        try {
            content.setRequestAttribute(RequestAttributeType.OTHER_USER.getName(), userToChange);
            content.setRequestAttribute(RequestAttributeType.COMMENTS_FILMS.getName(), commentDAO.findCommentsByUser(userToChange.getId()));
            content.setRequestAttribute(RequestAttributeType.MARKS_FILMS.getName(), markDAO.findMarksFilmsByUser(userToChange.getId()));
            content.setSessionAttribute(SessionAttributeType.MESSAGE_SUCCESS.getName(), successMessage);
            return PathManager.getPagePath(PageType.USER.getPageName());
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    public void changeStatus(int userId, String userStatus) throws ServiceException, RequestParameterException {
        try {
            userDAO.changeStatus(userId, userStatus);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    public UserStatus getNewUserStatus(UserStatus userStatus, String kind) throws ServiceException, RequestParameterException {
        switch (userStatus) {
            case AMATEUR:
                if (kind.equals("down") && !kind.equals("up")) {
                    throw new RequestParameterException();
                }
                return UserStatus.EXPERIENCED;
            case EXPERIENCED:
                if (kind.equals("down")) {
                    return UserStatus.AMATEUR;
                } else {
                    if (kind.equals("up")) {
                        return UserStatus.GURU;
                    }
                    throw new RequestParameterException();
                }
            case GURU:
                if (kind.equals("up") && !kind.equals("down")) {
                    throw new RequestParameterException();
                }
                return UserStatus.EXPERIENCED;
            default:
                throw new RequestParameterException();
        }
    }

    public List<Genre> findAllGenres() throws ServiceException {
        try {
            return filmDAO.findAllGenres();
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    public List<Country> findAllCountries() throws ServiceException {
        try {
            return filmDAO.findAllCountries();
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    public Genre findGenreById(int genreId) throws ServiceException {
        try {
            return filmDAO.findGenreById(genreId);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    public Country findCountryByCode(String countryCode) throws ServiceException {
        try {
            return filmDAO.findCountryByCode(countryCode);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    public void addFilm(String name, int duration, int releaseYear, String description, String url,
                        List<Genre> genres, List<Country> countries) throws ServiceException, RequestParameterException {
        try {
            filmDAO.addFilm(name, duration, releaseYear, description, url, genres, countries);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    public List<User> findLimitedUsers(int offset) throws ServiceException {
        try {
            return userDAO.findLimitedUsers(offset);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }
}

package by.training.sixthgroup.service;

import by.training.sixthgroup.constant.SortType;
import by.training.sixthgroup.db.dao.*;
import by.training.sixthgroup.db.pool.ConnectionPool;
import by.training.sixthgroup.db.table.FilmsTable;
import by.training.sixthgroup.exception.DatabaseException;
import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.*;
import by.training.sixthgroup.util.model.CommentFilmData;
import by.training.sixthgroup.util.model.CommentUserData;
import by.training.sixthgroup.util.model.MarkFilmData;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * provides operations with db which are specific for user mode
 */
public class UserService extends Service {

    private CommentDAO commentDAO;
    private UserDAO userDAO;
    private MarkDAO markDAO;
    private FilmDAO filmDAO;
    private ConnectionPool connectionPool;
    private Connection connection;


    public void initialize() throws ServiceException {
        DAOFactory daoFactory = DAOFactory.newInstance();
        connectionPool = daoFactory.getConnectionPool();
        try {
            connection = connectionPool.getConnection();
            commentDAO = daoFactory.getCommentDAO(connection);
            userDAO = daoFactory.getUserDAO(connection);
            markDAO = daoFactory.getMarkDAO(connection);
            filmDAO = daoFactory.getFilmDAO(connection);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    public void releaseConnection() {
        if (connection != null) {
            connectionPool.releaseConnection(connection);
        }

    }

    public List<CommentFilmData> findCommentsByUser(int userId) throws ServiceException {
        try {
            return commentDAO.findCommentsByUser(userId);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    public List<MarkFilmData> findMarksFilmsByUser(int userId) throws ServiceException {
        try {
            return markDAO.findMarksFilmsByUser(userId);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    public void deleteComment(int commentId) throws ServiceException {
        try {
            commentDAO.deleteComment(commentId);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    public User findUser(int id) throws ServiceException {
        try {
            return userDAO.findUser(id);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    public List<Genre> findGenresByFilm(int id) throws ServiceException {
        try {
            return filmDAO.findGenresByFilm(id);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    public List<CommentUserData> findCommentsByFilm(int filmId) throws ServiceException {
        try {
            return commentDAO.findCommentsByFilm(filmId);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    public List<Country> findCountriesByFilm(int id) throws ServiceException {
        try {
            return filmDAO.findCountriesByFilm(id);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    public void deleteMark(int filmId, int userId) throws ServiceException {
        try {
            markDAO.deleteMark(filmId, userId);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    public void changePassword(int userId, String oldPassword, String password) throws ServiceException {
        try {
            userDAO.changePassword(userId, oldPassword, password);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    public Mark findMarkByUserAndFilm(int userId, int filmId) throws ServiceException {
        try {
            return markDAO.findMarkByUserAndFilm(userId, filmId);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    private void updateRating(int filmId, double rating) throws ServiceException {
        try {
            filmDAO.updateRating(filmId, rating);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    public void addUser(User user) throws ServiceException {
        try {
            userDAO.addUser(user);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    public void addComment(Comment comment) throws ServiceException {
        try {
            commentDAO.addComment(comment);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    public void addMark(Mark mark) throws ServiceException {
        try {
            markDAO.addMark(mark);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    public void resetBan(int userId) throws ServiceException {
        try {
            userDAO.resetBan(userId);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    public User findUser(String login, String password) throws ServiceException {
        try {
            return userDAO.findUser(login, password);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    public User findUser(String login) throws ServiceException {
        try {
            return userDAO.findUser(login);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    public Map<Integer, String> findNamesByPattern(String pattern) throws ServiceException {
        try {
            return filmDAO.findFilmNamesByPattern(pattern);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }


    public User findUserByCommentId(int commentId) throws ServiceException {
        try {
            int userId = commentDAO.findUserIdByCommentId(commentId);
            if (userId > 0)
                return userDAO.findUser(userId);
            else throw new ServiceException();
        } catch (DatabaseException e) {
            throw new ServiceException();
        }
    }

    public boolean checkUserExistence(int userId) throws ServiceException {
        try {
            return userDAO.checkUserExistence(userId);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    public boolean checkFilmExistence(int filmId) throws ServiceException {
        try {
            return filmDAO.checkFilmExistence(filmId);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    public boolean checkMarkExistence(int userId, int filmId) throws ServiceException {
        try {
            return markDAO.checkMarkExistence(userId, filmId);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    public List<Film> findSortedFilms(String sortType, int offset) throws ServiceException {
        try {
            if (sortType.equals(SortType.BY_YEAR.getName())) {
                sortType = FilmsTable.RELEASE_YEAR.getName();
            }
            return filmDAO.findSortedFilms(sortType, offset);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    public Film findFilmById(int id) throws ServiceException {
        try {
            return filmDAO.findFilmById(id);
        } catch (DatabaseException e) {
            throw new ServiceException();
        }
    }

    public void updateRating(int filmId) throws ServiceException {
        List<Mark> marks = findMarksByFilm(filmId);
        List<Integer> markValues = fetchMarkValues(marks);
        double rating = calculateAverage(markValues);
        updateRating(filmId, rating);
    }

    private List<Mark> findMarksByFilm(int filmId) throws ServiceException {
        try {
            return markDAO.findMarksByFilm(filmId);
        } catch (DatabaseException e) {
            throw new ServiceException(e);
        }
    }

    private List<Integer> fetchMarkValues(List<Mark> marks) {
        List<Integer> markValues = new ArrayList<>();
        for (Mark mark : marks) {
            markValues.add(mark.getMark());
        }
        return markValues;
    }

    private double calculateAverage(List<Integer> marks) {
        Integer sum = 0;
        if (!marks.isEmpty()) {
            for (Integer mark : marks) {
                sum += mark;
            }
            return sum.doubleValue() / marks.size();
        }
        return sum;
    }
}

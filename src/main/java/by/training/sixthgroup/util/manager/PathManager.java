package by.training.sixthgroup.util.manager;

import by.training.sixthgroup.constant.PageType;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import static by.training.sixthgroup.constant.Constants.CONFIG_FILE_NAME;
import static by.training.sixthgroup.constant.Constants.PAGE_PATH_PROPERTY_PREFIX;


/**
 * reads properties from config.properties
 */
public class PathManager {

    private static ResourceBundle resourceBundle = ResourceBundle.getBundle(CONFIG_FILE_NAME);

    private PathManager() {
    }

    public static String getPagePath(String page) {
        try {
            return resourceBundle.getString(PAGE_PATH_PROPERTY_PREFIX + page);
        } catch (MissingResourceException e) {
            return resourceBundle.getString(PAGE_PATH_PROPERTY_PREFIX + PageType.HOME.getPageName());
        }
    }

}
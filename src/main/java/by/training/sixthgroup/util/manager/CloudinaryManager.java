package by.training.sixthgroup.util.manager;


import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;

import java.util.ResourceBundle;

import static by.training.sixthgroup.constant.Constants.*;

public class CloudinaryManager {

    private CloudinaryManager() {
    }

    /**
     * gets instance of Cloudinary object which loads an image to cloud.
     *
     * @return
     */
    public static Cloudinary getCloudinary() {
        ResourceBundle resourceBundle = ResourceBundle.getBundle(CLOUDINARY_PROPS_FILE_NAME);
        String cloudName = resourceBundle.getString(CLOUDINARY_CLOUD_NAME);
        String apiKey = resourceBundle.getString(CLOUDINARY_API_KEY);
        String apiSecret = resourceBundle.getString(CLOUDINARY_API_SECRET);
        return new Cloudinary(ObjectUtils.asMap(
                CLOUDINARY_CLOUD_NAME, cloudName,
                CLOUDINARY_API_KEY, apiKey,
                CLOUDINARY_API_SECRET, apiSecret));
    }
}

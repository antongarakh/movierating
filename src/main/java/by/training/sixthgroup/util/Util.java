package by.training.sixthgroup.util;

import by.training.sixthgroup.exception.ServiceException;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Util {
    
    private Util(){}

    /**
     * convers object to JSON string
     *
     * @param object
     * @return
     * @throws ServiceException
     */
    public static String objectToJSONString(Object object) throws ServiceException {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.writeValueAsString(object);
        } catch (IOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * gets list of String. Each String is year number representation
     *
     * @param firstYear
     * @param lastYear
     * @return
     */
    public static List<String> getListOfYears(int firstYear, int lastYear) {
        int count = lastYear - firstYear;
        if (count < 0) {
            return new ArrayList<>();
        } else {
            List<String> years = new ArrayList<>(count);
            for (int i = 0; i <= count; i++) {
                years.add("" + (firstYear + i));
            }
            return years;
        }
    }
}

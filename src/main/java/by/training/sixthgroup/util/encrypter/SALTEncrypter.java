package by.training.sixthgroup.util.encrypter;

import by.training.sixthgroup.exception.ServiceException;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SALTEncrypter implements Encrypter {

    private static final String SALT = "vhg234fgfg4566";
    private static final String ENCRYPTER_ALGORITHM = "SHA-256";

    @Override
    public String encrypt(String stringToEncrypt) throws ServiceException {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(ENCRYPTER_ALGORITHM);
            messageDigest.update(stringToEncrypt.concat(SALT).getBytes());
            byte[] encryptedStringBytes = messageDigest.digest();
            return DatatypeConverter.printHexBinary(encryptedStringBytes);
        } catch (NoSuchAlgorithmException e) {
            throw new ServiceException(e);
        }
    }
}

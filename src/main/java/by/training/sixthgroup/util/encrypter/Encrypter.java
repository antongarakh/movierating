package by.training.sixthgroup.util.encrypter;

import by.training.sixthgroup.exception.ServiceException;

public interface Encrypter {
    /**
     * enctypts String with an appropriate algorithm
     *
     * @param stringToEncrypt
     * @return
     * @throws ServiceException
     */
    String encrypt(String stringToEncrypt) throws ServiceException;
}

package by.training.sixthgroup.util.model;

import java.io.Serializable;

/**
 * class with specific data needed for displaying one user.jsp
 */
public class MarkFilmData implements Serializable {
    private int filmId;
    private int mark;
    private String filmName;

    public MarkFilmData() {
    }

    public MarkFilmData(int filmId, int mark, String filmName) {
        this.filmId = filmId;
        this.mark = mark;
        this.filmName = filmName;
    }

    public int getFilmId() {
        return filmId;
    }

    public void setFilmId(int filmId) {
        this.filmId = filmId;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public String getFilmName() {
        return filmName;
    }

    public void setFilmName(String filmName) {
        this.filmName = filmName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MarkFilmData)) return false;

        MarkFilmData that = (MarkFilmData) o;

        if (filmId != that.filmId) return false;
        if (mark != that.mark) return false;
        return filmName != null ? filmName.equals(that.filmName) : that.filmName == null;

    }

    @Override
    public int hashCode() {
        int result = filmId;
        result = 31 * result + mark;
        result = 31 * result + (filmName != null ? filmName.hashCode() : 0);
        return result;
    }
}

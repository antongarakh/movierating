package by.training.sixthgroup.util.model;

import java.io.Serializable;

/**
 * class with specific data needed for displaying one user.jsp
 */
public class CommentFilmData implements Serializable {

    private int commentId;
    private String comment;
    private String filmName;
    private int userId;

    public CommentFilmData() {
    }

    public CommentFilmData(int commentId, String comment, String filmName, int userId) {
        this.commentId = commentId;
        this.comment = comment;
        this.filmName = filmName;
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getFilmName() {
        return filmName;
    }

    public void setFilmName(String filmName) {
        this.filmName = filmName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CommentFilmData)) return false;

        CommentFilmData that = (CommentFilmData) o;

        if (commentId != that.commentId) return false;
        if (userId != that.userId) return false;
        if (comment != null ? !comment.equals(that.comment) : that.comment != null) return false;
        return filmName != null ? filmName.equals(that.filmName) : that.filmName == null;

    }

    @Override
    public int hashCode() {
        int result = commentId;
        result = 31 * result + (comment != null ? comment.hashCode() : 0);
        result = 31 * result + (filmName != null ? filmName.hashCode() : 0);
        result = 31 * result + userId;
        return result;
    }
}

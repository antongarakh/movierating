package by.training.sixthgroup.util.model;

import java.io.Serializable;

/**
 * class with specific data needed for displaying one film.jsp
 */
public class CommentUserData implements Serializable {

    private String comment;
    private String userName;
    private int userId;
    private boolean isUserAdmin;
    private int commentId;


    public CommentUserData(String comment, String userName, int userId, int commentId, boolean isUserAdmin) {
        this.comment = comment;
        this.userName = userName;
        this.userId = userId;
        this.commentId = commentId;
        this.isUserAdmin = isUserAdmin;

    }

    public CommentUserData() {

    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public boolean getIsUserAdmin() {
        return isUserAdmin;
    }

    public void setUserAdmin(boolean userAdmin) {
        isUserAdmin = userAdmin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CommentUserData)) return false;

        CommentUserData that = (CommentUserData) o;

        if (userId != that.userId) return false;
        if (isUserAdmin != that.isUserAdmin) return false;
        if (commentId != that.commentId) return false;
        if (comment != null ? !comment.equals(that.comment) : that.comment != null) return false;
        return userName != null ? userName.equals(that.userName) : that.userName == null;

    }

    @Override
    public int hashCode() {
        int result = comment != null ? comment.hashCode() : 0;
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        result = 31 * result + userId;
        result = 31 * result + (isUserAdmin ? 1 : 0);
        result = 31 * result + commentId;
        return result;
    }
}

package by.training.sixthgroup.listener;

import by.training.sixthgroup.db.dao.DAOFactory;
import by.training.sixthgroup.db.pool.ConnectionPool;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


public class ContextListener implements ServletContextListener {

    /**
     * instantiate connection pool which means that all the connections are in pool
     *
     * @param servletContextEvent
     */
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        DAOFactory daoFactory = DAOFactory.newInstance();
        daoFactory.getConnectionPool();
    }

    /**
     * closes connection pool which means that all the connections from pool are dead
     *
     * @param servletContextEvent
     */
    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        DAOFactory daoFactory = DAOFactory.newInstance();
        ConnectionPool connectionPool = daoFactory.getConnectionPool();
        connectionPool.close();
    }
}

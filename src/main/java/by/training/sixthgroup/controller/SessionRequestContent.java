package by.training.sixthgroup.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Map;

/**
 * wrapper of request and response objects
 */
public class SessionRequestContent {

    private HttpServletRequest request;
    private HttpServletResponse response;

    public SessionRequestContent(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public Object getRequestAttribute(String name) {
        return request.getAttribute(name);
    }

    public String getRequestParameter(String name) {
        return request.getParameter(name);
    }

    public String[] getRequestParameterValues(String name) {
        return request.getParameterValues(name);
    }

    public Object getSessionAttribute(String name) {
        HttpSession session = request.getSession(false);
        return session != null ? session.getAttribute(name) : null;
    }

    public void setSessionAttribute(String name, Object value, boolean create) {
        HttpSession session = request.getSession(create);
        if (session != null) {
            session.setAttribute(name, value);
        }
    }

    public void setSessionAttribute(String name, Object value) {
        setSessionAttribute(name, value, false);
    }

    public void removeSessionAttribute(String name) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.removeAttribute(name);
        }
    }

    public void setRequestAttribute(String name, Object value) {
        request.setAttribute(name, value);
    }

    public void invalidateSession() {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }
    }

    public String getRequestURI() {
        return request.getRequestURI();
    }

    public Enumeration<String> getRequestParameterNames() {
        return request.getParameterNames();
    }

    public Map<String, String[]> getRequestParameterMap() {
        return request.getParameterMap();
    }

    public String getRealPath(String path) {
        return request.getServletContext().getRealPath(path);
    }

    public void writeResponse(String responseJSON) throws IOException {
        response.getWriter().write(responseJSON);
    }
}

package by.training.sixthgroup.controller;

import by.training.sixthgroup.command.Command;
import by.training.sixthgroup.command.CommandFactory;
import by.training.sixthgroup.constant.RequestAttributeType;
import by.training.sixthgroup.constant.RequestParameterType;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static by.training.sixthgroup.constant.Constants.COMMAND;
import static by.training.sixthgroup.constant.Constants.SEARCH_COMMAND_NAME;

/**
 * Coordinates further behavior based on the request parameters.
 */
public class Controller extends HttpServlet {

    public static final Logger logger = LogManager.getLogger(Controller.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        SessionRequestContent sessionRequestContent = new SessionRequestContent(request, response);
        CommandFactory commandFactory = new CommandFactory();
        Command command = commandFactory.createCommand(request.getParameter(COMMAND));
        String page = command.execute(sessionRequestContent);
        if (!(request.getParameter(RequestParameterType.COMMAND.getName()).equals(SEARCH_COMMAND_NAME))) {
            Boolean isRedirect = (Boolean) (request.getAttribute(RequestAttributeType.SEND_REDIRECT.getName()));
            if (isRedirect != null && isRedirect) {
                response.sendRedirect(page);
            } else {
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
                dispatcher.forward(request, response);
            }
        }
    }

}

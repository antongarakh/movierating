package by.training.sixthgroup.constant;

import java.util.Objects;

/**
 * provided types of film sorting
 */
public enum SortType {

    BY_NAME("name"),
    BY_RATING("rating"),
    BY_DURATION("duration"),
    BY_YEAR("year"),
    NO_SORT("no");

    private String name;

    SortType(String name) {
        this.name = name;
    }

    public static SortType getType(String name) {
        if (name == null || Objects.equals(name, "")) return NO_SORT;
        for (SortType value : SortType.values()) {
            if (name.equals(value.getName())) {
                return value;
            }
        }
        return NO_SORT;
    }

    public String getName() {
        return name;
    }

}

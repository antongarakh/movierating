package by.training.sixthgroup.constant;

/**
 * enum of possible ways user behaves and sees other pages.
 */
public enum UserPageContentType {

    USER_VIEW("userView"),
    ADMIN_VIEW("adminView"),
    OWN_PAGE("ownPage");

    private String name;

    UserPageContentType(String name) {
        this.name = name;
    }

    public static UserPageContentType getType(String name) {
        for (UserPageContentType value : UserPageContentType.values()) {
            if (name.equals(value.getName())) {
                return value;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

}

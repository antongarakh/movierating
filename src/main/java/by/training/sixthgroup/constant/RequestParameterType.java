package by.training.sixthgroup.constant;

/**
 * all the possible request parameters
 */
public enum RequestParameterType {

    COMMAND("command"),
    PAGE("page"),
    LOGIN("formLogin"),
    PASSWORD("formPassword"),
    SUBMIT_COMMENT("formSubmitComment"),
    SUBMIT_MARK("formSubmitMark"),
    CONFIRM_PASSWORD("confirmFormPassword"),
    ID("id"),
    FILM_ID("filmId"),
    EDIT_OLD_PASSWORD("editOldPassword"),
    EDIT_PASSWORD("editPassword"),
    EDIT_CONFIRM_PASSWORD("editConfirmPassword"),
    USER_CONTENT_TYPE("userContentType"),
    KIND("kind"),
    USER_ID("userId"),
    URI("uri"),
    USERS_OFFSET("usersOffset"),
    FILMS_OFFSET("filmsOffset"),
    SORT_TYPE("sort"),
    TERM("term");

    private String name;

    RequestParameterType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

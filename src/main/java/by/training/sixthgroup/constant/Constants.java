package by.training.sixthgroup.constant;

public final class Constants {

    public static final String COMMAND = "command";
    public static final String PAGE_PATH_PROPERTY_PREFIX = "path.page.";
    public static final String CONFIG_FILE_NAME = "config";
    public static final String DB_PROPERTIES_FILE_NAME = "database";
    public static final String DB_URL_PROPERTY = "db.url";
    public static final String DB_USER_PROPERTY = "db.user";
    public static final String DB_PASSWORD_PROPERTY = "db.password";
    public static final String CLOUDINARY_PROPS_FILE_NAME = "cloudinary";
    public static final String CLOUDINARY_CLOUD_NAME = "cloud_name";
    public static final String CLOUDINARY_API_KEY = "api_key";
    public static final String CLOUDINARY_API_SECRET = "api_secret";
    public static final String YEAR = "year";
    public static final String DURATION = "duration";
    public static final String USER_ID = "user_id";
    public static final String FILM_ID = "film_id";
    public static final String USERNAME = "userName";
    public static final String STATUS = "status";
    public static final String SET = "set";
    public static final String RESET = "reset";
    public static final String USERS = "users";
    public static final String ADD_FILM = "add_film";
    public static final String LOAD_FILM = "load_film";
    public static final String LOGOUT = "logout";
    public static final String LOGIN = "login";
    public static final String REGISTER = "register";
    public static final String ERROR_NOT_FOUND = "Not Found";
    public static final String ERROR_NOT_FOUND_CODE = "404";
    public static final String ERROR_SERVER = "Internal Server Error";
    public static final String ERROR_SERVER_CODE = "500";
    public static final String ERROR_BAD_REQUEST = "Bad Request";
    public static final String ERROR_BAD_REQUEST_CODE = "400";
    public static final String SENT_FILE = "sentFile";
    public static final String URL = "url";
    public static final String IMG_FOLDER_NAME = "img";
    public static final String PAGE_CONTENT_FILE_NAME = "pageContent";
    public static final String FLAG = "flag";
    public static final String NO_SORT = "no";
    public static final String SEARCH_COMMAND_NAME = "search";
    public static final String LOCALE_RU = "ru_RU";
    public static final String LOCALE_EN = "en_US";

    private Constants() {
    }

}

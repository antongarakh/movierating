package by.training.sixthgroup.constant;

/**
 * Enumeration of possible errors.
 */
public enum ErrorType {

    NOT_FOUND("404"), INTERNAL_SERVER("500"), BAD_REQUEST("400");

    private String name;

    ErrorType(String name) {
        this.name = name;
    }

    public static ErrorType getType(String name) {
        for (ErrorType value : ErrorType.values()) {
            if (name.equals(value.getName())) {
                return value;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }
}

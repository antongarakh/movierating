package by.training.sixthgroup.constant;

/**
 * enum for add_film.jsp submit form
 */
public enum UploadItemType {

    NAME("addName"),
    DURATION("addDuration"),
    DESCRIPTION("addDescription"),
    RELEASE_YEAR("addReleaseYear"),
    GENRES("addGenres"),
    COUNTRIES("addCountries"),
    SENT_FILE("sentFile");

    private String name;

    UploadItemType(String name) {
        this.name = name;
    }

    public static UserPageContentType getType(String name) {
        for (UserPageContentType value : UserPageContentType.values()) {
            if (name.equals(value.getName())) {
                return value;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }
}

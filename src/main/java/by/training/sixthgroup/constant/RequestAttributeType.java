package by.training.sixthgroup.constant;

/**
 * all the possible request attributes
 */
public enum RequestAttributeType {

    ERROR_MESSAGE_KEY("errorMessageKey"),
    LOCALE("locale"),
    FILMS("films"),
    FILM("film"),
    GENRES("genres"),
    COMMENTS("comments"),
    COUNTRIES("countries"),
    COMMENTS_FILMS("commentsFilms"),
    MARKS_FILMS("marksFilms"),
    OTHER_USER("otherUser"),
    DAYS("days"),
    SEND_REDIRECT("sendRedirect"),
    PAGE("page"),
    MARK("mark"),
    ERROR_CODE("errorCode"),
    ERROR_MESSAGE("errorMessage"),
    EXPIRATION_DATE("expirationDate"),
    ALL_COUNTRIES("allCountries"),
    ALL_GENRES("allGenres"),
    ALL_YEARS("allYears"),
    USERS("users"),
    USERS_OFFSET("usersOffset"),
    FILMS_OFFSET("filmsOffset"),
    USER("user"),
    USER_CONTENT_TYPE("userContentType"),;

    private String name;

    RequestAttributeType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
package by.training.sixthgroup.constant;

/**
 * all the possible session attributes
 */
public enum SessionAttributeType {

    USER("user"),
    LOCALE("locale"),
    IS_ADMIN("isAdmin"),
    FILM_ID("filmId"),
    USER_CONTENT_TYPE("userContentType"),
    ALL_COUNTRIES("allCountries"),
    ALL_GENRES("allGenres"),
    ALL_YEARS("allYears"),
    MESSAGE_SUCCESS("success"),
    URI("uri"),
    FILMS_SORT_TYPE("sort"),
    USERS("users"),
    USERS_OFFSET("usersOffset"),
    FILMS_OFFSET("filmsOffset");

    private String name;

    SessionAttributeType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

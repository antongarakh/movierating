package by.training.sixthgroup.constant;

/**
 * Enumeration of pages in jsp folder
 */
public enum PageType {

    HOME("home"),
    FILM("film"),
    FILMS("films"),
    ERROR("error"),
    LOGIN("login"),
    REGISTRATION("registration"),
    USER("user"),
    USERS("users"),
    ADD_FILM("add_film");

    private String pageName;

    PageType(String pageName) {
        this.pageName = pageName;
    }

    public String getPageName() {
        return pageName;
    }
}

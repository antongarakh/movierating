package by.training.sixthgroup.validation;

import by.training.sixthgroup.exception.RequestParameterException;
import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.User;
import by.training.sixthgroup.service.UserService;
import by.training.sixthgroup.util.encrypter.SALTEncrypter;

import java.util.Date;

public class LogInValidator extends Validator {

    private static final String WRONG_LOGIN_ERROR_KEY = "login.label.logInErrorMessage";
    private static final String BANNED_USER = "user.ban.login";

    private String login;
    private String password;
    private User user;
    private UserService userService;

    public LogInValidator(String login, String password, UserService userService) {
        this.login = login;
        this.password = password;
        this.userService = userService;
    }

    @Override
    public boolean checkValidity() throws ServiceException, RequestParameterException {
        user = this.userService.findUser(this.login, (new SALTEncrypter()).encrypt(this.password));
        boolean valid = (user != null);
        if (!valid) {
            setErrorMessageKey(WRONG_LOGIN_ERROR_KEY);
        } else {
            Date expirationDate = user.getExpirationDate();
            if (user.getExpirationDate() != null) {
                Date currentDate = new Date();
                if (currentDate.compareTo(expirationDate) > 0) {
                    userService.resetBan(user.getId());
                    valid = true;
                } else {
                    setErrorMessageKey(BANNED_USER);
                    valid = false;
                }
            }
        }
        return valid;
    }

    public User getLoggedInUser() {
        return user;
    }

    public Date getExpirationDate() {
        if (user != null)
            return user.getExpirationDate();
        return null;
    }
}

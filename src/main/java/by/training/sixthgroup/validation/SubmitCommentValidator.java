package by.training.sixthgroup.validation;

import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.User;
import by.training.sixthgroup.service.UserService;

import java.util.Objects;

public class SubmitCommentValidator extends Validator {

    private static final String WRONG_COMMENT_ERROR_KEY = "movie.comment.error";

    private String comment;
    private User user;
    private String filmId;
    private UserService userService;
    private IdValidator idValidator;

    public SubmitCommentValidator(String comment, User user, String filmId, UserService userService) {
        this.comment = comment;
        this.user = user;
        this.filmId = filmId;
        this.userService = userService;
        this.idValidator = new IdValidator();
    }

    @Override
    public boolean checkValidity() throws ServiceException {
        boolean valid = checkEmptiness() && checkFilmId() && checkCommentLength();
        if (!valid) {
            setErrorMessageKey(WRONG_COMMENT_ERROR_KEY);
        }
        return valid;
    }

    private boolean checkEmptiness() {
        return (comment != null && !Objects.equals(comment, "")
                && filmId != null && !Objects.equals(filmId, "") && user != null);
    }

    private boolean checkFilmId() throws ServiceException {
        idValidator.setId(filmId);
        if (idValidator.checkValidity()) {
            int filmIdValue = Integer.parseInt(filmId);
            return userService.checkFilmExistence(filmIdValue);
        } else return false;
    }

    private boolean checkCommentLength() {
        return comment.length() <= 1000;
    }
}

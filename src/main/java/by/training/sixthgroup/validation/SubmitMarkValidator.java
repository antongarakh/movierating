package by.training.sixthgroup.validation;

import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.Mark;
import by.training.sixthgroup.model.User;
import by.training.sixthgroup.service.UserService;

import java.util.Objects;

public class SubmitMarkValidator extends Validator {

    private static final String WRONG_MARK_ERROR_KEY = "movie.mark.error";

    private User user;
    private String filmId;
    private UserService userService;
    private String mark;
    private IdValidator idValidator;

    public SubmitMarkValidator(String mark, User user, String filmId, UserService userService) {
        this.mark = mark;
        this.user = user;
        this.filmId = filmId;
        this.userService = userService;
        this.idValidator = new IdValidator();
    }


    @Override
    public boolean checkValidity() throws ServiceException {
        boolean valid = checkEmptiness() && checkFilmId() && checkMark() && checkMarkAppearance();
        if (!valid) {
            setErrorMessageKey(WRONG_MARK_ERROR_KEY);
        }
        return valid;
    }

    private boolean checkEmptiness() {
        return (mark != null && !Objects.equals(mark, "")
                && filmId != null && !Objects.equals(filmId, "") && user != null);
    }

    private boolean checkFilmId() throws ServiceException {
        idValidator.setId(filmId);
        if (idValidator.checkValidity()) {
            int filmIdValue = Integer.parseInt(filmId);
            return userService.checkFilmExistence(filmIdValue);
        } else return false;
    }

    private boolean checkMark() throws ServiceException {
        idValidator.setId(mark);
        if (idValidator.checkValidity()) {
            int markValue = Integer.parseInt(mark);
            if (markValue > 10) return false;
        } else return false;
        return true;
    }

    private boolean checkMarkAppearance() throws ServiceException {
        Mark submittedMark = userService.findMarkByUserAndFilm(user.getId(), Integer.parseInt(filmId));
        return (submittedMark == null || submittedMark.getMark() < 1);
    }


}

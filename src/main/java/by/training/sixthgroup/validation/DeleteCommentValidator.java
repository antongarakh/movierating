package by.training.sixthgroup.validation;

import by.training.sixthgroup.db.table.CommentsTable;
import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.User;
import by.training.sixthgroup.service.UserService;

import java.util.Objects;

public class DeleteCommentValidator extends Validator {

    private static final String DELETE_COMMENT_ERROR_KEY = "user.delete.comment.failure";

    private String commentId;
    private String filmId;
    private String userId;
    private User user;
    private UserService userService;
    private IdValidator idValidator;

    public DeleteCommentValidator(String commentId, String filmId, String userId, User user, UserService userService) {
        this.commentId = commentId;
        this.filmId = filmId;
        this.userId = userId;
        this.user = user;
        this.userService = userService;
        this.idValidator = new IdValidator();
    }

    private String getUserOrFilm() throws ServiceException {
        if (filmId != null && !Objects.equals(filmId, "") && userId != null && !Objects.equals(userId, "")) {
            throw new ServiceException();
        }
        if ((filmId == null || Objects.equals(filmId, "")) && (userId == null && Objects.equals(userId, ""))) {
            throw new ServiceException();
        }
        if (filmId != null && !Objects.equals(filmId, "")) {
            return CommentsTable.FILM_ID.getName();
        } else {
            return CommentsTable.USER_ID.getName();
        }
    }


    @Override
    public boolean checkValidity() throws ServiceException {
        boolean valid;
        if (this.getUserOrFilm().equals(CommentsTable.FILM_ID.getName())) {
            valid = checkEmptiness() && checkFilmId() && checkCommentId() && checkUserRights();
        } else {
            valid = checkEmptiness() && checkUserId() && checkCommentId() && checkUserRights();
        }
        if (!valid) {
            setErrorMessageKey(DELETE_COMMENT_ERROR_KEY);
        }
        return valid;
    }

    private boolean checkEmptiness() {
        return (commentId != null && !Objects.equals(commentId, "")
                && user != null);
    }

    private boolean checkFilmId() throws ServiceException {
        idValidator.setId(filmId);
        if (idValidator.checkValidity()) {
            int filmIdValue = Integer.parseInt(filmId);
            return userService.checkFilmExistence(filmIdValue);
        } else return false;
    }

    private boolean checkUserId() throws ServiceException {
        idValidator.setId(userId);
        if (idValidator.checkValidity()) {
            int userIdValue = Integer.parseInt(userId);
            return userService.checkUserExistence(userIdValue);
        } else return false;
    }

    private boolean checkCommentId() throws ServiceException {
        idValidator.setId(commentId);
        return idValidator.checkValidity();
    }

    private boolean checkUserRights() {
        boolean result = true;
        try {
            User otherUser = userService.findUserByCommentId(Integer.parseInt(commentId));
            if (otherUser != null) {
                if (otherUser.getId() != user.getId()) {
                    if (!user.getIsAdmin())
                        result = false;
                    else if (otherUser.getIsAdmin())
                        result = false;
                }
            } else {
                result = false;
            }
        } catch (ServiceException e) {
            result = false;
        }
        return result;
    }


}

package by.training.sixthgroup.validation;

import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.User;
import by.training.sixthgroup.service.UserService;

import java.util.Objects;

public class DeleteMarkValidator extends Validator {

    private static final String DELETE_MARK_ERROR_KEY = "user.delete.mark.failure";

    private String userId;
    private String filmId;
    private User user;
    private UserService userService;
    private IdValidator idValidator;

    public DeleteMarkValidator(String userId, String filmId, User user, UserService userService) {
        this.userId = userId;
        this.filmId = filmId;
        this.user = user;
        this.userService = userService;
        this.idValidator = new IdValidator();
    }

    @Override
    public boolean checkValidity() throws ServiceException {
        boolean valid = checkEmptiness() && checkFilmId() && checkUserId() && checkMarkExistence() && checkUserRights();
        if (!valid) {
            setErrorMessageKey(DELETE_MARK_ERROR_KEY);
        }
        return valid;
    }

    private boolean checkEmptiness() {
        return (userId != null && !Objects.equals(userId, "")
                && filmId != null && !Objects.equals(filmId, "")
                && user != null);
    }

    private boolean checkFilmId() throws ServiceException {
        idValidator.setId(filmId);
        if (idValidator.checkValidity()) {
            int filmIdValue = Integer.parseInt(filmId);
            return userService.checkFilmExistence(filmIdValue);
        } else return false;
    }

    private boolean checkUserId() throws ServiceException {
        idValidator.setId(userId);
        if (idValidator.checkValidity()) {
            int userIdValue = Integer.parseInt(userId);
            return userService.checkUserExistence(userIdValue);
        } else return false;
    }

    private boolean checkUserRights() {
        boolean result = true;
        try {
            User otherUser = userService.findUser(Integer.parseInt(userId));
            if (otherUser != null) {
                if (otherUser.getId() != user.getId()) {
                    if (!user.getIsAdmin())
                        result = false;
                    else if (otherUser.getIsAdmin())
                        result = false;
                }
            } else {
                result = false;
            }
        } catch (ServiceException e) {
            result = false;
        }
        return result;
    }

    private boolean checkMarkExistence() throws ServiceException {
        return userService.checkMarkExistence(Integer.parseInt(userId), Integer.parseInt(filmId));
    }

}

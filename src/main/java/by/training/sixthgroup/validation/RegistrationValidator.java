package by.training.sixthgroup.validation;

import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.User;
import by.training.sixthgroup.service.UserService;


public class RegistrationValidator extends Validator {

    private static final String WRONG_LOGIN_ERROR_KEY = "error.label.occupiedLogin";
    private static final String WRONG_PASSWORDS_ERROR_KEY = "error.label.wrongPasswords";
    private static final String REGEX_MATCHING_ERROR_KEY = "error.label.passwordRegex";
    private static final String PASSWORD_CHECK_REGEX = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])\\S{8,}$";


    private String login;
    private String password;
    private String confirmedPassword;
    private UserService userService;

    public RegistrationValidator(String login, String password, String confirmedPassword, UserService userService) {
        super();
        this.login = login;
        this.password = password;
        this.userService = userService;
        this.confirmedPassword = confirmedPassword;
    }

    @Override
    public boolean checkValidity() throws ServiceException {
        return checkLogin() && checkPasswordsSameness() && checkPasswordsRegexMatching();
    }

    private boolean checkLogin() throws ServiceException {
        User user = userService.findUser(login);
        boolean valid = (user == null);
        if (!valid) {
            setErrorMessageKey(WRONG_LOGIN_ERROR_KEY);
        }
        return valid;
    }


    private boolean checkPasswordsSameness() {
        boolean valid = password.equals(confirmedPassword);
        if (!valid) {
            setErrorMessageKey(WRONG_PASSWORDS_ERROR_KEY);
        }
        return valid;
    }

    private boolean checkPasswordsRegexMatching() {
        boolean valid = password.matches(PASSWORD_CHECK_REGEX);
        if (!valid) {
            setErrorMessageKey(REGEX_MATCHING_ERROR_KEY);
        }
        return valid;
    }

}

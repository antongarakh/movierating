package by.training.sixthgroup.validation;

import by.training.sixthgroup.exception.ServiceException;

import java.util.Objects;

public class IdValidator extends Validator {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean checkValidity() throws ServiceException {
        boolean valid = true;
        if (id == null || Objects.equals(id, "")) valid = false;
        else try {
            int idValue = Integer.parseInt(id);
            if (idValue < 1) valid = false;
        } catch (NumberFormatException e) {
            valid = false;
        }
        return valid;
    }

}

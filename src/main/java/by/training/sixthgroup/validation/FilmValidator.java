package by.training.sixthgroup.validation;

import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.service.UserService;

public class FilmValidator extends Validator {

    private static final String WRONG_FILM_MESSAGE_KEY = "movie.id.error";

    private String filmId;
    private IdValidator idValidator;
    private UserService userService;

    public FilmValidator(String filmId, UserService userService) {
        this.filmId = filmId;
        this.idValidator = new IdValidator();
        this.userService = userService;
    }

    @Override
    public boolean checkValidity() throws ServiceException {
        boolean valid = true;
        idValidator.setId(filmId);
        if (idValidator.checkValidity()) {
            if (!userService.checkFilmExistence(Integer.parseInt(filmId))) {
                valid = false;
            }
        } else valid = false;
        if (!valid) {
            setErrorMessageKey(WRONG_FILM_MESSAGE_KEY);
        }
        return valid;
    }
}

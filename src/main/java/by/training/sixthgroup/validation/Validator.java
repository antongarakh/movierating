package by.training.sixthgroup.validation;

import by.training.sixthgroup.exception.RequestParameterException;
import by.training.sixthgroup.exception.ServiceException;

public abstract class Validator {

    private String errorMessageKey;

    public Validator() {
        this.errorMessageKey = "";
    }

    /**
     * check validity of request parameters for specific command
     *
     * @return
     * @throws ServiceException
     * @throws RequestParameterException
     */
    public abstract boolean checkValidity() throws ServiceException, RequestParameterException;

    /**
     * gets specific errorMessageKey which is needed for internationalization purposes.
     *
     * @return
     */
    public String getErrorMessageKey() {
        return errorMessageKey;
    }

    /**
     * sets specific errorMessageKey which is needed for internationalization purposes.
     *
     * @param errorMessageKey
     */
    public void setErrorMessageKey(String errorMessageKey) {
        this.errorMessageKey = errorMessageKey;
    }

}

package by.training.sixthgroup.validation;

import by.training.sixthgroup.constant.Constants;
import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.User;
import by.training.sixthgroup.service.AdminService;

import java.util.Objects;

public class BanValidator extends Validator {

    private static final String WRONG_BAN_ERROR_KEY = "user.ban.error";

    private User currentUser;
    private String kind;
    private String userId;
    private String page;
    private String numberOfDays;
    private AdminService adminService;
    private IdValidator idValidator;

    public BanValidator(User currentUser, String kind, String userId,
                        AdminService adminService, String page, String numberOfDays) {
        this.currentUser = currentUser;
        this.kind = kind;
        this.userId = userId;
        this.adminService = adminService;
        this.page = page;
        this.numberOfDays = numberOfDays;
        this.idValidator = new IdValidator();
    }


    @Override
    public boolean checkValidity() throws ServiceException {
        boolean valid = true;
        if (kind.equals(Constants.SET)) {
            valid = checkEmptiness() && checkIsAdmin() && checkUserToBan() && checkNumberOfDays();
        }
        if (kind.equals(Constants.RESET)) {
            valid = checkEmptiness() && checkIsAdmin() && checkUserToBan();
        }
        if (!valid) {
            setErrorMessageKey(WRONG_BAN_ERROR_KEY);
        }
        return valid;
    }

    private boolean checkEmptiness() {
        boolean valid = (kind != null && !Objects.equals(kind, "")
                && userId != null && !Objects.equals(userId, "")
                && page != null && !Objects.equals(page, "")
                && currentUser != null);
        if (valid) {
            if (kind.equals(Constants.RESET) && (numberOfDays != null && !Objects.equals(numberOfDays, ""))) {
                valid = false;
            } else if (kind.equals(Constants.SET) && (numberOfDays == null || Objects.equals(numberOfDays, ""))) {
                valid = false;
            }
        }
        return valid;
    }

    private boolean checkIsAdmin() {
        return currentUser.getIsAdmin();
    }

    private boolean checkUserToBan() throws ServiceException {
        idValidator.setId(userId);
        if (idValidator.checkValidity()) {
            int userIdValue = Integer.parseInt(userId);
            User user = adminService.findUser(userIdValue);
            if (user == null || user.getId() != userIdValue) return false;
            if (user.getIsAdmin()) return false;
        } else return false;
        return true;
    }

    private boolean checkNumberOfDays() throws ServiceException {
        idValidator.setId(numberOfDays);
        return idValidator.checkValidity();
    }


}

package by.training.sixthgroup.validation;

import by.training.sixthgroup.constant.UserPageContentType;
import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.User;
import by.training.sixthgroup.service.UserService;

import java.util.Objects;

public class UserPageValidator extends Validator {

    private static final String USER_PAGE_ERROR_KEY = "user.page.error";

    private UserPageContentType userPageContentType;
    private User user;
    private String otherUserId;
    private UserService userService;
    private IdValidator idValidator;

    public UserPageValidator(UserPageContentType userPageContentType, User user, String userId, UserService userService) {
        this.userPageContentType = userPageContentType;
        this.user = user;
        this.otherUserId = userId;
        this.userService = userService;
        this.idValidator = new IdValidator();
    }

    @Override
    public boolean checkValidity() throws ServiceException {
        boolean valid = checkEmptiness() && checkUserId() && checkUser();
        if (!valid) {
            setErrorMessageKey(USER_PAGE_ERROR_KEY);
        }
        return valid;
    }

    private boolean checkEmptiness() {
        return (userPageContentType != null && otherUserId != null
                && !Objects.equals(otherUserId, ""));
    }

    private boolean checkUserId() throws ServiceException {
        idValidator.setId(otherUserId);
        if (idValidator.checkValidity()) {
            int userIdValue = Integer.parseInt(otherUserId);
            if (!userService.checkUserExistence(userIdValue)) return false;
        } else return false;
        return true;
    }

    private boolean checkUser() {
        if (user == null) {
            return checkGuestRole();
        } else {
            if (user.getIsAdmin()) {
                return checkAdminRole();
            } else
                return checkUserRole();
        }
    }

    private boolean checkGuestRole() {
        return userPageContentType.equals(UserPageContentType.USER_VIEW);
    }

    private boolean checkAdminRole() {
        if (userPageContentType.equals(UserPageContentType.USER_VIEW))
            return false;
        return !(userPageContentType.equals(UserPageContentType.OWN_PAGE)
                && user.getId() != Integer.parseInt(otherUserId));
    }

    private boolean checkUserRole() {
        if (userPageContentType.equals(UserPageContentType.ADMIN_VIEW))
            return false;
        return !(userPageContentType.equals(UserPageContentType.OWN_PAGE)
                && user.getId() != Integer.parseInt(otherUserId));
    }


}

package by.training.sixthgroup.validation;

import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.User;
import by.training.sixthgroup.service.AdminService;

import java.util.Objects;

public class ChangeStatusValidator extends Validator {

    private static final String WRONG_CHANGE_STATUS_ERROR_KEY = "user.status.error";

    private User currentUser;
    private String userId;
    private String kind;
    private String page;
    private AdminService adminService;
    private IdValidator idValidator;

    public ChangeStatusValidator(User currentUser, String userId, String kind,
                                 String page, AdminService adminService) {
        this.currentUser = currentUser;
        this.userId = userId;
        this.kind = kind;
        this.page = page;
        this.adminService = adminService;
        this.idValidator = new IdValidator();
    }


    @Override
    public boolean checkValidity() throws ServiceException {

        boolean valid = checkEmptiness() && checkIsAdmin() && checkUserToChange();
        if (!valid) {
            setErrorMessageKey(WRONG_CHANGE_STATUS_ERROR_KEY);
        }
        return valid;
    }

    private boolean checkEmptiness() {
        return (kind != null && !Objects.equals(kind, "")
                && userId != null && !Objects.equals(userId, "")
                && page != null && !Objects.equals(page, "")
                && currentUser != null);
    }

    private boolean checkUserToChange() throws ServiceException {
        idValidator.setId(userId);
        if (idValidator.checkValidity()) {
            int userIdValue = Integer.parseInt(userId);
            User user = adminService.findUser(userIdValue);
            if (user == null || user.getId() != userIdValue) return false;
            if (user.getIsAdmin()) return false;
        } else return false;
        return true;
    }

    private boolean checkIsAdmin() {
        return currentUser.getIsAdmin();
    }
}

package by.training.sixthgroup.model;

import java.io.Serializable;

public class Country implements Cloneable, Serializable {
    private String countryCode;
    private String countryName;


    public Country(String countryCode, String countryName) {
        this.countryCode = countryCode;
        this.countryName = countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    @Override
    public Country clone() throws CloneNotSupportedException {
        Country country;
        country = (Country) super.clone();
        return country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Country)) return false;

        Country country = (Country) o;

        if (countryCode != null ? !countryCode.equals(country.countryCode) : country.countryCode != null) return false;
        return countryName != null ? countryName.equals(country.countryName) : country.countryName == null;

    }

    @Override
    public int hashCode() {
        int result = countryCode != null ? countryCode.hashCode() : 0;
        result = 31 * result + (countryName != null ? countryName.hashCode() : 0);
        return result;
    }
}

package by.training.sixthgroup.model;

public enum UserStatus {

    GURU("GURU"),
    EXPERIENCED("EXPERIENCED"),
    AMATEUR("AMATEUR");

    private String statusName;

    UserStatus(String statusName) {
        this.statusName = statusName;
    }

    public String getStatusName() {
        return statusName;
    }
}
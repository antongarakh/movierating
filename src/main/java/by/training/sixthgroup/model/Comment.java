package by.training.sixthgroup.model;

import java.util.Date;

public class Comment {
    private String comment;
    private int id;
    private Date date;

    private int filmId;
    private int userId;

    public Comment() {
    }

    public Comment(String comment, int id, Date date, int filmId, int userId) {
        this.comment = comment;
        this.id = id;
        this.date = date;
        this.filmId = filmId;
        this.userId = userId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getFilmId() {
        return filmId;
    }

    public void setFilmId(int filmId) {
        this.filmId = filmId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}

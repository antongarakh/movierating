package by.training.sixthgroup.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Film implements Serializable {
    private int id;
    private int duration;
    private String description;
    private int releaseYear;
    private double rating;
    private String name;
    private String url;


    private List<Genre> genres;
    private List<Country> countries;

    public Film() {
        genres = new ArrayList<>();
        countries = new ArrayList<>();
    }

    public Film(int id, int duration, String description, int releaseYear, double rating, String name, String url) {
        this.id = id;
        this.duration = duration;
        this.description = description;
        this.releaseYear = releaseYear;
        this.rating = rating;
        this.name = name;
        this.url = url;
    }

    public Film(int id, int duration, String description, int releaseYear, double rating, String name, String url, List<Genre> genres, List<Country> countries) {
        this.id = id;
        this.duration = duration;
        this.description = description;
        this.releaseYear = releaseYear;
        this.rating = rating;
        this.name = name;
        this.url = url;
        this.genres = genres;
        this.countries = countries;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<Genre> getGenres() throws CloneNotSupportedException {
        if (genres == null) {
            genres = new ArrayList<>();
        }
        List<Genre> copyGenresList = new ArrayList<>();
        for (Genre genreFromList : genres) {
            copyGenresList.add(genreFromList.clone());
        }
        return copyGenresList;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public List<Country> getCountries() throws CloneNotSupportedException {
        if (countries == null) {
            countries = new ArrayList<>();
        }
        List<Country> copyCountriesList = new ArrayList<>();
        for (Country countryFromList : countries) {
            copyCountriesList.add(countryFromList.clone());
        }
        return copyCountriesList;
    }

    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }

    public void addGenreToFilm(Genre genre) {
        if (genres == null) {
            genres = new ArrayList<>();
        }
        genres.add(genre);
    }


    public void addCountryToFilm(Country country) {
        if (countries == null) {
            countries = new ArrayList<>();
        }
        countries.add(country);
    }

    @Override
    public String toString() {
        return "Film{" +
                " name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Film)) return false;

        Film film = (Film) o;

        if (id != film.id) return false;
        if (duration != film.duration) return false;
        if (releaseYear != film.releaseYear) return false;
        if (Double.compare(film.rating, rating) != 0) return false;
        if (description != null ? !description.equals(film.description) : film.description != null) return false;
        if (name != null ? !name.equals(film.name) : film.name != null) return false;
        if (url != null ? !url.equals(film.url) : film.url != null) return false;
        if (genres != null ? !genres.equals(film.genres) : film.genres != null) return false;
        return countries != null ? countries.equals(film.countries) : film.countries == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id;
        result = 31 * result + duration;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + releaseYear;
        temp = Double.doubleToLongBits(rating);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (genres != null ? genres.hashCode() : 0);
        result = 31 * result + (countries != null ? countries.hashCode() : 0);
        return result;
    }
}

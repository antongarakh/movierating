package by.training.sixthgroup.model;

import java.util.Date;

public class Mark {
    private int filmId;
    private int userId;
    private int mark;
    private Date date;

    public Mark() {
    }

    public Mark(int filmId, int userId, int mark, Date date) {
        this.filmId = filmId;
        this.userId = userId;
        this.mark = mark;
        this.date = date;
    }

    public int getFilmId() {
        return filmId;
    }

    public void setFilmId(int filmId) {
        this.filmId = filmId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}

package by.training.sixthgroup.model;

import java.util.Date;

public class SimpleUser extends User {

    public SimpleUser() {
        super(0);
    }

    public SimpleUser(int id, String userName, String password, UserStatus status, Date expirationDate) {
        super(id, userName, password, 0, status, expirationDate);
    }

}

package by.training.sixthgroup.model;

import java.util.Date;

public class Admin extends User {

    public Admin() {
        super(1);
    }

    public Admin(int id, String userName, String password, UserStatus status, Date expirationDate) {
        super(id, userName, password, 1, status, expirationDate);
    }
}

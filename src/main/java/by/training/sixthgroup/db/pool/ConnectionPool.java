package by.training.sixthgroup.db.pool;

import by.training.sixthgroup.exception.DatabaseException;

import java.sql.Connection;

public interface ConnectionPool {

    Connection getConnection() throws DatabaseException;

    void releaseConnection(Connection connection);

    void close();
}
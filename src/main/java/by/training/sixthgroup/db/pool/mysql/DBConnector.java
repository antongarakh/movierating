package by.training.sixthgroup.db.pool.mysql;

import by.training.sixthgroup.constant.Constants;
import by.training.sixthgroup.exception.DatabaseException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class DBConnector {

    private String url;
    private String user;
    private String password;

    public DBConnector() {
        ResourceBundle resourceBundle = ResourceBundle.getBundle(Constants.DB_PROPERTIES_FILE_NAME);
        url = resourceBundle.getString(Constants.DB_URL_PROPERTY);
        user = resourceBundle.getString(Constants.DB_USER_PROPERTY);
        password = resourceBundle.getString(Constants.DB_PASSWORD_PROPERTY);
    }

    public Connection getConnection() throws DatabaseException {
        try {
            return DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }
}

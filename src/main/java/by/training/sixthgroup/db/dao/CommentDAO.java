package by.training.sixthgroup.db.dao;

import by.training.sixthgroup.exception.DatabaseException;
import by.training.sixthgroup.model.Comment;
import by.training.sixthgroup.util.model.CommentFilmData;
import by.training.sixthgroup.util.model.CommentUserData;

import java.util.List;

public interface CommentDAO {

    /**
     * adds comment to database
     *
     * @param comment
     * @throws DatabaseException
     */
    void addComment(Comment comment) throws DatabaseException;

    /**
     * finds comments of user found by userId
     *
     * @param userId
     * @return
     * @throws DatabaseException
     */
    List<CommentFilmData> findCommentsByUser(int userId) throws DatabaseException;

    /**
     * finds comments for requested film
     *
     * @param filmId
     * @return
     * @throws DatabaseException
     */
    List<CommentUserData> findCommentsByFilm(int filmId) throws DatabaseException;

    /**
     * finds users id by comment he placed
     *
     * @param commentId
     * @return
     * @throws DatabaseException
     */
    int findUserIdByCommentId(int commentId) throws DatabaseException;

    /**
     * deletes comment
     *
     * @param commentId
     * @throws DatabaseException
     */
    void deleteComment(int commentId) throws DatabaseException;
}

package by.training.sixthgroup.db.dao.mysql;

import by.training.sixthgroup.constant.Constants;
import by.training.sixthgroup.db.dao.UserDAO;
import by.training.sixthgroup.db.table.UserTable;
import by.training.sixthgroup.exception.DatabaseException;
import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.Admin;
import by.training.sixthgroup.model.SimpleUser;
import by.training.sixthgroup.model.User;
import by.training.sixthgroup.model.UserStatus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MySQLUserDAO implements UserDAO {

    private static final String CHECK_USER_EXISTENCE = "SELECT EXISTS( "
            + "SELECT 1 FROM users "
            + "WHERE users.id = ? ) AS flag";
    private static final String FIND_ALL_USERS = "SELECT * "
            + "FROM users ";
    private static final String FIND_USER_BY_LOGIN_AND_PASSWORD_QUERY = "SELECT * "
            + "FROM users "
            + "WHERE username = ? "
            + "AND password = ? ";
    private static final String FIND_USER_BY_LOGIN_QUERY = "SELECT * "
            + "FROM users "
            + "WHERE username = ? ";
    private static final String FIND_USER_BY_ID_QUERY = "SELECT * "
            + "FROM users "
            + "WHERE id = ? ";
    private static final String INSERT_USER_QUERY = "INSERT INTO users "
            + "(username, password, is_admin, status) "
            + "VALUES (?, ?, ?, ?) ";
    private static final String SET_PASSWORD_QUERY = "UPDATE users "
            + "SET users.password = ? "
            + "WHERE users.id = ? "
            + "AND users.password = ? ";
    private static final String SET_USERNAME_QUERY = "UPDATE users "
            + "SET users.username = ? "
            + "WHERE id = ?  ";
    private static final String SET_STATUS_QUERY = "UPDATE users "
            + "SET users.status = ? "
            + "WHERE id = ? ";
    private static final String SET_BAN_DATE_QUERY = "UPDATE users "
            + "SET users.expiration_date = DATE_ADD(CURTIME(), INTERVAL ? DAY) "
            + "WHERE users.id = ? ";
    private static final String RESET_BAN_QUERY = "UPDATE users "
            + "SET users.expiration_date = null "
            + "WHERE users.id = ? ";

    private static final String FIND_LIMITED_USERS = "SELECT * "
            + "FROM users "
            + "LIMIT 10 OFFSET ? ";
    private Connection connection;

    public MySQLUserDAO(Connection connection) {
        this.connection = connection;
    }

    @Override
    public User findUser(String login) throws DatabaseException {
        try (PreparedStatement query = connection.prepareStatement(FIND_USER_BY_LOGIN_QUERY)) {
            query.setString(1, login);
            ResultSet userResultSet = query.executeQuery();
            User user = null;
            if (userResultSet.next()) {
                user = createUser(userResultSet);
            }
            return user;
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public List<User> findLimitedUsers(int offset) throws DatabaseException {
        try (PreparedStatement query = connection.prepareStatement(FIND_LIMITED_USERS)) {
            query.setInt(1, offset * 10);
            ResultSet resultSet = query.executeQuery();
            List<User> users = new ArrayList<>();
            while (resultSet.next()) {
                users.add(createUser(resultSet));
            }
            return users;
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }


    @Override
    public User findUser(int id) throws DatabaseException {
        try (PreparedStatement query = connection.prepareStatement(FIND_USER_BY_ID_QUERY)) {
            query.setInt(1, id);
            ResultSet userResultSet = query.executeQuery();
            User user = null;
            if (userResultSet.next()) {
                user = createUser(userResultSet);
            }
            return user;
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public User findUser(String login, String password) throws DatabaseException {
        try (PreparedStatement query = connection.prepareStatement(FIND_USER_BY_LOGIN_AND_PASSWORD_QUERY)) {
            query.setString(1, login);
            query.setString(2, password);
            ResultSet userResultSet = query.executeQuery();
            User user = null;
            if (userResultSet.next()) {
                user = createUser(userResultSet);
            }
            return user;
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public void addUser(User user) throws DatabaseException {
        try (PreparedStatement query = connection.prepareStatement(INSERT_USER_QUERY)) {
            query.setString(1, user.getUsername());
            query.setString(2, user.getPassword());
            query.setString(3, user.getIsAdmin() ? "1" : "0");
            query.setString(4, user.getStatus());
            query.executeUpdate();
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public void changePassword(int userId, String oldPassword, String password) throws DatabaseException {
        try (PreparedStatement query = connection.prepareStatement(SET_PASSWORD_QUERY)) {
            query.setString(1, password);
            query.setInt(2, userId);
            query.setString(3, oldPassword);
            query.executeUpdate();
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public void changeStatus(int userId, String userStatus) throws DatabaseException, ServiceException {
        changeField(userId, Constants.STATUS, userStatus);
    }

    @Override
    public void setBan(int userId, int numberOfDays) throws DatabaseException {
        try (PreparedStatement query = connection.prepareStatement(SET_BAN_DATE_QUERY)) {
            query.setInt(1, numberOfDays);
            query.setInt(2, userId);
            query.executeUpdate();
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public void resetBan(int userId) throws DatabaseException {
        try (PreparedStatement query = connection.prepareStatement(RESET_BAN_QUERY)) {
            query.setInt(1, userId);
            query.executeUpdate();
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    private void changeField(int userId, String paramName, String param) throws DatabaseException, ServiceException {
        String selectedQuery;
        switch (paramName) {
            case Constants.USERNAME:
                selectedQuery = SET_USERNAME_QUERY;
                break;
            case Constants.STATUS:
                selectedQuery = SET_STATUS_QUERY;
                break;
            default:
                throw new ServiceException();
        }
        try (PreparedStatement query = connection.prepareStatement(selectedQuery)) {
            query.setString(1, param);
            query.setInt(2, userId);
            query.executeUpdate();
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }


    private User createUser(ResultSet resultSet) throws SQLException {
        int userId = resultSet.getInt(UserTable.ID.getName());
        String password = resultSet.getString(UserTable.PASSWORD.getName());
        String userName = resultSet.getString(UserTable.USERNAME.getName());
        boolean isAdmin = resultSet.getInt(UserTable.IS_ADMIN.getName()) == 1;
        UserStatus userStatus = UserStatus.valueOf(resultSet.getString(UserTable.STATUS.getName()));
        Date date = resultSet.getDate(UserTable.EXPIRATION_DATE.getName());
        return createDeterminedUser(userId, userName, password, isAdmin, userStatus, date);
    }

    private User createDeterminedUser(int userId, String userName, String password, boolean isAdmin, UserStatus userStatus, Date date) throws SQLException {
        User user;
        if (isAdmin) {
            user = new Admin(userId, userName, password, userStatus, date);

        } else {
            user = new SimpleUser(userId, userName, password, userStatus, date);
        }
        return user;
    }

    @Override
    public boolean checkUserExistence(int id) throws DatabaseException {
        try (PreparedStatement query = connection.prepareStatement(CHECK_USER_EXISTENCE)) {
            query.setInt(1, id);
            ResultSet resultSet = query.executeQuery();
            return resultSet.next() && resultSet.getBoolean(Constants.FLAG);
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }


}

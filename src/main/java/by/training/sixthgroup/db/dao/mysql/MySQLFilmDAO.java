package by.training.sixthgroup.db.dao.mysql;

import by.training.sixthgroup.constant.Constants;
import by.training.sixthgroup.constant.SortType;
import by.training.sixthgroup.db.dao.FilmDAO;
import by.training.sixthgroup.db.table.CountryTable;
import by.training.sixthgroup.db.table.FilmsTable;
import by.training.sixthgroup.db.table.GenresTable;
import by.training.sixthgroup.exception.DatabaseException;
import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.Country;
import by.training.sixthgroup.model.Film;
import by.training.sixthgroup.model.Genre;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MySQLFilmDAO implements FilmDAO {

    private static final String FIND_COUNTRIES_BY_FILM_ID_QUERY = "SELECT * "
            + "FROM countries "
            + "WHERE countries.country_code IN (SELECT films_countries.country_code "
            + "FROM films_countries INNER JOIN films "
            + "ON films_countries.film_id = films.id "
            + "WHERE films.id = ? )";

    private static final String FIND_ALL_FILMS_QUERY = "SELECT * "
            + "FROM films ";

    private static final String FIND_FILMS_BY_DURATION_QUERY = "SELECT * "
            + "FROM films "
            + "WHERE duration < ? ";

    private static final String FIND_FILMS_BY_YEAR_QUERY = "SELECT * "
            + "FROM films "
            + "WHERE release_year < ?";

    private static final String INSERT_FILM_QUERY = "INSERT INTO films "
            + "(name, duration, release_year, description, url) "
            + "VALUES (?, ?, ?, ?, ?) ";

    private static final String FIND_FILM_BY_ID_QUERY = "SELECT * "
            + "FROM films "
            + "WHERE films.id = ? ";

    private static final String FIND_GENRES_BY_FILM_ID_QUERY = "SELECT * "
            + "FROM genres "
            + "WHERE genres.id IN (SELECT films_genres.genre_id "
            + "FROM films_genres INNER JOIN films "
            + "ON films_genres.film_id = films.id "
            + "WHERE films.id = ? )";

    private static final String FIND_FILM_ID_BY_URL_QUERY = "SELECT films.id "
            + "FROM films "
            + "WHERE films.url = ? ";

    private static final String INSERT_FILMS_GENRES_QUERY = "INSERT INTO films_genres "
            + "(film_id, genre_id) "
            + "VALUES (?, ?) ";

    private static final String FIND_ALL_GENRES_QUERY = "SELECT * "
            + "FROM genres ";

    private static final String FIND_ALL_COUNTRIES_QUERY = "SELECT * "
            + "FROM countries ";

    private static final String FIND_GENRE_BY_ID_QUERY = "SELECT * "
            + "FROM genres "
            + "WHERE genres.id = ? ";

    private static final String FIND_COUNTRY_BY_CODE_QUERY = "SELECT * "
            + "FROM countries "
            + "WHERE countries.country_code = ? ";

    private static final String INSERT_FILMS_COUNTRIES_QUERY = "INSERT INTO films_countries "
            + "(film_id, country_code) "
            + "VALUES (?, ?) ";

    private static final String UPDATE_RATING_QUERY = "UPDATE films "
            + "SET films.rating = ? "
            + "WHERE films.id = ? ";


    private static final String FIND_NAMES_BY_PATTERN_QUERY = "SELECT films.id, films.name " +
            "FROM films " +
            "WHERE films.name LIKE ? " +
            "LIMIT 5";

    private static final String CHECK_FILM_EXISTENCE = "SELECT EXISTS( "
            + "SELECT 1 FROM films "
            + "WHERE films.id = ? ) AS flag ";


    private static final String FIND_LIMITED_FILMS = "SELECT * "
            + "FROM films "
            + "LIMIT 10 OFFSET ? ";

    private Connection connection;

    public MySQLFilmDAO(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<Film> findSortedFilms(String sortType, int offset) throws DatabaseException, ServiceException {
        if (sortType.equals(SortType.NO_SORT.getName())) {
            return findFilmsByIntParam(offset * 10, sortType);
        } else {
            final String findSortedFilms;
            if (sortType.equals(SortType.BY_RATING.getName())) {
                findSortedFilms = "SELECT * FROM films ORDER BY " + sortType + " DESC LIMIT 10 OFFSET ? ";
            } else {
                findSortedFilms = "SELECT * FROM films ORDER BY " + sortType + " LIMIT 10 OFFSET ? ";
            }
            try (PreparedStatement query = connection.prepareStatement(findSortedFilms)) {
                query.setInt(1, offset * 10);
                ResultSet resultSet = query.executeQuery();
                List<Film> films = new ArrayList<>();
                while (resultSet.next()) {
                    films.add(createFilm(resultSet));
                }
                return films;
            } catch (SQLException e) {
                throw new DatabaseException(e);
            }
        }
    }

    @Override
    public Map<Integer, String> findFilmNamesByPattern(String pattern) throws DatabaseException {
        HashMap<Integer, String> films = new HashMap<>();
        try (PreparedStatement query = connection.prepareStatement(FIND_NAMES_BY_PATTERN_QUERY)) {
            query.setString(1, pattern + "%");
            ResultSet resultSet = query.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt(FilmsTable.ID.getName());
                String name = resultSet.getString(FilmsTable.NAME.getName());
                films.put(id, name);
            }
            return films;
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    private Film createFilm(ResultSet resultSet) throws SQLException, DatabaseException {
        int filmId = resultSet.getInt(FilmsTable.ID.getName());
        int duration = resultSet.getInt(FilmsTable.DURATION.getName());
        String description = resultSet.getString(FilmsTable.DESCRIPTION.getName());
        int releaseYear = resultSet.getInt(FilmsTable.RELEASE_YEAR.getName());
        double rating = resultSet.getDouble(FilmsTable.RATING.getName());
        String name = resultSet.getString(FilmsTable.NAME.getName());
        String url = resultSet.getString(FilmsTable.URL.getName());
        List<Genre> genres = findGenresByFilm(filmId);
        List<Country> countries = findCountriesByFilm(filmId);
        Film film = new Film(filmId, duration, description, releaseYear, rating, name, url);
        film.setGenres(genres);
        film.setCountries(countries);
        return film;
    }

    public List<Genre> findGenresByFilm(int id) throws DatabaseException {
        try (PreparedStatement query = connection.prepareStatement(FIND_GENRES_BY_FILM_ID_QUERY)) {
            query.setInt(1, id);
            ResultSet genresResultSet = query.executeQuery();
            List<Genre> genres = new ArrayList<>();
            while (genresResultSet.next()) {
                genres.add(createGenre(genresResultSet));
            }
            return genres;
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public List<Country> findCountriesByFilm(int id) throws DatabaseException {
        try (PreparedStatement query = connection.prepareStatement(FIND_COUNTRIES_BY_FILM_ID_QUERY)) {
            query.setInt(1, id);
            ResultSet countryResultSet = query.executeQuery();
            List<Country> countries = new ArrayList<>();
            while (countryResultSet.next()) {
                countries.add(createCountry(countryResultSet));
            }
            return countries;
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    private Country createCountry(ResultSet resultSet) throws SQLException {
        String countryCode = resultSet.getString(CountryTable.COUNTRY_CODE.getName());
        String countryName = resultSet.getString(CountryTable.COUNTRY_NAME.getName());
        return new Country(countryCode, countryName);
    }

    private Genre createGenre(ResultSet resultSet) throws SQLException {
        int genreId = resultSet.getInt(GenresTable.ID.getName());
        String name = resultSet.getString(GenresTable.GENRE_NAME.getName());
        return new Genre(genreId, name);
    }

    private List<Film> findFilmsByIntParam(int param, String paramName) throws ServiceException, DatabaseException {
        String selectedQuery;
        switch (paramName) {
            case Constants.YEAR:
                selectedQuery = FIND_FILMS_BY_YEAR_QUERY;
                break;
            case Constants.DURATION:
                selectedQuery = FIND_FILMS_BY_DURATION_QUERY;
                break;
            case Constants.NO_SORT:
                selectedQuery = FIND_LIMITED_FILMS;
                break;
            default:
                throw new ServiceException();
        }
        try (PreparedStatement query = connection.prepareStatement(selectedQuery)) {
            query.setInt(1, param);
            ResultSet filmResultSet = query.executeQuery();
            List<Film> films = new ArrayList<>();
            while (filmResultSet.next()) {
                films.add(createFilm(filmResultSet));
            }
            return films;
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public void addFilm(String name, int duration, int releaseYear, String description,
                        String url, List<Genre> genres, List<Country> countries) throws DatabaseException, ServiceException {
        try (PreparedStatement query = connection.prepareStatement(INSERT_FILM_QUERY)) {
            query.setString(1, name);
            query.setInt(2, duration);
            query.setInt(3, releaseYear);
            query.setString(4, description);
            query.setString(5, url);
            query.executeUpdate();
            addGenresToFilm(genres, url);
            addCountriesToFilm(countries, url);
        } catch (SQLException e) {
            throw new DatabaseException(e);
        } catch (CloneNotSupportedException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Genre> findAllGenres() throws DatabaseException {
        try (PreparedStatement query = connection.prepareStatement(FIND_ALL_GENRES_QUERY)) {
            ResultSet resultSet = query.executeQuery();
            List<Genre> genres = new ArrayList<>();
            while (resultSet.next()) {
                genres.add(createGenre(resultSet));
            }
            return genres;
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public List<Country> findAllCountries() throws DatabaseException {
        try (PreparedStatement query = connection.prepareStatement(FIND_ALL_COUNTRIES_QUERY)) {
            ResultSet resultSet = query.executeQuery();
            List<Country> countries = new ArrayList<>();
            while (resultSet.next()) {
                countries.add(createCountry(resultSet));
            }
            return countries;
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public Country findCountryByCode(String countryCode) throws DatabaseException {
        Country country = null;
        try (PreparedStatement query = connection.prepareStatement(FIND_COUNTRY_BY_CODE_QUERY)) {
            query.setString(1, countryCode);
            ResultSet resultSet = query.executeQuery();
            while (resultSet.next()) {
                country = createCountry(resultSet);
            }
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
        return country;
    }


    @Override
    public void updateRating(int filmId, double rating) throws DatabaseException {
        try (PreparedStatement query = connection.prepareStatement(UPDATE_RATING_QUERY)) {
            query.setDouble(1, rating);
            query.setInt(2, filmId);
            query.executeUpdate();
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public boolean checkFilmExistence(int id) throws DatabaseException {
        try (PreparedStatement query = connection.prepareStatement(CHECK_FILM_EXISTENCE)) {
            query.setInt(1, id);
            ResultSet resultSet = query.executeQuery();
            return resultSet.next() && resultSet.getBoolean(Constants.FLAG);
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public Film findFilmById(int id) throws DatabaseException {
        try (PreparedStatement query = connection.prepareStatement(FIND_FILM_BY_ID_QUERY)) {
            query.setInt(1, id);
            ResultSet resultSet = query.executeQuery();
            Film film = null;
            if (resultSet.next()) {
                film = createFilm(resultSet);
            }
            return film;
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public Genre findGenreById(int genreId) throws DatabaseException {
        Genre genre = null;
        try (PreparedStatement query = connection.prepareStatement(FIND_GENRE_BY_ID_QUERY)) {
            query.setInt(1, genreId);
            ResultSet resultSet = query.executeQuery();
            if (resultSet.next()) {
                genre = createGenre(resultSet);
            }
        } catch (SQLException e) {
            throw new DatabaseException();
        }
        return genre;
    }


    private void addGenresToFilm(List<Genre> genres, String url) throws CloneNotSupportedException, DatabaseException {
        int filmId = findFilmIdByUrl(url);
        for (Genre genre : genres) {
            try (PreparedStatement query = connection.prepareStatement(INSERT_FILMS_GENRES_QUERY)) {
                query.setInt(1, filmId);
                query.setInt(2, genre.getId());
                query.executeUpdate();
            } catch (SQLException e) {
                throw new DatabaseException(e);
            }
        }
    }

    private void addCountriesToFilm(List<Country> countries, String url) throws DatabaseException {
        int filmId = findFilmIdByUrl(url);
        for (Country country : countries) {
            try (PreparedStatement query = connection.prepareStatement(INSERT_FILMS_COUNTRIES_QUERY)) {
                query.setInt(1, filmId);
                query.setString(2, country.getCountryCode());
                query.executeUpdate();
            } catch (SQLException e) {
                throw new DatabaseException(e);
            }
        }
    }

    private int findFilmIdByUrl(String url) throws DatabaseException {
        try (PreparedStatement query = connection.prepareStatement(FIND_FILM_ID_BY_URL_QUERY)) {
            query.setString(1, url);
            ResultSet filmResultSet = query.executeQuery();
            if (filmResultSet.next()) {
                return filmResultSet.getInt(FilmsTable.ID.getName());
            } else {
                throw new DatabaseException();
            }
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }
}
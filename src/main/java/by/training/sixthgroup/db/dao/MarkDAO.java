package by.training.sixthgroup.db.dao;

import by.training.sixthgroup.exception.DatabaseException;
import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.Mark;
import by.training.sixthgroup.util.model.MarkFilmData;

import java.util.List;

public interface MarkDAO {

    /**
     * checks whether mark exist in db or doens't
     *
     * @param userId
     * @param filmId
     * @return
     * @throws DatabaseException
     */
    boolean checkMarkExistence(int userId, int filmId) throws DatabaseException;

    /**
     * adds mark to db
     *
     * @param mark
     * @return
     * @throws DatabaseException
     */
    boolean addMark(Mark mark) throws DatabaseException;

    /**
     * gets List of Mark placed for requested film
     *
     * @param filmId
     * @return
     * @throws DatabaseException
     * @throws ServiceException
     */
    List<Mark> findMarksByFilm(int filmId) throws DatabaseException, ServiceException;

    /**
     * gets special List of MarkFilmData objects that are used for displaying on user.jsp
     *
     * @param userId
     * @return
     * @throws DatabaseException
     */
    List<MarkFilmData> findMarksFilmsByUser(int userId) throws DatabaseException;

    /**
     * find mark of user to specific film
     *
     * @param userId
     * @param filmId
     * @return
     * @throws DatabaseException
     */
    Mark findMarkByUserAndFilm(int userId, int filmId) throws DatabaseException;

    /**
     * deletes mark based on filmId and userId
     *
     * @param filmId
     * @param userId
     * @throws DatabaseException
     */
    void deleteMark(int filmId, int userId) throws DatabaseException;

}

package by.training.sixthgroup.db.dao;

import by.training.sixthgroup.db.DatabaseType;
import by.training.sixthgroup.db.dao.mysql.MySQLDAOFactory;
import by.training.sixthgroup.db.pool.ConnectionPool;
import by.training.sixthgroup.db.pool.mysql.MySQLConnectionPool;
import by.training.sixthgroup.exception.DatabaseException;
import com.sun.prism.PixelFormat;

import java.sql.Connection;
import java.util.ResourceBundle;

public abstract class DAOFactory<T> {
    private static final String DB_PROPERTIES_FILE_NAME = "database";
    private static final String DB_TYPE_PROPERTY = "db.type";
    private static DatabaseType type;

    static {
        ResourceBundle bundle = ResourceBundle.getBundle(DB_PROPERTIES_FILE_NAME);
        String dbType = bundle.getString(DB_TYPE_PROPERTY);
        type = DatabaseType.forValue(dbType);
    }


    public static DAOFactory newInstance() {
        return DAOFactoryHolder.INSTANCE;
    }

    /**
     * creates an instance of specific DAOFactory.
     *
     * @return
     * @throws DatabaseException
     */
    private static DAOFactory getDAOFactory() throws DatabaseException {
        switch (type) {
            case MYSQL:
                return new MySQLDAOFactory();
            case ORACLE:
                throw new UnsupportedOperationException("Oracle DB is not supported yet");
            default:
                throw new EnumConstantNotPresentException(PixelFormat.DataType.class, String.valueOf(type));
        }
    }

    /**
     * returns userDA0 object
     *
     * @param connection
     * @return
     */
    public abstract UserDAO getUserDAO(Connection connection);


    /**
     * returns filmDA0 object
     *
     * @param connection
     * @return
     */
    public abstract FilmDAO getFilmDAO(Connection connection);

    /**
     * returns MarkDAO object
     *
     * @param connection
     * @return
     */
    public abstract MarkDAO getMarkDAO(Connection connection);


    /**
     * returns commentDA0 object
     *
     * @param connection
     * @return
     */
    public abstract CommentDAO getCommentDAO(Connection connection);

    /**
     * returns connection pool instance for specific type
     *
     * @return
     */
    public ConnectionPool getConnectionPool() {
        switch (type) {
            case MYSQL:
                return MySQLConnectionPool.newInstance();
            case ORACLE:
                throw new UnsupportedOperationException("Oracle DB is not supported yet");
            default:
                throw new EnumConstantNotPresentException(PixelFormat.DataType.class, String.valueOf(type));
        }
    }

    /**
     * holds INSTANCE of DAOFactory
     */
    private static class DAOFactoryHolder {
        private static final DAOFactory INSTANCE;

        static {
            try {
                INSTANCE = getDAOFactory();
            } catch (DatabaseException e) {
                throw new ExceptionInInitializerError(e);
            }
        }
    }
}

package by.training.sixthgroup.db.dao;

import by.training.sixthgroup.exception.DatabaseException;
import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.Country;
import by.training.sixthgroup.model.Film;
import by.training.sixthgroup.model.Genre;

import java.util.List;
import java.util.Map;

public interface FilmDAO {

    /**
     * gets Map(filmId, filmMame) found by pattern of filmName
     *
     * @param pattern
     * @return
     * @throws DatabaseException
     */
    Map<Integer, String> findFilmNamesByPattern(String pattern) throws DatabaseException;

    /**
     * finds films and sorts it. Moreover, fetches only limited amount based on offset and limit
     *
     * @param sortType
     * @param offset
     * @return
     * @throws DatabaseException
     * @throws ServiceException
     */
    List<Film> findSortedFilms(String sortType, int offset) throws DatabaseException, ServiceException;

    /**
     * gets List of Country objects by filmId
     *
     * @param id
     * @return
     * @throws DatabaseException
     */
    List<Country> findCountriesByFilm(int id) throws DatabaseException;

    /**
     * gets List of Genre objects by filmId
     *
     * @param id
     * @return
     * @throws DatabaseException
     */
    List<Genre> findGenresByFilm(int id) throws DatabaseException;

    /**
     * adds film to database
     *
     * @param name
     * @param duration
     * @param releaseYear
     * @param description
     * @param url
     * @param genres
     * @param countries
     * @throws DatabaseException
     * @throws ServiceException
     */
    void addFilm(String name, int duration, int releaseYear, String description,
                 String url, List<Genre> genres, List<Country> countries) throws DatabaseException, ServiceException;

    /**
     * gets list of all genres placed in database
     *
     * @return
     * @throws DatabaseException
     */
    List<Genre> findAllGenres() throws DatabaseException;

    /**
     * gets list of all countries placed in database
     *
     * @return
     * @throws DatabaseException
     */
    List<Country> findAllCountries() throws DatabaseException;

    /**
     * gets Genre object based on genreId
     *
     * @param genreId
     * @return
     * @throws DatabaseException
     */
    Genre findGenreById(int genreId) throws DatabaseException;

    /**
     * gets Country object based on countryCode
     *
     * @param countryCode
     * @return
     * @throws DatabaseException
     */
    Country findCountryByCode(String countryCode) throws DatabaseException;

    /**
     * updates rating of film
     *
     * @param filmId
     * @param rating
     * @throws DatabaseException
     */
    void updateRating(int filmId, double rating) throws DatabaseException;

    /**
     * checks whether films exists or doesn't
     *
     * @param id
     * @return
     * @throws DatabaseException
     */
    boolean checkFilmExistence(int id) throws DatabaseException;

    /**
     * gets Film object by id.
     *
     * @param id
     * @return
     * @throws DatabaseException
     */
    Film findFilmById(int id) throws DatabaseException;

}

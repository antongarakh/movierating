package by.training.sixthgroup.db.dao;

import by.training.sixthgroup.exception.DatabaseException;
import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.User;

import java.util.List;

public interface UserDAO {

    /**
     * finds user by his login which is unique
     *
     * @param login
     * @return
     * @throws DatabaseException
     */
    User findUser(String login) throws DatabaseException;

    /**
     * gets List of User objects of limited amount
     *
     * @param offset
     * @return
     * @throws DatabaseException
     */
    List<User> findLimitedUsers(int offset) throws DatabaseException;

    /**
     * finds user by his id which is unique
     *
     * @param id
     * @return
     * @throws DatabaseException
     */
    User findUser(int id) throws DatabaseException;

    /**
     * finds user by his login and crypted password
     *
     * @param login
     * @param password
     * @return
     * @throws DatabaseException
     */
    User findUser(String login, String password) throws DatabaseException;

    /**
     * adds user to database
     *
     * @param user
     * @throws DatabaseException
     */
    void addUser(User user) throws DatabaseException;

    /**
     * changes password of requested user
     *
     * @param userId
     * @param oldPassword
     * @param password
     * @throws DatabaseException
     */
    void changePassword(int userId, String oldPassword, String password) throws DatabaseException;

    /**
     * changes status of requested user
     *
     * @param userId
     * @param userStatus
     * @throws DatabaseException
     * @throws ServiceException
     */
    void changeStatus(int userId, String userStatus) throws DatabaseException, ServiceException;

    /**
     * sets expiration date for user which actually means setting ban
     *
     * @param userId
     * @param numberOfDays
     * @throws DatabaseException
     */
    void setBan(int userId, int numberOfDays) throws DatabaseException;

    /**
     * deletes expiration date for user which actually means resetting ban
     *
     * @param userId
     * @throws DatabaseException
     */
    void resetBan(int userId) throws DatabaseException;

    /**
     * checks whether user exists in db or doesn't
     *
     * @param id
     * @return
     * @throws DatabaseException
     */
    boolean checkUserExistence(int id) throws DatabaseException;


}
package by.training.sixthgroup.db.dao.mysql;

import by.training.sixthgroup.constant.Constants;
import by.training.sixthgroup.db.dao.MarkDAO;
import by.training.sixthgroup.db.table.FilmsTable;
import by.training.sixthgroup.db.table.MarksTable;
import by.training.sixthgroup.exception.DatabaseException;
import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.Mark;
import by.training.sixthgroup.util.model.MarkFilmData;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MySQLMarkDAO implements MarkDAO {

    private static final String FIND_MARK_BY_FILM_USER_QUERY = " SELECT * "
            + "FROM marks "
            + "WHERE user_id = ? "
            + "AND film_id = ? ";

    private static final String INSERT_MARK_QUERY = "INSERT INTO marks "
            + "(film_id, user_id, mark) "
            + "VALUES (?, ?, ?)";

    private static final String FIND_MARK_BY_FILM_ID_QUERY = "SELECT * "
            + "FROM marks "
            + "WHERE film_id = ? ";

    private static final String FIND_MARK_BY_USER_ID_QUERY = "SELECT * "
            + "FROM marks "
            + "WHERE user_id = ? ";

    private static final String FIND_MARKS_FILMS_BY_USER_ID_QUERY =
            "SELECT usermarks.mark, usermarks.film_id, films.name "
                    + "FROM (SELECT * "
                    + "FROM marks "
                    + "WHERE user_id = ?) "
                    + "AS usermarks "
                    + "INNER JOIN films "
                    + "ON usermarks.film_id = films.id ";

    private static final String DELETE_MARK_QUERY = "DELETE "
            + "FROM marks "
            + "WHERE marks.film_id = ? "
            + "AND marks.user_id = ? ";

    private static final String CHECK_MARK_EXISTENCE = "SELECT EXISTS( "
            + "SELECT 1 FROM marks "
            + "WHERE marks.user_id = ? "
            + "AND marks.film_id = ? ) AS flag ";

    private Connection connection;

    public MySQLMarkDAO(Connection connection) {
        this.connection = connection;
    }

    @Override
    public boolean checkMarkExistence(int userId, int filmId) throws DatabaseException {
        try (PreparedStatement query = connection.prepareStatement(CHECK_MARK_EXISTENCE)) {
            query.setInt(1, userId);
            query.setInt(2, filmId);
            ResultSet resultSet = query.executeQuery();
            return resultSet.next() && resultSet.getBoolean(Constants.FLAG);
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public boolean addMark(Mark mark) throws DatabaseException {
        boolean result;
        if (findMarkByUserAndFilm(mark.getUserId(), mark.getFilmId()) != null) {
            result = false;
        } else {
            try (PreparedStatement query = connection.prepareStatement(INSERT_MARK_QUERY)) {
                query.setInt(1, mark.getFilmId());
                query.setInt(2, mark.getUserId());
                query.setInt(3, mark.getMark());
                query.executeUpdate();
                result = true;
            } catch (SQLException e) {
                throw new DatabaseException(e);
            }
        }
        return result;
    }

    @Override
    public void deleteMark(int filmId, int userId) throws DatabaseException {
        try (PreparedStatement query = connection.prepareStatement(DELETE_MARK_QUERY)) {
            query.setInt(1, filmId);
            query.setInt(2, userId);
            query.executeUpdate();
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    private List<Mark> findMarksByIntParam(int param, String paramName) throws DatabaseException, ServiceException {
        String selectedQuery;
        switch (paramName) {
            case Constants.USER_ID:
                selectedQuery = FIND_MARK_BY_USER_ID_QUERY;
                break;
            case Constants.FILM_ID:
                selectedQuery = FIND_MARK_BY_FILM_ID_QUERY;
                break;
            default:
                throw new ServiceException();
        }
        try (PreparedStatement query = connection.prepareStatement(selectedQuery)) {
            query.setInt(1, param);
            ResultSet markResultSet = query.executeQuery();
            List<Mark> marks = new ArrayList<>();
            while (markResultSet.next()) {
                marks.add(createMark(markResultSet));
            }
            return marks;
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    private Mark createMark(ResultSet markResultSet) throws SQLException {
        int filmId = markResultSet.getInt(MarksTable.FILM_ID.getName());
        int userId = markResultSet.getInt(MarksTable.USER_ID.getName());
        int markValue = markResultSet.getInt(MarksTable.MARK.getName());
        Date date = markResultSet.getDate(MarksTable.DATE.getName());
        return new Mark(filmId, userId, markValue, date);
    }

    @Override
    public List<Mark> findMarksByFilm(int filmId) throws DatabaseException, ServiceException {
        return findMarksByIntParam(filmId, Constants.FILM_ID);
    }

    @Override
    public List<MarkFilmData> findMarksFilmsByUser(int userId) throws DatabaseException {
        try (PreparedStatement query = connection.prepareStatement(FIND_MARKS_FILMS_BY_USER_ID_QUERY)) {
            query.setInt(1, userId);
            ResultSet markResultSet = query.executeQuery();
            List<MarkFilmData> commentFilmList = new ArrayList<>();
            while (markResultSet.next()) {
                int mark = markResultSet.getInt(MarksTable.MARK.getName());
                String filmName = markResultSet.getString(FilmsTable.NAME.getName());
                int filmId = markResultSet.getInt(MarksTable.FILM_ID.getName());
                commentFilmList.add(new MarkFilmData(filmId, mark, filmName));
            }
            return commentFilmList;
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public Mark findMarkByUserAndFilm(int userId, int filmId) throws DatabaseException {
        try (PreparedStatement query = connection.prepareStatement(FIND_MARK_BY_FILM_USER_QUERY)) {
            query.setInt(1, userId);
            query.setInt(2, filmId);
            ResultSet markResultSet = query.executeQuery();
            Mark mark = null;
            if (markResultSet.next()) {
                mark = createMark(markResultSet);
            }
            return mark;
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }
}

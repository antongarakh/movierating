package by.training.sixthgroup.db.dao.mysql;

import by.training.sixthgroup.db.dao.CommentDAO;
import by.training.sixthgroup.db.table.CommentsTable;
import by.training.sixthgroup.db.table.FilmsTable;
import by.training.sixthgroup.db.table.UserTable;
import by.training.sixthgroup.exception.DatabaseException;
import by.training.sixthgroup.model.Comment;
import by.training.sixthgroup.util.model.CommentFilmData;
import by.training.sixthgroup.util.model.CommentUserData;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MySQLCommentDAO implements CommentDAO {

    private static final String INSERT_COMMENT_QUERY = "INSERT INTO comments "
            + "(film_id, user_id, comment_text) "
            + "VALUES (?, ?, ?)";


    private static final String FIND_COMMENTS_BY_USER_QUERY = "SELECT usercomments.comment_text, usercomments.id, films.name "
            + "FROM (SELECT * "
            + "FROM comments "
            + "WHERE user_id = ?) "
            + "AS usercomments "
            + "INNER JOIN films "
            + "ON usercomments.film_id = films.id ";

    private static final String FIND_COMMENTS_USERS_BY_FILM_QUERY = "SELECT film_comments.comment_text, film_comments.id, " +
            "users.username, users.id AS user_id, users.is_admin "
            + "FROM (SELECT * "
            + "FROM comments "
            + "WHERE film_id = ?) "
            + "AS film_comments "
            + "INNER JOIN users "
            + "ON film_comments.user_id = users.id "
            + "ORDER BY film_comments.comment_text DESC ";


    private static final String DELETE_COMMENT_QUERY = "DELETE "
            + "FROM comments "
            + "WHERE comments.id = ?";


    private static final String FIND_USER_BY_COMMENT_QUERY = "SELECT comments.user_id "
            + "FROM comments "
            + "WHERE comments.id = ? ";

    private Connection connection;

    public MySQLCommentDAO(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void addComment(Comment comment) throws DatabaseException {
        try (PreparedStatement query = connection.prepareStatement(INSERT_COMMENT_QUERY)) {
            query.setInt(1, comment.getFilmId());
            query.setInt(2, comment.getUserId());
            query.setString(3, comment.getComment());
            query.executeUpdate();
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }


    @Override
    public List<CommentFilmData> findCommentsByUser(int userId) throws DatabaseException {
        try (PreparedStatement query = connection.prepareStatement(FIND_COMMENTS_BY_USER_QUERY)) {
            query.setInt(1, userId);
            ResultSet commentResultSet = query.executeQuery();
            List<CommentFilmData> commentFilmList = new ArrayList<>();
            while (commentResultSet.next()) {
                String comment = commentResultSet.getString(CommentsTable.COMMENT_TEXT.getName());
                String filmName = commentResultSet.getString(FilmsTable.NAME.getName());
                int commentId = commentResultSet.getInt(CommentsTable.ID.getName());
                commentFilmList.add(new CommentFilmData(commentId, comment, filmName, userId));
            }
            return commentFilmList;
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public List<CommentUserData> findCommentsByFilm(int filmId) throws DatabaseException {
        try (PreparedStatement query = connection.prepareStatement(FIND_COMMENTS_USERS_BY_FILM_QUERY)) {
            query.setInt(1, filmId);
            ResultSet commentResultSet = query.executeQuery();
            List<CommentUserData> commentUserList = new ArrayList<>();
            while (commentResultSet.next()) {
                String comment = commentResultSet.getString(CommentsTable.COMMENT_TEXT.getName());
                String userName = commentResultSet.getString(UserTable.USERNAME.getName());
                int userId = commentResultSet.getInt(CommentsTable.USER_ID.getName());
                int commentId = commentResultSet.getInt(CommentsTable.ID.getName());
                boolean isAdmin = commentResultSet.getBoolean(UserTable.IS_ADMIN.getName());
                commentUserList.add(new CommentUserData(comment, userName, userId, commentId, isAdmin));
            }
            return commentUserList;
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public int findUserIdByCommentId(int commentId) throws DatabaseException {
        try (PreparedStatement query = connection.prepareStatement(FIND_USER_BY_COMMENT_QUERY)) {
            query.setInt(1, commentId);
            ResultSet commentResultSet = query.executeQuery();
            if (commentResultSet.next()) {
                return commentResultSet.getInt(CommentsTable.USER_ID.getName());
            } else {
                throw new DatabaseException();
            }
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public void deleteComment(int commentId) throws DatabaseException {
        try (PreparedStatement query = connection.prepareStatement(DELETE_COMMENT_QUERY)) {
            query.setInt(1, commentId);
            query.executeUpdate();
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    private Comment createComment(ResultSet commentResultSet) throws SQLException {
        int id = commentResultSet.getInt(CommentsTable.ID.getName());
        int filmId = commentResultSet.getInt(CommentsTable.FILM_ID.getName());
        int userId = commentResultSet.getInt(CommentsTable.USER_ID.getName());
        String commentText = commentResultSet.getString(CommentsTable.COMMENT_TEXT.getName());
        Date date = commentResultSet.getDate(CommentsTable.DATE.getName());
        return new Comment(commentText, id, date, filmId, userId);
    }
}
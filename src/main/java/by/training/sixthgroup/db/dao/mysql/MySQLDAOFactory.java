package by.training.sixthgroup.db.dao.mysql;

import by.training.sixthgroup.db.dao.*;

import java.sql.Connection;

public class MySQLDAOFactory extends DAOFactory<Connection> {

    @Override
    public UserDAO getUserDAO(Connection connection) {
        return new MySQLUserDAO(connection);
    }

    @Override
    public FilmDAO getFilmDAO(Connection connection) {
        return new MySQLFilmDAO(connection);
    }

    @Override
    public MarkDAO getMarkDAO(Connection connection) {
        return new MySQLMarkDAO(connection);
    }

    @Override
    public CommentDAO getCommentDAO(Connection connection) {
        return new MySQLCommentDAO(connection);
    }

}

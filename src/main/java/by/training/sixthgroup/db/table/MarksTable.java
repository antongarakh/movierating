package by.training.sixthgroup.db.table;


public enum MarksTable {

    FILM_ID("film_id"),
    USER_ID("user_id"),
    MARK("mark"),
    DATE("date");

    private String name;

    MarksTable(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

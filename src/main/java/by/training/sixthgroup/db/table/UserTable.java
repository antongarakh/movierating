package by.training.sixthgroup.db.table;

public enum UserTable {

    ID("id"),
    IS_ADMIN("is_admin"),
    USERNAME("username"),
    PASSWORD("password"),
    STATUS("status"),
    EXPIRATION_DATE("expiration_date");


    private String name;

    UserTable(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

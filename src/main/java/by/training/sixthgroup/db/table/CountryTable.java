package by.training.sixthgroup.db.table;

public enum CountryTable {

    COUNTRY_CODE("country_code"),
    COUNTRY_NAME("country_name");

    private String name;

    CountryTable(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

package by.training.sixthgroup.db.table;

public enum GenresTable {

    ID("id"),
    GENRE_NAME("genre_name");

    private String name;

    GenresTable(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

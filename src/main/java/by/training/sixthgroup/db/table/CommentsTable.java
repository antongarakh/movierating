package by.training.sixthgroup.db.table;

public enum CommentsTable {

    ID("id"),
    FILM_ID("film_id"),
    USER_ID("user_id"),
    COMMENT_TEXT("comment_text"),
    DATE("date");

    private String name;

    CommentsTable(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

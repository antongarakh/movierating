package by.training.sixthgroup.db.table;

public enum FilmsTable {

    ID("id"),
    DURATION("duration"),
    NAME("name"),
    RATING("rating"),
    DESCRIPTION("description"),
    RELEASE_YEAR("release_year"),
    URL("url");

    private String name;

    FilmsTable(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}


package by.training.sixthgroup.filter;

import by.training.sixthgroup.constant.Constants;
import by.training.sixthgroup.constant.PageType;
import by.training.sixthgroup.constant.RequestAttributeType;
import by.training.sixthgroup.constant.SessionAttributeType;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Anton on 08.10.2016.
 */
public class LocaleFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        Object localeObject = request.getSession().getAttribute(SessionAttributeType.LOCALE.getName());
        if (localeObject != null) {
            String locale = (String) localeObject;
            if (locale.equals(Constants.LOCALE_EN) || locale.equals(Constants.LOCALE_RU)) {
                filterChain.doFilter(servletRequest, servletResponse);
            } else {
                request.getSession().setAttribute(SessionAttributeType.LOCALE.getName(), Constants.LOCALE_EN);
                String page = PageType.ERROR.getPageName();
                request.setAttribute(RequestAttributeType.ERROR_CODE.getName(), Constants.ERROR_BAD_REQUEST_CODE);
                request.setAttribute(RequestAttributeType.ERROR_MESSAGE.getName(), Constants.ERROR_BAD_REQUEST);
                RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher(page);
                dispatcher.forward(request, response);
            }
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {

    }
}

package by.training.sixthgroup.filter;

import javax.servlet.*;
import java.io.IOException;

/**
 * specifies encoding to the one from filter init parameter
 */
public class EncodingFilter implements Filter {

    private static final String ENCODING_INIT_PARAMETER = "encoding";

    private String encoding;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        encoding = filterConfig.getInitParameter(ENCODING_INIT_PARAMETER);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String currentEncoding = servletRequest.getCharacterEncoding();
        if (currentEncoding == null || !encoding.equalsIgnoreCase(currentEncoding)) {
            servletRequest.setCharacterEncoding(encoding);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}

package by.training.sixthgroup.filter;

import by.training.sixthgroup.constant.Constants;
import by.training.sixthgroup.constant.PageType;
import by.training.sixthgroup.constant.RequestAttributeType;
import by.training.sixthgroup.util.manager.PathManager;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * filter which intercepts bad requests that actually mean t
 */
public class BadURIFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String requestUri = request.getRequestURI();
        if (!(requestUri.startsWith("/controller?")
                || requestUri.equals("/controller")
                || requestUri.startsWith("/img/")
                || requestUri.startsWith("/js/")
                || requestUri.startsWith("/css/")
                || requestUri.startsWith("/jsp/")
                || requestUri.startsWith("/fonts/")
                || requestUri.equals("/uploader")
                || requestUri.startsWith("/uploader?")
                || requestUri.startsWith("/bower_components/")
                || requestUri.equals("/favicon.ico")
                || requestUri.equals("/"))) {
            String page = PathManager.getPagePath(PageType.ERROR.getPageName());
            request.setAttribute(RequestAttributeType.ERROR_CODE.getName(), Constants.ERROR_NOT_FOUND_CODE);
            request.setAttribute(RequestAttributeType.ERROR_MESSAGE.getName(), Constants.ERROR_NOT_FOUND);
            RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher(page);
            dispatcher.forward(request, response);
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {
    }
}

package by.training.sixthgroup.filter;

import by.training.sixthgroup.constant.Constants;
import by.training.sixthgroup.constant.PageType;
import by.training.sixthgroup.constant.RequestParameterType;
import by.training.sixthgroup.constant.SessionAttributeType;
import by.training.sixthgroup.model.User;
import by.training.sixthgroup.util.manager.PathManager;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter which denies access for simple users to admin-only commands
 */
public class AdminAccessFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        User user = (User) request.getSession().getAttribute(SessionAttributeType.USER.getName());
        Boolean isAdmin = null;
        if (user != null) {
            isAdmin = user.getIsAdmin();
        }
        checkCommand(request, response, isAdmin);
        filterChain.doFilter(request, response);
    }

    private void checkCommand(HttpServletRequest request, HttpServletResponse response, Boolean isAdmin)
            throws ServletException, IOException {
        String command = request.getParameter(RequestParameterType.COMMAND.getName());
        String page = PathManager.getPagePath(PageType.HOME.getPageName());
        RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher(page);
        if (command != null) {
            if (isAdmin == null || !isAdmin) {
                switch (command) {
                    case Constants.USERS:
                    case Constants.ADD_FILM:
                    case Constants.LOAD_FILM:
                        dispatcher.forward(request, response);
                }
            }
            if (isAdmin == null) {
                if (command.equals(Constants.LOGOUT))
                    dispatcher.forward(request, response);
            } else if (!isAdmin) {
                switch (command) {
                    case Constants.LOGIN:
                    case Constants.REGISTER:
                        dispatcher.forward(request, response);
                }
            }
        }
    }

    @Override
    public void destroy() {
    }
}
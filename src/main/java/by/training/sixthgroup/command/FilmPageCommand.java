package by.training.sixthgroup.command;

import by.training.sixthgroup.constant.*;
import by.training.sixthgroup.controller.SessionRequestContent;
import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.Film;
import by.training.sixthgroup.model.Mark;
import by.training.sixthgroup.model.User;
import by.training.sixthgroup.service.UserService;
import by.training.sixthgroup.util.manager.PathManager;
import by.training.sixthgroup.validation.FilmValidator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.List;


public class FilmPageCommand implements Command {

    public static final Logger logger = LogManager.getLogger(FilmPageCommand.class);

    @Override
    public String execute(SessionRequestContent content) {
        logger.info("FilmPageCommand performing...");
        String page = null;
        UserService userService = new UserService();
        try {
            userService.initialize();
            String filmId = content.getRequestParameter(RequestParameterType.ID.getName());
            FilmValidator filmValidator = new FilmValidator(filmId, userService);
            if (filmValidator.checkValidity()) {
                int filmIdValue = Integer.parseInt(filmId);
                Film film = userService.findFilmById(filmIdValue);
                content.setRequestAttribute(RequestAttributeType.FILM.getName(), film);
                logger.info("Next page is " + film.getName() + "page");
                content.setSessionAttribute(SessionAttributeType.FILM_ID.getName(), filmId);
                User user = (User) content.getSessionAttribute(SessionAttributeType.USER.getName());
                if (user != null) {
                    Mark mark = userService.findMarkByUserAndFilm(user.getId(), filmIdValue);
                    content.setRequestAttribute(RequestAttributeType.MARK.getName(), mark);
                }
                prepareFilmPage(userService, content, filmIdValue);
                content.setSessionAttribute(SessionAttributeType.URI.getName(), userService.getUriOfRequest(content));
                page = PathManager.getPagePath(PageType.FILM.getPageName());
            } else {
                logger.error("Validation Error");
                content.setRequestAttribute(RequestAttributeType.ERROR_MESSAGE_KEY.getName(), filmValidator.getErrorMessageKey());
                page = userService.prepareErrorPage(content, ErrorType.BAD_REQUEST);
            }
        } catch (ServiceException e) {
            logger.error(e);
            page = userService.prepareErrorPage(content, ErrorType.INTERNAL_SERVER);
        } finally {
            userService.releaseConnection();
        }
        return page;
    }

    private void prepareFilmPage(UserService userService, SessionRequestContent content, int filmId) throws ServiceException {
        List genres = userService.findGenresByFilm(filmId);
        List comments = userService.findCommentsByFilm(filmId);
        List countries = userService.findCountriesByFilm(filmId);
        content.setRequestAttribute(RequestAttributeType.GENRES.getName(), genres);
        content.setRequestAttribute(RequestAttributeType.COUNTRIES.getName(), countries);
        content.setRequestAttribute(RequestAttributeType.COMMENTS.getName(), comments);
    }
}

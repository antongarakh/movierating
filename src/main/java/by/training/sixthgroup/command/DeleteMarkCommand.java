package by.training.sixthgroup.command;

import by.training.sixthgroup.constant.*;
import by.training.sixthgroup.controller.SessionRequestContent;
import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.User;
import by.training.sixthgroup.service.UserService;
import by.training.sixthgroup.util.manager.PathManager;
import by.training.sixthgroup.validation.DeleteMarkValidator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.Objects;

/**
 * Invokes when admin click on delete mark button, prepares user.jsp
 */
public class DeleteMarkCommand implements Command {

    public static final Logger logger = LogManager.getLogger(ChangeStatusCommand.class);
    private static final String DELETE_SUCCESS_MESSAGE_KEY = "user.delete.mark";

    @Override
    public String execute(SessionRequestContent content) {
        logger.info("DeleteMarkCommand performing...");
        String page = null;
        UserService userService = new UserService();
        try {
            userService.initialize();
            String filmId = content.getRequestParameter(RequestParameterType.FILM_ID.getName());
            String userId = content.getRequestParameter(RequestParameterType.USER_ID.getName());
            User sessionUser = (User) content.getSessionAttribute(SessionAttributeType.USER.getName());
            DeleteMarkValidator validator = new DeleteMarkValidator(userId, filmId, sessionUser, userService);
            if (validator.checkValidity()) {
                userService.deleteMark(Integer.parseInt(filmId), Integer.parseInt(userId));
                userService.updateRating(Integer.parseInt(filmId));
                logger.info("Mark with userId = " + userId + " and filmId = " + filmId + " deleted");
                User user;
                if (!Objects.equals(userId, "")) {
                    user = userService.findUser(Integer.parseInt(userId));
                } else {
                    user = (User) content.getSessionAttribute(SessionAttributeType.USER.getName());
                }
                prepareUserPage(content, userService, user);
                page = PathManager.getPagePath(PageType.USER.getPageName());
            } else {
                logger.error("Validation Error");
                content.setRequestAttribute(RequestAttributeType.ERROR_MESSAGE_KEY.getName(), validator.getErrorMessageKey());
                page = userService.prepareErrorPage(content, ErrorType.BAD_REQUEST);
            }
        } catch (ServiceException e) {
            logger.error(e);
            page = userService.prepareErrorPage(content, ErrorType.INTERNAL_SERVER);
        } finally {
            userService.releaseConnection();
        }
        return page;
    }

    private void prepareUserPage(SessionRequestContent content, UserService userService, User user) throws ServiceException {
        content.setRequestAttribute(RequestAttributeType.MARKS_FILMS.getName(), userService.findMarksFilmsByUser(user.getId()));
        content.setRequestAttribute(RequestAttributeType.COMMENTS_FILMS.getName(), userService.findCommentsByUser(user.getId()));
        content.setRequestAttribute(RequestAttributeType.OTHER_USER.getName(), user);
        content.setSessionAttribute(SessionAttributeType.MESSAGE_SUCCESS.getName(), DELETE_SUCCESS_MESSAGE_KEY);
    }
}
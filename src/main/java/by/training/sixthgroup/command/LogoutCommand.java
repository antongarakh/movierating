package by.training.sixthgroup.command;

import by.training.sixthgroup.constant.PageType;
import by.training.sixthgroup.controller.SessionRequestContent;
import by.training.sixthgroup.util.manager.PathManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * invokes when user click on logout button. forwards to home.jsp and invalidates Session.
 */
public class LogoutCommand implements Command {

    public static final Logger logger = LogManager.getLogger(LogoutCommand.class);

    @Override
    public String execute(SessionRequestContent content) {
        logger.info("LogoutCommand performing...");
        content.invalidateSession();
        return PathManager.getPagePath(PageType.HOME.getPageName());
    }
}

package by.training.sixthgroup.command;

public class CommandFactory {


    /**
     * creates and returns appropriate command using @param commandName and CommandEnum class
     *
     * @param commandName
     * @return
     */
    public Command createCommand(String commandName) {
        Command command;
        CommandEnum commandType = CommandEnum.forValue(commandName);
        if (commandType != null) {
            command = commandType.getCommand();
        } else {
            command = new EmptyCommand();
        }
        return command;
    }


}
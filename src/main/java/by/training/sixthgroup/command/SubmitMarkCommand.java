package by.training.sixthgroup.command;

import by.training.sixthgroup.constant.ErrorType;
import by.training.sixthgroup.constant.RequestAttributeType;
import by.training.sixthgroup.constant.RequestParameterType;
import by.training.sixthgroup.constant.SessionAttributeType;
import by.training.sixthgroup.controller.SessionRequestContent;
import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.Film;
import by.training.sixthgroup.model.Mark;
import by.training.sixthgroup.model.User;
import by.training.sixthgroup.service.UserService;
import by.training.sixthgroup.validation.SubmitMarkValidator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * invokes every time users submits mark form at film.jsp
 */
public class SubmitMarkCommand implements Command {

    public static final Logger logger = LogManager.getLogger(SubmitMarkCommand.class);
    private static final String SUBMIT_SUCCESS_MESSAGE_KEY = "user.mark.success";

    @Override
    public String execute(SessionRequestContent content) {
        logger.info("SubmitMarkCommand performing...");
        String page = null;
        UserService userService = new UserService();
        try {
            userService.initialize();
            String mark = content.getRequestParameter(RequestParameterType.SUBMIT_MARK.getName());
            User user = (User) content.getSessionAttribute(SessionAttributeType.USER.getName());
            String filmId = (String) content.getSessionAttribute(SessionAttributeType.FILM_ID.getName());
            int filmIdValue = Integer.parseInt(filmId);
            SubmitMarkValidator markValidator = new SubmitMarkValidator(mark, user, filmId, userService);
            if (markValidator.checkValidity()) {
                submitMark(Integer.parseInt(mark), user.getId(), filmIdValue, userService);
                Film film = userService.findFilmById(filmIdValue);
                content.setRequestAttribute(RequestAttributeType.FILM.getName(), film);
                content.setSessionAttribute(SessionAttributeType.MESSAGE_SUCCESS.getName(), SUBMIT_SUCCESS_MESSAGE_KEY);
                content.setRequestAttribute(RequestAttributeType.SEND_REDIRECT.getName(), Boolean.TRUE);
                logger.info("Mark <" + mark + "> for movie #" + filmId + " was successfully submitted");
                page = (String) content.getSessionAttribute(SessionAttributeType.URI.getName());
            } else {
                logger.error("Validation Error");
                content.setRequestAttribute(RequestAttributeType.ERROR_MESSAGE_KEY.getName(), markValidator.getErrorMessageKey());
                page = userService.prepareErrorPage(content, ErrorType.BAD_REQUEST);
            }
        } catch (ServiceException e) {
            logger.error(e);
            page = userService.prepareErrorPage(content, ErrorType.INTERNAL_SERVER);
        } finally {
            userService.releaseConnection();
        }

        return page;
    }

    private void submitMark(int markValue, int userId, int filmId, UserService userService) throws ServiceException {
        Mark mark = new Mark();
        mark.setMark(markValue);
        mark.setFilmId(filmId);
        mark.setUserId(userId);
        userService.addMark(mark);
        userService.updateRating(filmId);
    }


}

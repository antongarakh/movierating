package by.training.sixthgroup.command;

import by.training.sixthgroup.controller.SessionRequestContent;
import by.training.sixthgroup.service.UserService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * invokes when user tries to change language.
 */
public class RefreshCommand implements Command {

    public static final Logger logger = LogManager.getLogger(RefreshCommand.class);

    @Override
    public String execute(SessionRequestContent content) {
        logger.info("RefreshCommand performing...");
        UserService service = new UserService();
        return service.getPageName(content);
    }
}

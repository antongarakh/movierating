package by.training.sixthgroup.command;

import by.training.sixthgroup.constant.RequestParameterType;
import by.training.sixthgroup.controller.SessionRequestContent;
import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.service.UserService;
import by.training.sixthgroup.util.Util;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Map;

/**
 * invokes every time user types something on search bar
 */
public class SearchCommand implements Command {

    public static final Logger logger = LogManager.getLogger(SearchCommand.class);

    @Override
    public String execute(SessionRequestContent content) {
        logger.info("SearchCommand performing...");
        UserService userService = new UserService();
        try {
            userService.initialize();
            String term = content.getRequestParameter(RequestParameterType.TERM.getName());
            Map filmList = userService.findNamesByPattern(term);
            logger.info("Films response on requested pattern " + term + " - " + filmList);
            String jsonInString = Util.objectToJSONString(filmList);
            content.writeResponse(jsonInString);
        } catch (IOException | ServiceException e) {
            logger.error(e);
        } finally {
            userService.releaseConnection();
        }
        return userService.getPageName(content);
    }
}


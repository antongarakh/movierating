package by.training.sixthgroup.command;

import java.util.HashMap;
import java.util.Map;

/**
 * Enumeration of commands for CommandFactory
 */
public enum CommandEnum {

    LOGIN(new LoginCommand()),
    LOGOUT(new LogoutCommand()),
    REGISTER(new RegisterCommand()),
    FILMS(new FilmsCommand()),
    FILM_PAGE(new FilmPageCommand()),
    FORWARD(new ForwardCommand()),
    SUBMIT_MARK(new SubmitMarkCommand()),
    SUBMIT_COMMENT(new SubmitCommentCommand()),
    USER(new UserCommand()),
    DELETE_COMMENT(new DeleteCommentCommand()),
    DELETE_MARK(new DeleteMarkCommand()),
    EDIT_PROFILE(new EditProfileCommand()),
    ADMIN(new AdminCommand()),
    USERS(new UsersCommand()),
    CHANGE_STATUS(new ChangeStatusCommand()),
    BAN(new BanCommand()),
    ADD_FILM(new AddFilmPageCommand()),
    LOAD_FILM(new LoadFilmCommand()),
    REFRESH(new RefreshCommand()),
    SEARCH(new SearchCommand());

    private static final Map<String, CommandEnum> nameToValueMap = new HashMap<>();


    static {
        for (CommandEnum value : CommandEnum.values()) {
            nameToValueMap.put(value.name(), value);
        }
    }

    private Command command;

    CommandEnum(Command command) {
        this.command = command;
    }

    public static CommandEnum forValue(String commandName) {
        if (commandName != null) {
            return nameToValueMap.get(commandName.toUpperCase());
        }
        return null;
    }

    public Command getCommand() {
        return command;
    }
}

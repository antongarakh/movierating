package by.training.sixthgroup.command;

import by.training.sixthgroup.constant.ErrorType;
import by.training.sixthgroup.constant.RequestAttributeType;
import by.training.sixthgroup.constant.RequestParameterType;
import by.training.sixthgroup.constant.SessionAttributeType;
import by.training.sixthgroup.controller.SessionRequestContent;
import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.Comment;
import by.training.sixthgroup.model.Film;
import by.training.sixthgroup.model.User;
import by.training.sixthgroup.service.UserService;
import by.training.sixthgroup.validation.SubmitCommentValidator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * invokes every time users submits comment form at film.jsp
 */
public class SubmitCommentCommand implements Command {

    public static final Logger logger = LogManager.getLogger(SubmitCommentCommand.class);
    private static final String SUBMIT_SUCCESS_MESSAGE_KEY = "user.comment.success";

    @Override
    public String execute(SessionRequestContent content) {
        logger.info("SubmitCommentCommand performing...");
        String page = null;
        UserService userService = new UserService();
        try {
            userService.initialize();
            String comment = content.getRequestParameter(RequestParameterType.SUBMIT_COMMENT.getName());
            User user = (User) content.getSessionAttribute(SessionAttributeType.USER.getName());
            String filmId = (String) content.getSessionAttribute(SessionAttributeType.FILM_ID.getName());
            SubmitCommentValidator commentValidator = new SubmitCommentValidator(comment, user, filmId, userService);
            if (commentValidator.checkValidity()) {
                int filmIdValue = Integer.parseInt(filmId);
                createComment(comment, user.getId(), filmIdValue, userService);
                Film film = userService.findFilmById(filmIdValue);
                content.setRequestAttribute(RequestAttributeType.FILM.getName(), film);
                content.setRequestAttribute(RequestAttributeType.SEND_REDIRECT.getName(), Boolean.TRUE);
                content.setSessionAttribute(SessionAttributeType.MESSAGE_SUCCESS.getName(), SUBMIT_SUCCESS_MESSAGE_KEY);
                logger.info("Comment <" + comment + "> for movie #" + filmId + " was successfully submitted");
                page = (String) content.getSessionAttribute(SessionAttributeType.URI.getName());
            } else {
                logger.error("Validation Error");
                content.setRequestAttribute(RequestAttributeType.ERROR_MESSAGE_KEY.getName(), commentValidator.getErrorMessageKey());
                page = userService.prepareErrorPage(content, ErrorType.BAD_REQUEST);
            }
        } catch (ServiceException e) {
            logger.error(e);
            page = userService.prepareErrorPage(content, ErrorType.INTERNAL_SERVER);
        } finally {
            userService.releaseConnection();
        }
        return page;
    }

    private void createComment(String commentText, int userId, int filmId, UserService userService) throws ServiceException {
        Comment comment = new Comment();
        comment.setComment(commentText);
        comment.setFilmId(filmId);
        comment.setUserId(userId);
        userService.addComment(comment);
    }


}

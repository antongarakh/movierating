package by.training.sixthgroup.command;

import by.training.sixthgroup.constant.ErrorType;
import by.training.sixthgroup.constant.PageType;
import by.training.sixthgroup.constant.RequestAttributeType;
import by.training.sixthgroup.constant.SessionAttributeType;
import by.training.sixthgroup.controller.SessionRequestContent;
import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.service.AdminService;
import by.training.sixthgroup.util.Util;
import by.training.sixthgroup.util.manager.PathManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * Prepares add_film.jsp
 */
public class AddFilmPageCommand implements Command {

    public static final Logger logger = LogManager.getLogger(AddFilmPageCommand.class);


    @Override
    public String execute(SessionRequestContent content) {
        logger.info("AddFilmPageCommand performing...");
        String page = null;
        AdminService adminService = new AdminService();
        try {
            adminService.initialize();
            if (content.getSessionAttribute(SessionAttributeType.ALL_COUNTRIES.getName()) == null) {
                content.setSessionAttribute(SessionAttributeType.ALL_COUNTRIES.getName(), adminService.findAllCountries());
            }
            if (content.getSessionAttribute(SessionAttributeType.ALL_GENRES.getName()) == null) {
                content.setSessionAttribute(SessionAttributeType.ALL_GENRES.getName(), adminService.findAllGenres());
            }
            if (content.getSessionAttribute(SessionAttributeType.ALL_YEARS.getName()) == null) {
                content.setSessionAttribute(SessionAttributeType.ALL_YEARS.getName(), Util.getListOfYears(1901, 2016));
            }
            copyInfoToRequest(content);
            content.setSessionAttribute(SessionAttributeType.URI.getName(), adminService.getUriOfRequest(content));
            page = PathManager.getPagePath(PageType.ADD_FILM.getPageName());
        } catch (ServiceException e) {
            logger.error(e);
            page = adminService.prepareErrorPage(content, ErrorType.INTERNAL_SERVER);
        } finally {
            adminService.releaseConnection();
        }
        return page;
    }

    private void copyInfoToRequest(SessionRequestContent content) {
        List genres = (List) content.getSessionAttribute(SessionAttributeType.ALL_GENRES.getName());
        List countries = (List) content.getSessionAttribute(SessionAttributeType.ALL_COUNTRIES.getName());
        List years = (List) content.getSessionAttribute(SessionAttributeType.ALL_YEARS.getName());
        content.setRequestAttribute(RequestAttributeType.ALL_GENRES.getName(), genres);
        content.setRequestAttribute(RequestAttributeType.ALL_COUNTRIES.getName(), countries);
        content.setRequestAttribute(RequestAttributeType.ALL_YEARS.getName(), years);
    }


}


package by.training.sixthgroup.command;

import by.training.sixthgroup.constant.*;
import by.training.sixthgroup.controller.SessionRequestContent;
import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.User;
import by.training.sixthgroup.service.AdminService;
import by.training.sixthgroup.util.manager.PathManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * prepares users.jsp when in admin mode.
 */
public class UsersCommand implements Command {

    public static final Logger logger = LogManager.getLogger(UsersCommand.class);

    @Override
    public String execute(SessionRequestContent content) {
        logger.info("UsersCommand performing...");
        String page = null;
        AdminService adminService = new AdminService();
        try {
            adminService.initialize();
            String offset = content.getRequestParameter(RequestParameterType.USERS_OFFSET.getName());
            offset = adminService.processOffset(offset, PageType.USERS, content);
            if (adminService.checkOffsetAppropriate(offset)) {
                List<User> users = adminService.findLimitedUsers(Integer.parseInt(offset));
                content.setRequestAttribute(RequestAttributeType.USERS.getName(), users);
                content.setSessionAttribute(SessionAttributeType.USERS_OFFSET.getName(), offset);
                content.setSessionAttribute(SessionAttributeType.URI.getName(), adminService.getUriOfRequest(content));
                page = PathManager.getPagePath(PageType.USERS.getPageName());
            } else {
                logger.error("Validation Error");
                page = adminService.prepareErrorPage(content, ErrorType.BAD_REQUEST);
            }
        } catch (ServiceException e) {
            logger.error(e);
            page = adminService.prepareErrorPage(content, ErrorType.INTERNAL_SERVER);
        } finally {
            adminService.releaseConnection();
        }
        return page;
    }
}

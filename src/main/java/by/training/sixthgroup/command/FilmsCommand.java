package by.training.sixthgroup.command;

import by.training.sixthgroup.constant.*;
import by.training.sixthgroup.controller.SessionRequestContent;
import by.training.sixthgroup.exception.RequestParameterException;
import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.Film;
import by.training.sixthgroup.service.UserService;
import by.training.sixthgroup.util.manager.PathManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * Prepares films.jsp
 */
public class FilmsCommand implements Command {

    public static final Logger logger = LogManager.getLogger(FilmsCommand.class);

    @Override
    public String execute(SessionRequestContent content) {
        logger.info("FilmsCommand performing...");
        String page = null;
        UserService userService = new UserService();
        try {
            userService.initialize();
            String offset = content.getRequestParameter(RequestParameterType.FILMS_OFFSET.getName());
            String sortTypeValue = content.getRequestParameter(RequestParameterType.SORT_TYPE.getName());
            if (sortTypeValue == null) {
                sortTypeValue = (String) content.getSessionAttribute(SessionAttributeType.FILMS_SORT_TYPE.getName());
            }
            List<Film> films;
            SortType sortType = SortType.getType(sortTypeValue);
            offset = userService.processOffset(offset, PageType.FILMS, content);
            if (!userService.checkOffsetAppropriate(offset)) {
                throw new RequestParameterException();
            } else {
                content.setSessionAttribute(SessionAttributeType.FILMS_SORT_TYPE.getName(), sortType.getName());
            }
            films = userService.findSortedFilms(sortType.getName(), Integer.parseInt(offset));
            content.setRequestAttribute(RequestAttributeType.FILMS.getName(), films);
            content.setSessionAttribute(SessionAttributeType.FILMS_OFFSET.getName(), offset);
            content.setSessionAttribute(SessionAttributeType.URI.getName(), userService.getUriOfRequest(content));
            page = PathManager.getPagePath(PageType.FILMS.getPageName());
        } catch (ServiceException e) {
            logger.error(e);
            page = userService.prepareErrorPage(content, ErrorType.INTERNAL_SERVER);
        } catch (RequestParameterException e) {
            logger.error(e);
            page = userService.prepareErrorPage(content, ErrorType.BAD_REQUEST);
        } finally {
            userService.releaseConnection();
        }

        return page;
    }


}

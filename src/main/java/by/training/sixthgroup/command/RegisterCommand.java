package by.training.sixthgroup.command;

import by.training.sixthgroup.constant.*;
import by.training.sixthgroup.controller.SessionRequestContent;
import by.training.sixthgroup.exception.RequestParameterException;
import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.SimpleUser;
import by.training.sixthgroup.model.UserStatus;
import by.training.sixthgroup.service.UserService;
import by.training.sixthgroup.util.encrypter.SALTEncrypter;
import by.training.sixthgroup.util.manager.PathManager;
import by.training.sixthgroup.validation.RegistrationValidator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * invokes when user submits registration form. if successfully - redirects to login.jsp, if not - stays on current page register.jsp
 */
public class RegisterCommand implements Command {

    public static final Logger logger = LogManager.getLogger(RegisterCommand.class);

    @Override
    public String execute(SessionRequestContent content) {
        logger.info("RegisterCommand performing...");
        String page = null;
        UserService userService = new UserService();

        try {
            userService.initialize();
            String login = content.getRequestParameter(RequestParameterType.LOGIN.getName());
            String password = content.getRequestParameter(RequestParameterType.PASSWORD.getName());
            String confirmedPassword = content.getRequestParameter(RequestParameterType.CONFIRM_PASSWORD.getName());
            logger.info(login + " " + password);
            RegistrationValidator registrationValidator = new RegistrationValidator(login, password, confirmedPassword, userService);
            if (registrationValidator.checkValidity()) {
                registerNewUser(login, password, userService);
                content.setRequestAttribute(RequestAttributeType.SEND_REDIRECT.getName(), true);
                page = PathManager.getPagePath(PageType.LOGIN.getPageName());
                logger.info("User " + login + " successfully signed up");
            } else {
                logger.info("Some issues during registration");
                page = PathManager.getPagePath(PageType.REGISTRATION.getPageName());
                content.setRequestAttribute(RequestAttributeType.ERROR_MESSAGE_KEY.getName(), registrationValidator.getErrorMessageKey());
            }
            content.setSessionAttribute(SessionAttributeType.URI.getName(), userService.getUriOfRequest(content));
        } catch (ServiceException e) {
            logger.error(e);
            page = userService.prepareErrorPage(content, ErrorType.INTERNAL_SERVER);
        } catch (RequestParameterException e) {
            logger.error(e);
            page = userService.prepareErrorPage(content, ErrorType.BAD_REQUEST);
        } finally {
            userService.releaseConnection();
        }
        return page;
    }

    private void registerNewUser(String login, String password, UserService userService) throws ServiceException, RequestParameterException {
        SimpleUser newUser = new SimpleUser();
        newUser.setUsername(login);
        newUser.setPassword(new SALTEncrypter().encrypt(password));
        newUser.setAdmin(false);
        newUser.setStatus(UserStatus.AMATEUR);
        userService.addUser(newUser);
    }
}

package by.training.sixthgroup.command;

import by.training.sixthgroup.constant.*;
import by.training.sixthgroup.controller.SessionRequestContent;
import by.training.sixthgroup.exception.RequestParameterException;
import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.User;
import by.training.sixthgroup.service.UserService;
import by.training.sixthgroup.util.encrypter.Encrypter;
import by.training.sixthgroup.util.encrypter.SALTEncrypter;
import by.training.sixthgroup.util.manager.PathManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.Objects;

/**
 * invokes when user submits form of changing password on their profile page.
 */
public class EditProfileCommand implements Command {

    public static final Logger logger = LogManager.getLogger(EditProfileCommand.class);

    private static final String EDIT_SUCCESS_MESSAGE_KEY = "user.edit.profile";
    private static final String PASSWORD_CHECK_REGEX = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])\\S{8,}$";

    @Override
    public String execute(SessionRequestContent content) {
        logger.info("EditProfileCommand performing...");
        String page = null;
        UserService userService = new UserService();
        try {
            userService.initialize();
            User user = (User) content.getSessionAttribute(SessionAttributeType.USER.getName());
            String newPassword = content.getRequestParameter(RequestParameterType.EDIT_PASSWORD.getName());
            String oldPassword = content.getRequestParameter(RequestParameterType.EDIT_OLD_PASSWORD.getName());
            String confirmPassword = content.getRequestParameter(RequestParameterType.EDIT_CONFIRM_PASSWORD.getName());
            SALTEncrypter encrypter = new SALTEncrypter();
            if (checkPasswords(oldPassword, newPassword, confirmPassword, encrypter, user.getPassword())) {
                userService.changePassword(user.getId(), encrypter.encrypt(oldPassword), encrypter.encrypt(newPassword));
                prepareUserPage(content, userService, user.getId());
                page = (String) content.getSessionAttribute(SessionAttributeType.URI.getName());
                logger.info("Profile of user #" + user.getId() + " was successfully edited");
                } else {
                logger.error("Validation Error");
                page = userService.prepareErrorPage(content, ErrorType.BAD_REQUEST);
            }
        } catch (ServiceException e) {
            logger.error(e);
            page = userService.prepareErrorPage(content, ErrorType.INTERNAL_SERVER);
        } catch (RequestParameterException e) {
            logger.error(e);
            page = userService.prepareErrorPage(content, ErrorType.BAD_REQUEST);
        } finally {
            userService.releaseConnection();
        }

        return page;
    }

    private void prepareUserPage(SessionRequestContent content, UserService userService, int userId) throws ServiceException {
        User newUser = userService.findUser(userId);
        content.setSessionAttribute(SessionAttributeType.USER.getName(), newUser);
        content.setRequestAttribute(RequestAttributeType.SEND_REDIRECT.getName(), Boolean.TRUE);
        content.setSessionAttribute(SessionAttributeType.MESSAGE_SUCCESS.getName(), EDIT_SUCCESS_MESSAGE_KEY);
    }

    private boolean checkPasswords(String oldPassword, String newPassword, String confirmPassword,
                                   Encrypter encrypter, String userPassword) throws RequestParameterException, ServiceException {
        return (oldPassword != null && !Objects.equals(oldPassword, "")
                && encrypter.encrypt(oldPassword).equals(userPassword)
                && confirmPassword != null && !Objects.equals(confirmPassword, "")
                && confirmPassword.matches(PASSWORD_CHECK_REGEX)
                && newPassword != null && !Objects.equals(newPassword, "")
                && confirmPassword.equals(newPassword));
    }
}

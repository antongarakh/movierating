package by.training.sixthgroup.command;

import by.training.sixthgroup.constant.*;
import by.training.sixthgroup.controller.SessionRequestContent;
import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.User;
import by.training.sixthgroup.service.UserService;
import by.training.sixthgroup.util.manager.PathManager;
import by.training.sixthgroup.validation.UserPageValidator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.Objects;

/**
 * prepares user.jsp
 */
public class UserCommand implements Command {

    public static final Logger logger = LogManager.getLogger(UserCommand.class);

    @Override
    public String execute(SessionRequestContent content) {
        logger.info("UserCommand performing...");
        String page = null;
        UserService userService = new UserService();
        try {
            userService.initialize();
            String userPageTypeValue = content.getRequestParameter(RequestParameterType.USER_CONTENT_TYPE.getName());
            if (userPageTypeValue == null || Objects.equals(userPageTypeValue, "")) {
                throw new ServiceException();
            }
            UserPageContentType userPageContentType = UserPageContentType.getType(userPageTypeValue);
            String otherUserPageId = content.getRequestParameter(RequestParameterType.ID.getName());
            User sessionUser = (User) content.getSessionAttribute(SessionAttributeType.USER.getName());
            if (userPageContentType == null || (sessionUser == null && otherUserPageId == null)) {
                throw new ServiceException();
            } else if (otherUserPageId == null)
                otherUserPageId = "" + sessionUser.getId();
            UserPageValidator userPageValidator = new UserPageValidator(userPageContentType,
                    sessionUser, otherUserPageId, userService);
            if (userPageValidator.checkValidity()) {
                User user;
                if (sessionUser != null && otherUserPageId.equals("" + sessionUser.getId())) {
                    user = sessionUser;
                    userPageContentType = UserPageContentType.OWN_PAGE;
                } else {
                    user = userService.findUser(Integer.parseInt(otherUserPageId));
                    content.setRequestAttribute(RequestAttributeType.OTHER_USER.getName(), user);
                }
                content.setSessionAttribute(SessionAttributeType.USER_CONTENT_TYPE.getName(), userPageContentType.getName());
                content.setRequestAttribute(RequestAttributeType.USER_CONTENT_TYPE.getName(), userPageContentType.getName());
                if (sessionUser != null) {
                    content.setRequestAttribute(RequestAttributeType.USER.getName(), sessionUser);
                }
                logger.info("Next page is of " + user.getUsername() + " user");
                content.setRequestAttribute(RequestAttributeType.COMMENTS_FILMS.getName(), userService.findCommentsByUser(user.getId()));
                content.setRequestAttribute(RequestAttributeType.MARKS_FILMS.getName(), userService.findMarksFilmsByUser(user.getId()));
                page = PathManager.getPagePath(PageType.USER.getPageName());
                content.setSessionAttribute(SessionAttributeType.URI.getName(), userService.getUriOfRequest(content));
            } else {
                logger.error("Validation Error");
                content.setRequestAttribute(RequestAttributeType.ERROR_MESSAGE_KEY.getName(), userPageValidator.getErrorMessageKey());
                page = userService.prepareErrorPage(content, ErrorType.BAD_REQUEST);
            }
        } catch (ServiceException e) {
            logger.error(e);
            page = userService.prepareErrorPage(content, ErrorType.BAD_REQUEST);
        } finally {
            userService.releaseConnection();
        }
        return page;
    }
}


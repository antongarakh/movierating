package by.training.sixthgroup.command;

import by.training.sixthgroup.controller.SessionRequestContent;

public interface Command {
    String execute(SessionRequestContent content);
}

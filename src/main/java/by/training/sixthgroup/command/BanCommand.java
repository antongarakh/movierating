package by.training.sixthgroup.command;

import by.training.sixthgroup.constant.*;
import by.training.sixthgroup.controller.SessionRequestContent;
import by.training.sixthgroup.exception.RequestParameterException;
import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.User;
import by.training.sixthgroup.service.AdminService;
import by.training.sixthgroup.validation.BanValidator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * Invokes when admin click on ban button, prepares user.jsp or users.jsp depending on request parameter "page"
 */
public class BanCommand implements Command {

    public static final Logger logger = LogManager.getLogger(BanCommand.class);

    private static final String BAN_SUCCESS_MESSAGE_KEY = "user.ban.success";
    private static final String RESET_SUCCESS_MESSAGE_KEY = "user.reset.success";

    @Override
    public String execute(SessionRequestContent content) {
        logger.info("BanCommand performing...");
        String page = null;
        AdminService adminService = new AdminService();
        try {
            adminService.initialize();
            String userId = content.getRequestParameter(RequestParameterType.ID.getName());
            String kind = content.getRequestParameter(RequestParameterType.KIND.getName());
            String numberOfDays = content.getRequestParameter(RequestAttributeType.DAYS.getName());
            logger.info(kind + "ting ban for user with id=" + userId + " for the period of " + numberOfDays + " days");
            String pageParam = content.getRequestParameter(RequestParameterType.PAGE.getName());
            User currentUser = (User) content.getSessionAttribute(SessionAttributeType.USER.getName());
            BanValidator banValidator = new BanValidator(currentUser, kind, userId, adminService, pageParam, numberOfDays);
            if (banValidator.checkValidity()) {
                adminService.changeBanStatus(kind, userId, numberOfDays);
                User userToChange = adminService.findUser(Integer.parseInt(userId));
                if (pageParam.equals(PageType.USERS.getPageName())) {
                    page = adminService.prepareUsersPage(content, adminService);
                } else {
                    if (!pageParam.equals(PageType.USER.getPageName())) {
                        throw new ServiceException();
                    }
                    page = adminService.prepareUserPage(content, getSuccessMessage(kind), userToChange);
                }
            } else {
                logger.error("Validation Error");
                content.setRequestAttribute(RequestAttributeType.ERROR_MESSAGE_KEY.getName(), banValidator.getErrorMessageKey());
                page = adminService.prepareErrorPage(content, ErrorType.BAD_REQUEST);
            }
        } catch (ServiceException e) {
            logger.error(e);
            page = adminService.prepareErrorPage(content, ErrorType.INTERNAL_SERVER);
        } catch (RequestParameterException e) {
            logger.error(e);
            page = adminService.prepareErrorPage(content, ErrorType.BAD_REQUEST);
        } finally {
            adminService.releaseConnection();
        }

        return page;
    }

    private String getSuccessMessage(String kind) {
        switch (kind) {
            case Constants.SET:
                return BAN_SUCCESS_MESSAGE_KEY;
            case Constants.RESET:
                return RESET_SUCCESS_MESSAGE_KEY;
            default:
                return null;
        }
    }
}

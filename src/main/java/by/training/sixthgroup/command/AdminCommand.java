package by.training.sixthgroup.command;

import by.training.sixthgroup.constant.*;
import by.training.sixthgroup.controller.SessionRequestContent;
import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.User;
import by.training.sixthgroup.service.UserService;
import by.training.sixthgroup.util.manager.PathManager;
import by.training.sixthgroup.validation.UserPageValidator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * When logged in as admin, AdminCommand will invoke and prepare user.jsp
 */
public class AdminCommand implements Command {

    public static final Logger logger = LogManager.getLogger(AdminCommand.class);

    @Override
    public String execute(SessionRequestContent content) {
        logger.info("AdminCommand performing...");
        String page = null;
        UserService userService = new UserService();
        try {
            userService.initialize();
            String userPageTypeValue = content.getRequestParameter(RequestParameterType.USER_CONTENT_TYPE.getName());
            UserPageContentType userPageContentType = UserPageContentType.getType(userPageTypeValue);
            User user = (User) content.getSessionAttribute(SessionAttributeType.USER.getName());
            if (userPageContentType == null || !userPageContentType.equals(UserPageContentType.OWN_PAGE)
                    || user == null || !user.getIsAdmin()) {
                throw new ServiceException();
            }
            UserPageValidator validator = new UserPageValidator(userPageContentType, user, "" + user.getId(), userService);
            if (validator.checkValidity()) {
                content.setSessionAttribute(SessionAttributeType.USER_CONTENT_TYPE.getName(), userPageContentType.getName());
                content.setRequestAttribute(RequestAttributeType.USER_CONTENT_TYPE.getName(), userPageContentType.getName());
                content.setRequestAttribute(RequestAttributeType.COMMENTS_FILMS.getName(), userService.findCommentsByUser(user.getId()));
                content.setRequestAttribute(RequestAttributeType.MARKS_FILMS.getName(), userService.findMarksFilmsByUser(user.getId()));
                content.setSessionAttribute(SessionAttributeType.URI.getName(), userService.getUriOfRequest(content));
                page = PathManager.getPagePath(PageType.USER.getPageName());
            } else {
                logger.error("Validation Error");
                content.setRequestAttribute(RequestAttributeType.ERROR_MESSAGE_KEY.getName(), validator.getErrorMessageKey());
                page = userService.prepareErrorPage(content, ErrorType.BAD_REQUEST);
            }
        } catch (ServiceException e) {
            logger.error(e);
            page = userService.prepareErrorPage(content, ErrorType.BAD_REQUEST);
        } finally {
            userService.releaseConnection();
        }

        return page;
    }
}

package by.training.sixthgroup.command;

import by.training.sixthgroup.constant.*;
import by.training.sixthgroup.controller.SessionRequestContent;
import by.training.sixthgroup.exception.RequestParameterException;
import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.Country;
import by.training.sixthgroup.model.Film;
import by.training.sixthgroup.model.Genre;
import by.training.sixthgroup.service.AdminService;
import by.training.sixthgroup.util.manager.CloudinaryManager;
import by.training.sixthgroup.util.manager.PathManager;
import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import static org.apache.commons.lang3.CharEncoding.UTF_8;

/**
 * invokes when form of loading film is submitted. Adds film info to database. Loads image from desktop to cloud for images and adds the link to database.
 */
public class LoadFilmCommand implements Command {

    public static final Logger logger = LogManager.getLogger(LoadFilmCommand.class);
    private static final String LOAD_SUCCESS_MESSAGE_KEY = "movie.load.success";


    @Override
    public String execute(SessionRequestContent content) {
        logger.info("LoadFilmCommand performing...");
        DiskFileItemFactory fileFactory = new DiskFileItemFactory();
        ServletFileUpload uploader = new ServletFileUpload(fileFactory);
        uploader.setHeaderEncoding(UTF_8);
        AdminService adminService = new AdminService();
        String imageURL = null;
        String page = null;
        try {
            adminService.initialize();
            List<FileItem> fileItemsList = uploader.parseRequest(content.getRequest());
            Film film = new Film();
            for (FileItem fileItem : fileItemsList) {
                if (fileItem.isFormField()) {
                    String fieldName = fileItem.getFieldName();
                    setFilmInfo(fieldName, film, fileItem, adminService);
                } else {
                    imageURL = this.uploadFile(fileItem, content);
                }
            }
            adminService.addFilm(film.getName(), film.getDuration(), film.getReleaseYear(), film.getDescription(), imageURL, film.getGenres(), film.getCountries());
            content.setRequestAttribute(RequestAttributeType.SEND_REDIRECT.getName(), Boolean.TRUE);
            content.setSessionAttribute(SessionAttributeType.MESSAGE_SUCCESS.getName(), LOAD_SUCCESS_MESSAGE_KEY);
            logger.info("Movie was successfully added");
            page = PathManager.getPagePath(PageType.ADD_FILM.getPageName());
        } catch (Exception e) {
            logger.error(e);
            page = adminService.prepareErrorPage(content, ErrorType.INTERNAL_SERVER);
        } finally {
            adminService.releaseConnection();
        }

        return page;
    }

    private String uploadFile(FileItem fileItem, SessionRequestContent content) throws Exception {
        String fieldName = fileItem.getFieldName();
        String fileName = FilenameUtils.getName(fileItem.getName());
        String filePath = content.getRealPath("/") + Constants.IMG_FOLDER_NAME;
        if (fieldName.equals(Constants.SENT_FILE)) {
            filePath = filePath + File.separator + fileName;
            File storeFile = new File(filePath);
            if (storeFile.createNewFile()) {
                fileItem.write(storeFile);
            }
            Cloudinary cloudinary = CloudinaryManager.getCloudinary();
            Map uploadResult = cloudinary.uploader().upload(storeFile, ObjectUtils.emptyMap());
            return (String) uploadResult.get(Constants.URL);
        } else {
            throw new RequestParameterException();
        }
    }

    private void setFilmInfo(String fieldName, Film film, FileItem fileItem, AdminService adminService) throws ServiceException, CloneNotSupportedException, UnsupportedEncodingException {
        if (fieldName.equals(UploadItemType.NAME.getName())) {
            logger.info("Movie to add is " + fileItem.getString(UTF_8));
            film.setName(fileItem.getString(UTF_8));
        } else if (fieldName.equals(UploadItemType.DURATION.getName())) {
            film.setDuration(Integer.parseInt(fileItem.getString()));
        } else if (fieldName.equals(UploadItemType.DESCRIPTION.getName())) {
            film.setDescription(fileItem.getString(UTF_8));
        } else if (fieldName.equals(UploadItemType.RELEASE_YEAR.getName())) {
            film.setReleaseYear(Integer.parseInt(fileItem.getString()));
        } else if (fieldName.equals(UploadItemType.GENRES.getName())) {
            Genre genre = adminService.findGenreById(Integer.parseInt(fileItem.getString()));
            film.addGenreToFilm(genre);
        } else if (fieldName.equals(UploadItemType.COUNTRIES.getName())) {
            Country country = adminService.findCountryByCode(fileItem.getString());
            film.addCountryToFilm(country);
        }
    }

}
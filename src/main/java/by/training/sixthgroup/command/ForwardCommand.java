package by.training.sixthgroup.command;

import by.training.sixthgroup.constant.RequestParameterType;
import by.training.sixthgroup.constant.SessionAttributeType;
import by.training.sixthgroup.controller.SessionRequestContent;
import by.training.sixthgroup.service.UserService;
import by.training.sixthgroup.util.manager.PathManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * forwards to requested page
 */
public class ForwardCommand implements Command {

    public static final Logger logger = LogManager.getLogger(FilmsCommand.class);

    @Override
    public String execute(SessionRequestContent content) {
        logger.info("ForwardCommand performing...");
        UserService userService = new UserService();
        content.setSessionAttribute(SessionAttributeType.URI.getName(), userService.getUriOfRequest(content));
        String pageName = content.getRequestParameter(RequestParameterType.PAGE.getName());
        return PathManager.getPagePath(pageName);

    }
}

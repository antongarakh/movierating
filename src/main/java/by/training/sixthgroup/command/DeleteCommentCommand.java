package by.training.sixthgroup.command;

import by.training.sixthgroup.constant.*;
import by.training.sixthgroup.controller.SessionRequestContent;
import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.Film;
import by.training.sixthgroup.model.Genre;
import by.training.sixthgroup.model.User;
import by.training.sixthgroup.service.UserService;
import by.training.sixthgroup.util.manager.PathManager;
import by.training.sixthgroup.util.model.CommentUserData;
import by.training.sixthgroup.validation.DeleteCommentValidator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Objects;

/**
 * Invokes when admin click on delete comment button, prepares user.jsp or film.jsp depending on request parameters
 */
public class DeleteCommentCommand implements Command {

    public static final Logger logger = LogManager.getLogger(DeleteCommentCommand.class);
    private static final String DELETE_USER_MESSAGE_KEY = "user.delete.comment";
    private static final String DELETE_FILM_MESSAGE_KEY = "movie.delete.comment";


    @Override
    public String execute(SessionRequestContent content) {
        logger.info("DeleteCommentCommand performing...");
        String page = null;
        UserService userService = new UserService();
        try {
            userService.initialize();
            String commentId = content.getRequestParameter(RequestParameterType.ID.getName());
            String userId = content.getRequestParameter(RequestParameterType.USER_ID.getName());
            String filmId = content.getRequestParameter(RequestParameterType.FILM_ID.getName());
            User sessionUser = (User) content.getSessionAttribute(SessionAttributeType.USER.getName());
            DeleteCommentValidator validator = new DeleteCommentValidator(commentId, filmId, userId, sessionUser, userService);
            if (validator.checkValidity()) {
                userService.deleteComment(Integer.parseInt(commentId));
                logger.info("Comment # " + commentId + " deleted");
                User user;
                if (userId != null && !Objects.equals(userId, ""))
                    user = userService.findUser(Integer.parseInt(userId));
                else
                    user = sessionUser;
                if (filmId != null && !Objects.equals(filmId, "")) {
                    prepareFilmPage(content, userService, filmId);
                    page = PathManager.getPagePath(PageType.FILM.getPageName());
                } else {
                    prepareUserPage(content, userService, user);
                    page = PathManager.getPagePath(PageType.USER.getPageName());
                }
            } else {
                logger.error("Validation Error");
                content.setRequestAttribute(RequestAttributeType.ERROR_MESSAGE_KEY.getName(), validator.getErrorMessageKey());
                page = userService.prepareErrorPage(content, ErrorType.BAD_REQUEST);
            }
        } catch (ServiceException e) {
            logger.error(e.getMessage(), e);
            page = userService.prepareErrorPage(content, ErrorType.INTERNAL_SERVER);
        } finally {
            userService.releaseConnection();
        }
        return page;
    }

    private void prepareFilmPage(SessionRequestContent content, UserService userService, String filmId) throws ServiceException {
        Film film = userService.findFilmById(Integer.parseInt(filmId));
        content.setRequestAttribute(RequestAttributeType.FILM.getName(), film);
        List<Genre> genres = userService.findGenresByFilm(Integer.parseInt(filmId));
        content.setRequestAttribute(RequestAttributeType.GENRES.getName(), genres);
        List<CommentUserData> comments = userService.findCommentsByFilm(Integer.parseInt(filmId));
        List countries = userService.findCountriesByFilm(Integer.parseInt(filmId));
        content.setRequestAttribute(RequestAttributeType.COUNTRIES.getName(), countries);
        content.setRequestAttribute(RequestAttributeType.COMMENTS.getName(), comments);
        content.setSessionAttribute(SessionAttributeType.MESSAGE_SUCCESS.getName(), DELETE_FILM_MESSAGE_KEY);
    }

    private void prepareUserPage(SessionRequestContent content, UserService userService, User user) throws ServiceException {
        content.setRequestAttribute(RequestAttributeType.COMMENTS_FILMS.getName(), userService.findCommentsByUser(user.getId()));
        content.setRequestAttribute(RequestAttributeType.MARKS_FILMS.getName(), userService.findMarksFilmsByUser(user.getId()));
        content.setRequestAttribute(RequestAttributeType.OTHER_USER.getName(), user);
        content.setSessionAttribute(SessionAttributeType.MESSAGE_SUCCESS.getName(), DELETE_USER_MESSAGE_KEY);
    }
}

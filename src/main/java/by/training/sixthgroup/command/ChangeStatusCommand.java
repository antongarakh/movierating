package by.training.sixthgroup.command;

import by.training.sixthgroup.constant.*;
import by.training.sixthgroup.controller.SessionRequestContent;
import by.training.sixthgroup.exception.RequestParameterException;
import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.User;
import by.training.sixthgroup.model.UserStatus;
import by.training.sixthgroup.service.AdminService;
import by.training.sixthgroup.validation.ChangeStatusValidator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * Invokes when admin click on change status button, prepares user.jsp or users.jsp depending on request parameter "page"
 */
public class ChangeStatusCommand implements Command {

    public static final Logger logger = LogManager.getLogger(ChangeStatusCommand.class);
    private static final String STATUS_SUCCESS_MESSAGE_KEY = "user.status.success";

    @Override
    public String execute(SessionRequestContent content) {
        logger.info("ChangeStatusCommand performing...");
        String page = null;
        AdminService adminService = new AdminService();
        try {
            adminService.initialize();
            String userId = content.getRequestParameter(RequestParameterType.ID.getName());
            String kind = content.getRequestParameter(RequestParameterType.KIND.getName());
            User userToChange = adminService.findUser(Integer.parseInt(userId));
            User currentUser = (User) content.getSessionAttribute(SessionAttributeType.USER.getName());
            String pageParam = content.getRequestParameter(RequestParameterType.PAGE.getName());
            ChangeStatusValidator validator = new ChangeStatusValidator(currentUser, userId, kind, pageParam, adminService);
            if (validator.checkValidity()) {
                UserStatus userStatus = userToChange.getStatusObject();
                UserStatus newStatus = adminService.getNewUserStatus(userStatus, kind);
                logger.info(kind + "status for user with id=" + userId + " to " + newStatus.getStatusName());
                adminService.changeStatus(Integer.parseInt(userId), newStatus.getStatusName());
                userToChange = adminService.findUser(Integer.parseInt(userId));
                if (pageParam.equals(PageType.USERS.getPageName())) {
                    page = adminService.prepareUsersPage(content, adminService);
                } else {
                    if (!pageParam.equals(PageType.USER.getPageName())) {
                        throw new ServiceException();
                    }
                    page = adminService.prepareUserPage(content, STATUS_SUCCESS_MESSAGE_KEY, userToChange);
                }
            } else {
                content.setRequestAttribute(RequestAttributeType.ERROR_MESSAGE_KEY.getName(), validator.getErrorMessageKey());
                logger.error("Validation Error");
                page = adminService.prepareErrorPage(content, ErrorType.BAD_REQUEST);
            }
        } catch (ServiceException e) {
            logger.error(e);
            page = adminService.prepareErrorPage(content, ErrorType.INTERNAL_SERVER);
        } catch (RequestParameterException e) {
            page = adminService.prepareErrorPage(content, ErrorType.BAD_REQUEST);
        } finally {
            adminService.releaseConnection();
        }
        return page;
    }

}

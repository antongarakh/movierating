package by.training.sixthgroup.command;

import by.training.sixthgroup.constant.PageType;
import by.training.sixthgroup.constant.SessionAttributeType;
import by.training.sixthgroup.controller.SessionRequestContent;
import by.training.sixthgroup.service.UserService;
import by.training.sixthgroup.util.manager.PathManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * invokes when there's no appropriate command in CommandFactory for requested command name. Just redirects to home.jsp
 */
public class EmptyCommand implements Command {

    public static final Logger logger = LogManager.getLogger(EmptyCommand.class);


    @Override
    public String execute(SessionRequestContent content) {
        logger.info("EmptyCommand performing...");
        content.setSessionAttribute(SessionAttributeType.URI.getName(), new UserService().getUriOfRequest(content));
        return PathManager.getPagePath(PageType.HOME.getPageName());
    }

}

package by.training.sixthgroup.command;

import by.training.sixthgroup.constant.*;
import by.training.sixthgroup.controller.SessionRequestContent;
import by.training.sixthgroup.exception.RequestParameterException;
import by.training.sixthgroup.exception.ServiceException;
import by.training.sixthgroup.model.User;
import by.training.sixthgroup.service.UserService;
import by.training.sixthgroup.util.manager.PathManager;
import by.training.sixthgroup.validation.LogInValidator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.Date;

/**
 * invokes when user tries to log in, if successfully - forwards to home.jsp, if not - to login.jsp from where request has been sent
 */
public class LoginCommand implements Command {

    public static final Logger logger = LogManager.getLogger(LoginCommand.class);


    @Override
    public String execute(SessionRequestContent content) {
        logger.info("LoginCommand performing...");
        String page = null;
        UserService userService = new UserService();
        try {
            userService.initialize();
            String login = content.getRequestParameter(RequestParameterType.LOGIN.getName());
            String password = content.getRequestParameter(RequestParameterType.PASSWORD.getName());
            LogInValidator logInValidator = new LogInValidator(login, password, userService);
            if (logInValidator.checkValidity()) {
                User user = logInValidator.getLoggedInUser();
                content.setSessionAttribute(SessionAttributeType.USER.getName(), user);
                logger.info("User " + login + " successfully logged in");
                page = PathManager.getPagePath(PageType.HOME.getPageName());
            } else {
                Date date = logInValidator.getExpirationDate();
                if (date != null) {
                    content.setRequestAttribute(RequestAttributeType.EXPIRATION_DATE.getName(), date);
                }
                content.setRequestAttribute(RequestAttributeType.ERROR_MESSAGE_KEY.getName(), logInValidator.getErrorMessageKey());
                logger.info("User didn't log in");
                page = PathManager.getPagePath(PageType.LOGIN.getPageName());
            }
            content.setSessionAttribute(SessionAttributeType.URI.getName(), userService.getUriOfRequest(content));
        } catch (ServiceException e) {
            logger.error(e);
            page = userService.prepareErrorPage(content, ErrorType.INTERNAL_SERVER);
        } catch (RequestParameterException e) {
            logger.error(e);
            page = userService.prepareErrorPage(content, ErrorType.BAD_REQUEST);
        } finally {
            userService.releaseConnection();
        }
        return page;
    }
}

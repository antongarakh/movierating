package by.training.sixthgroup.upload;

import by.training.sixthgroup.command.Command;
import by.training.sixthgroup.command.CommandEnum;
import by.training.sixthgroup.command.CommandFactory;
import by.training.sixthgroup.controller.SessionRequestContent;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet for adding new film. Uploads an image from desktop that means content in submitting form is of multiple types
 */
public class Uploader extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        SessionRequestContent sessionRequestContent = new SessionRequestContent(request, response);
        CommandFactory commandFactory = new CommandFactory();
        Command command = commandFactory.createCommand(CommandEnum.LOAD_FILM.name());
        String page = command.execute(sessionRequestContent);
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
        dispatcher.forward(request, response);
    }

}



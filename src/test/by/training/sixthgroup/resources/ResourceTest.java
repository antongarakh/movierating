package by.training.sixthgroup.resources;

import by.training.sixthgroup.constant.Constants;
import org.junit.Assert;
import org.junit.Test;

import java.util.ResourceBundle;

public class ResourceTest {

    @Test
    public void pagesPropertiesExist() {
        Assert.assertNotNull(ResourceBundle.getBundle(Constants.CONFIG_FILE_NAME));
    }

    @Test
    public void databasePropertiesExist() {
        Assert.assertNotNull(ResourceBundle.getBundle(Constants.DB_PROPERTIES_FILE_NAME));
    }

    @Test
    public void cloudPropertiesPropertiesExist() {
        Assert.assertNotNull(ResourceBundle.getBundle(Constants.CLOUDINARY_PROPS_FILE_NAME));
    }

    @Test
    public void pageContentPropertiesExist() {
        Assert.assertNotNull(ResourceBundle.getBundle(Constants.PAGE_CONTENT_FILE_NAME));
    }
}

package by.training.sixthgroup.model;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SerializationTest {

    private static final int GENRE_ID = 1;
    private static final String GENRE_NAME = "Drama";
    private static Genre genre;
    private static Genre deserializedGenre;

    @Before
    public void setUp() {
        genre = new Genre(GENRE_ID, GENRE_NAME);
        byte[] array = SerializationUtils.serialize(genre);
        deserializedGenre = (Genre) SerializationUtils.deserialize(array);
    }

    @Test
    public void genreNotEmptyTest() {
        assertTrue(deserializedGenre != null);
    }

    @Test
    public void objectsEqualTest() {
        assertEquals(genre, deserializedGenre);
    }

    @Test
    public void hashCodeEqualsTest() {
        assertEquals(genre.hashCode(), deserializedGenre.hashCode());
    }

}
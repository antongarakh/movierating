package by.training.sixthgroup.db.pool.mysql;

import by.training.sixthgroup.db.pool.ConnectionPool;
import by.training.sixthgroup.exception.DatabaseException;
import org.junit.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class MySQLConnectionPoolTest {

    private static final int POOL_CAPACITY = 10;
    private static ConnectionPool pool;
    private static List<Connection> connections;

    @BeforeClass
    public static void initializePool() {
        pool = MySQLConnectionPool.newInstance();
        assertNotNull(pool);
    }

    @AfterClass
    public static void closePool() {
        pool.close();
    }

    @Before
    public void takeConnections() throws DatabaseException {
        connections = new ArrayList<>();
        for (int i = 0; i < POOL_CAPACITY; i++) {
            connections.add(pool.getConnection());
        }
        assertEquals(POOL_CAPACITY, connections.size());
    }

    @Test
    public void takeAllowedConnectionsTest() {
        for (int i = 0; i < POOL_CAPACITY; i++) {
            assertNotNull(connections.get(i));
        }
    }

    @Test(expected = NullPointerException.class)
    public void takeDisallowedConnectionTest() throws DatabaseException, SQLException {
        pool.getConnection().createStatement();
    }

    @After
    public void returnConnections() {
        for (Connection connection : connections) {
            pool.releaseConnection(connection);
        }
    }
}
package by.training.sixthgroup.db.pool.mysql;

import by.training.sixthgroup.exception.DatabaseException;
import org.junit.Test;

import java.sql.Connection;

public class DBConnectorTest {
    @Test
    public void takeConnection() {
        boolean result = false;
        try {
            DBConnector dbConnector = new DBConnector();
            Connection connection = dbConnector.getConnection();
            if (connection != null) {
                result = true;
            }
        } catch (DatabaseException e) {

        }
        assert result;
    }
}
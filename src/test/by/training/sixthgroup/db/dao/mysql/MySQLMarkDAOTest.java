package by.training.sixthgroup.db.dao.mysql;

import by.training.sixthgroup.db.dao.MarkDAO;
import by.training.sixthgroup.model.Mark;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.when;

public class MySQLMarkDAOTest {

    @Mock
    MarkDAO markDAO;

    @Mock
    Mark mark;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addMark() throws Exception {
        when(mark.getUserId()).thenReturn(1);
        when(mark.getFilmId()).thenReturn(1);
        when(markDAO.findMarkByUserAndFilm(1, 1)).thenReturn(mark);
        assert (!markDAO.addMark(mark));
    }

}
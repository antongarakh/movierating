-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: movie_rating
-- ------------------------------------------------------
-- Server version	5.7.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id`           INT(11)            NOT NULL AUTO_INCREMENT,
  `film_id`      INT(11)            NOT NULL,
  `user_id`      INT(11)            NOT NULL,
  `comment_text` VARCHAR(1000)
                 CHARACTER SET utf8 NOT NULL,
  `date`         DATETIME           NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_comment_film_idx` (`film_id`),
  KEY `fk_comment_user1_idx` (`user_id`),
  CONSTRAINT `fk_comment_film` FOREIGN KEY (`film_id`) REFERENCES `films` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_comment_user1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 124
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments`
  DISABLE KEYS */;
INSERT INTO `comments`
VALUES (4, 12, 25, 'greataerh movie', '2016-07-24 21:27:08'), (5, 14, 5, 'greatffff movie', '2016-07-24 21:27:08'),
  (6, 20, 4, 'great aejmovie', '2016-07-24 21:27:09'), (11, 17, 12, 'greataerh movie', '2016-07-24 21:27:09'),
  (12, 13, 13, 'greatffff movie', '2016-07-24 21:27:09'), (13, 19, 14, 'great aejmovie', '2016-07-24 21:27:09'),
  (35, 5, 31, 'aerhaerhaerh', '2016-09-16 19:49:31'), (42, 5, 10, 'я молодец', '2016-09-16 20:10:00'),
  (55, 10, 31, 'araerhaerha', '2016-09-18 18:07:51'), (57, 3, 31, 'arhaerh', '2016-09-18 18:18:46'),
  (118, 4, 32, 'aerhaerh', '2016-09-28 18:42:37'), (122, 8, 32, 'фильм збс', '2016-09-28 20:35:47'),
  (123, 19, 31, 'уыо', '2016-09-28 22:00:42');
/*!40000 ALTER TABLE `comments`
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `country_code` CHAR(2)     NOT NULL,
  `country_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`country_code`),
  UNIQUE KEY `country_UNIQUE` (`country_name`),
  UNIQUE KEY `country_code_UNIQUE` (`country_code`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries`
  DISABLE KEYS */;
INSERT INTO `countries`
VALUES ('AF', 'Afghanistan'), ('AL', 'Albania'), ('DZ', 'Algeria'), ('DS', 'American Samoa'), ('AD', 'Andorra'),
  ('AO', 'Angola'), ('AI', 'Anguilla'), ('AQ', 'Antarctica'), ('AG', 'Antigua and Barbuda'), ('AR', 'Argentina'),
  ('AM', 'Armenia'), ('AW', 'Aruba'), ('AU', 'Australia'), ('AT', 'Austria'), ('AZ', 'Azerbaijan'), ('BS', 'Bahamas'),
  ('BH', 'Bahrain'), ('BD', 'Bangladesh'), ('BB', 'Barbados'), ('BY', 'Belarus'), ('BE', 'Belgium'), ('BZ', 'Belize'),
  ('BJ', 'Benin'), ('BM', 'Bermuda'), ('BT', 'Bhutan'), ('BO', 'Bolivia'), ('BA', 'Bosnia and Herzegovina'),
  ('BW', 'Botswana'), ('BV', 'Bouvet Island'), ('BR', 'Brazil'), ('IO', 'British Indian Ocean Territory'),
  ('BN', 'Brunei Darussalam'), ('BG', 'Bulgaria'), ('BF', 'Burkina Faso'), ('BI', 'Burundi'), ('KH', 'Cambodia'),
  ('CM', 'Cameroon'), ('CA', 'Canada'), ('CV', 'Cape Verde'), ('KY', 'Cayman Islands'),
  ('CF', 'Central African Republic'), ('TD', 'Chad'), ('CL', 'Chile'), ('CN', 'China'), ('CX', 'Christmas Island'),
  ('CC', 'Cocos (Keeling) Islands'), ('CO', 'Colombia'), ('KM', 'Comoros'), ('CG', 'Congo'), ('CK', 'Cook Islands'),
  ('CR', 'Costa Rica'), ('HR', 'Croatia (Hrvatska)'), ('CU', 'Cuba'), ('CY', 'Cyprus'), ('CZ', 'Czech Republic'),
  ('DK', 'Denmark'), ('DJ', 'Djibouti'), ('DM', 'Dominica'), ('DO', 'Dominican Republic'), ('TP', 'East Timor'),
  ('EC', 'Ecuador'), ('EG', 'Egypt'), ('SV', 'El Salvador'), ('GQ', 'Equatorial Guinea'), ('ER', 'Eritrea'),
  ('EE', 'Estonia'), ('ET', 'Ethiopia'), ('FK', 'Falkland Islands (Malvinas)'), ('FO', 'Faroe Islands'), ('FJ', 'Fiji'),
  ('FI', 'Finland'), ('FR', 'France'), ('FX', 'France, Metropolitan'), ('GF', 'French Guiana'),
  ('PF', 'French Polynesia'), ('TF', 'French Southern Territories'), ('GA', 'Gabon'), ('GM', 'Gambia'),
  ('GE', 'Georgia'), ('DE', 'Germany'), ('GH', 'Ghana'), ('GI', 'Gibraltar'), ('GR', 'Greece'), ('GL', 'Greenland'),
  ('GD', 'Grenada'), ('GP', 'Guadeloupe'), ('GU', 'Guam'), ('GT', 'Guatemala'), ('GK', 'Guernsey'), ('GN', 'Guinea'),
  ('GW', 'Guinea-Bissau'), ('GY', 'Guyana'), ('HT', 'Haiti'), ('HM', 'Heard and Mc Donald Islands'), ('HN', 'Honduras'),
  ('HK', 'Hong Kong'), ('HU', 'Hungary'), ('IS', 'Iceland'), ('IN', 'India'), ('ID', 'Indonesia'),
  ('IR', 'Iran (Islamic Republic of)'), ('IQ', 'Iraq'), ('IE', 'Ireland'), ('IM', 'Isle of Man'), ('IL', 'Israel'),
  ('IT', 'Italy'), ('CI', 'Ivory Coast'), ('JM', 'Jamaica'), ('JP', 'Japan'), ('JE', 'Jersey'), ('JO', 'Jordan'),
  ('KZ', 'Kazakhstan'), ('KE', 'Kenya'), ('KI', 'Kiribati'), ('KP', 'Korea, Democratic People\'s Republic of'),
  ('KR', 'Korea, Republic of'), ('XK', 'Kosovo'), ('KW', 'Kuwait'), ('KG', 'Kyrgyzstan'),
  ('LA', 'Lao People\'s Democratic Republic'), ('LV', 'Latvia'), ('LB', 'Lebanon'), ('LS', 'Lesotho'),
  ('LR', 'Liberia'), ('LY', 'Libyan Arab Jamahiriya'), ('LI', 'Liechtenstein'), ('LT', 'Lithuania'),
  ('LU', 'Luxembourg'), ('MO', 'Macau'), ('MK', 'Macedonia'), ('MG', 'Madagascar'), ('MW', 'Malawi'),
  ('MY', 'Malaysia'), ('MV', 'Maldives'), ('ML', 'Mali'), ('MT', 'Malta'), ('MH', 'Marshall Islands'),
  ('MQ', 'Martinique'), ('MR', 'Mauritania'), ('MU', 'Mauritius'), ('TY', 'Mayotte'), ('MX', 'Mexico'),
  ('FM', 'Micronesia, Federated States of'), ('MD', 'Moldova, Republic of'), ('MC', 'Monaco'), ('MN', 'Mongolia'),
  ('ME', 'Montenegro'), ('MS', 'Montserrat'), ('MA', 'Morocco'), ('MZ', 'Mozambique'), ('MM', 'Myanmar'),
  ('NA', 'Namibia'), ('NR', 'Nauru'), ('NP', 'Nepal'), ('NL', 'Netherlands'), ('AN', 'Netherlands Antilles'),
  ('NC', 'New Caledonia'), ('NZ', 'New Zealand'), ('NI', 'Nicaragua'), ('NE', 'Niger'), ('NG', 'Nigeria'),
  ('NU', 'Niue'), ('NF', 'Norfolk Island'), ('MP', 'Northern Mariana Islands'), ('NO', 'Norway'), ('OM', 'Oman'),
  ('PK', 'Pakistan'), ('PW', 'Palau'), ('PS', 'Palestine'), ('PA', 'Panama'), ('PG', 'Papua New Guinea'),
  ('PY', 'Paraguay'), ('PE', 'Peru'), ('PH', 'Philippines'), ('PN', 'Pitcairn'), ('PL', 'Poland'), ('PT', 'Portugal'),
  ('PR', 'Puerto Rico'), ('QA', 'Qatar'), ('RE', 'Reunion'), ('RO', 'Romania'), ('RU', 'Russian Federation'),
  ('RW', 'Rwanda'), ('KN', 'Saint Kitts and Nevis'), ('LC', 'Saint Lucia'), ('VC', 'Saint Vincent and the Grenadines'),
  ('WS', 'Samoa'), ('SM', 'San Marino'), ('ST', 'Sao Tome and Principe'), ('SA', 'Saudi Arabia'), ('SN', 'Senegal'),
  ('RS', 'Serbia'), ('SC', 'Seychelles'), ('SL', 'Sierra Leone'), ('SG', 'Singapore'), ('SK', 'Slovakia'),
  ('SI', 'Slovenia'), ('SB', 'Solomon Islands'), ('SO', 'Somalia'), ('ZA', 'South Africa'),
  ('GS', 'South Georgia South Sandwich Islands'), ('ES', 'Spain'), ('LK', 'Sri Lanka'), ('SH', 'St. Helena'),
  ('PM', 'St. Pierre and Miquelon'), ('SD', 'Sudan'), ('SR', 'Suriname'), ('SJ', 'Svalbard and Jan Mayen Islands'),
  ('SZ', 'Swaziland'), ('SE', 'Sweden'), ('CH', 'Switzerland'), ('SY', 'Syrian Arab Republic'), ('TW', 'Taiwan'),
  ('TJ', 'Tajikistan'), ('TZ', 'Tanzania, United Republic of'), ('TH', 'Thailand'), ('TG', 'Togo'), ('TK', 'Tokelau'),
  ('TO', 'Tonga'), ('TT', 'Trinidad and Tobago'), ('TN', 'Tunisia'), ('TR', 'Turkey'), ('TM', 'Turkmenistan'),
  ('TC', 'Turks and Caicos Islands'), ('TV', 'Tuvalu'), ('UG', 'Uganda'), ('UA', 'Ukraine'),
  ('AE', 'United Arab Emirates'), ('GB', 'United Kingdom'), ('US', 'United States'),
  ('UM', 'United States minor outlying islands'), ('UY', 'Uruguay'), ('UZ', 'Uzbekistan'), ('VU', 'Vanuatu'),
  ('VA', 'Vatican City State'), ('VE', 'Venezuela'), ('VN', 'Vietnam'), ('VG', 'Virgin Islands (British)'),
  ('VI', 'Virgin Islands (U.S.)'), ('WF', 'Wallis and Futuna Islands'), ('EH', 'Western Sahara'), ('YE', 'Yemen'),
  ('YU', 'Yugoslavia'), ('ZR', 'Zaire'), ('ZM', 'Zambia'), ('ZW', 'Zimbabwe');
/*!40000 ALTER TABLE `countries`
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `films`
--

DROP TABLE IF EXISTS `films`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `films` (
  `id`           INT(11)       NOT NULL AUTO_INCREMENT,
  `duration`     INT(11)       NOT NULL,
  `description`  VARCHAR(1000) NOT NULL,
  `release_year` YEAR(4)       NOT NULL,
  `rating`       DOUBLE                 DEFAULT NULL,
  `name`         VARCHAR(100)           DEFAULT NULL,
  `url`          VARCHAR(500)           DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 35
  DEFAULT CHARSET = utf8
  DELAY_KEY_WRITE = 1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `films`
--

LOCK TABLES `films` WRITE;
/*!40000 ALTER TABLE `films`
  DISABLE KEYS */;
INSERT INTO `films` VALUES (1, 112,
                            'In Paris, the aristocratic and intellectual Philippe is a quadriplegic millionaire who is interviewing candidates for the position of his carer, with his red-haired secretary Magalie. Out of the blue, the rude African Driss cuts the line of candidates and brings a document from the Social Security and asks Phillipe to sign it to prove that he is seeking a job position so he can receive his unemployment benefit. Philippe challenges Driss, offering him a trial period of one month to gain experience helping him. Then Driss can decide whether he would like to stay with him or not. Driss accepts the challenge and moves to the mansion, changing the boring life of Phillipe and his employees.',
                            2011, 0, 'Intouchables',
                            'http://res.cloudinary.com/dqiqkrbxp/image/upload/v1473774920/intouchables-2_wglhs4.jpg'),
  (2, 148, 'A thief, who steals corporate secrets through use of dream-sharing technology, is given the inverse task of planting an idea into the mind of a CEO.', 2010, 0, 'Inception', 'http://res.cloudinary.com/dqiqkrbxp/image/upload/v1473774919/inception_gkkicq.jpg'),
  (3, 169, 'A team of explorers travel through a wormhole in space in an attempt to ensure humanity\'s survival.', 2014, 0, 'Interstellar', 'http://res.cloudinary.com/dqiqkrbxp/image/upload/v1473774920/Interstella_fzc6bi.jpg'),
  (4, 108, 'In a city of anthropomorphic animals, a rookie bunny cop and a cynical con artist fox must work together to uncover a conspiracy.', 2016, 0, 'Zootopia', 'http://res.cloudinary.com/dqiqkrbxp/image/upload/v1473774908/ZootopiaIN_wjbvgd.jpg'),
  (5, 98, 'A hapless young Viking who aspires to hunt dragons becomes the unlikely friend of a young dragon himself, and learns there may be more to the creatures than he assumed.', 2010, 0, 'How to Train Your Dragon', 'http://res.cloudinary.com/dqiqkrbxp/image/upload/v1473774922/how-to-train-your-dragon-2-slide_n4xidb.jpg'),
  (6, 165, 'With the help of a German bounty hunter, a freed slave sets out to rescue his wife from a brutal Mississippi plantation owner.', 2012, 0, 'Django Unchained', 'http://res.cloudinary.com/dqiqkrbxp/image/upload/v1473774924/django_unchained_wiais0.jpg'),
  (7, 107, 'A promising young drummer enrolls at a cut-throat music conservatory where his dreams of greatness are mentored by an instructor who will stop at nothing to realize a student\'s potential.', 2013, 0, 'Whiplash', 'http://res.cloudinary.com/dqiqkrbxp/image/upload/v1473774914/whiplash_sddok2.jpg'),
  (8, 146, 'An aspiring author during the civil rights movement of the 1960s decides to write a book detailing the African-American maids\' point of view on the white families for which they work, and the hardships they go through on a daily basis.', 2011, 7.666666666666667, 'The Help', 'http://res.cloudinary.com/dqiqkrbxp/image/upload/v1473774920/The-Help_hjf1u7.jpg'),
  (9, 123, 'The merciless 1970s rivalry between Formula One rivals James Hunt and Niki Lauda.', 2013, 0, 'Rush', 'http://res.cloudinary.com/dqiqkrbxp/image/upload/v1473774918/rush_bx6imd.jpg'),
  (10, 164, 'Eight years after the Joker\'s reign of anarchy, the Dark Knight, with the help of the enigmatic Selina, is forced from his imposed exile to save Gotham City, now on the edge of total annihilation, from the brutal guerrilla terrorist Bane.', 2012, 0, 'The Dark Knight Rises', 'http://res.cloudinary.com/dqiqkrbxp/image/upload/v1473774918/dark_knight_rises_ef5rr4.jpg'),
  (11, 130, 'Two stage magicians engage in competitive one-upmanship in an attempt to create the ultimate stage illusion.', 2006, 0, 'The Prestige', 'http://res.cloudinary.com/dqiqkrbxp/image/upload/v1473774914/the-prestige_bj3p3c.jpg'),
  (12, 135, 'After John Nash, a brilliant but asocial mathematician, accepts secret work in cryptography, his life takes a turn for the nightmarish.', 2001, 9, 'A Beautiful Mind', 'http://res.cloudinary.com/dqiqkrbxp/image/upload/v1473787882/beautiful_mind_hnyuzw.jpg'),
  (13, 201, 'Gandalf and Aragorn lead the World of Men against Sauron\'s army to draw his gaze from Frodo and Sam as they approach Mount Doom with the One Ring.', 2003, 8, 'The Lord of the Rings: The Return of the King', 'http://res.cloudinary.com/dqiqkrbxp/image/upload/v1473774914/screenshot-the-lord-of-the-rings-the-return-of-the-King_durkwy.jpg'),
  (14, 155, 'When a Roman general is betrayed and his family murdered by an emperor\'s corrupt son, he comes to Rome as a gladiator to seek revenge.', 2000, 10, 'Gladiator', 'http://res.cloudinary.com/dqiqkrbxp/image/upload/v1473774920/gladiator_yieoci.jpg'),
  (15, 150, 'A Polish Jewish musician struggles to survive the destruction of the Warsaw ghetto of World War II.', 2002, 0, 'The Pianist', 'http://res.cloudinary.com/dqiqkrbxp/image/upload/v1473774914/Pianist_puthmu.jpg'),
  (16, 141, 'The true story of Frank Abagnale Jr. who, before his 19th birthday, successfully conned millions of dollar\'s worth of checks as a Pan Am pilot, doctor, and legal prosecutor.', 2002, 0, 'Catch Me If You Can', 'http://res.cloudinary.com/dqiqkrbxp/image/upload/v1473774923/catch_me_if_you_can_udset0.jpg'),
  (17, 178, 'A meek Hobbit and eight companions set out on a journey to destroy the One Ring and the Dark Lord Sauron.', 2001, 6, 'The Lord of the Rings: The Fellowship of the Ring', 'http://res.cloudinary.com/dqiqkrbxp/image/upload/v1473774915/lord_of_rings_fellowship_tqakyv.jpg'),
  (18, 151, 'An undercover cop and a mole in the police attempt to identify each other while infiltrating an Irish gang in South Boston.', 2006, 0, 'The Departed', 'http://res.cloudinary.com/dqiqkrbxp/image/upload/v1473774922/departed_xnavyr.jpg'),
  (19, 179, 'While Frodo and Sam edge closer to Mordor with the help of the shifty Gollum, the divided fellowship makes a stand against Sauron\'s new ally, Saruman, and his hordes of Isengard.', 2002, 9, 'The Lord of the Rings: The Two Towers', 'http://res.cloudinary.com/dqiqkrbxp/image/upload/v1473774913/two-towers_rxn8cr.jpg'),
  (20, 104, 'Unscrupulous boxing promoters, violent bookmakers, a Russian gangster, incompetent amateur robbers, and supposedly Jewish jewelers fight to track down a priceless stolen diamond.', 2000, 0, 'Snatch', 'http://res.cloudinary.com/dqiqkrbxp/image/upload/v1473774915/snatch_iqptws.jpg'),
  (24, 117, 'Chris Gardner is a bright and talented, but marginally employed salesman. Struggling to make ends meet, Gardner finds himself and his five-year-old son evicted from their San Francisco apartment with nowhere to go. When Gardner lands an internship at a prestigious stock brokerage firm, he and his son endure many hardships, including living in shelters, in pursuit of his dream of a better life for the two of them.', 2006, 0, 'The Pursuit of Happyness', 'http://res.cloudinary.com/dqiqkrbxp/image/upload/v1473787882/pursuit-of-happyness_rms4nc.png'),
  (28, 131,
   'An insomniac office worker, looking for a way to change his life, crosses paths with a devil-may-care soap maker, forming an underground fight club that evolves into something much, much more.',
   1999, 0, 'Fight Club', 'http://res.cloudinary.com/dqiqkrbxp/image/upload/v1473789122/gresn0omar7zfhoigqaf.jpg'),
  (29, 142,
   'Big-city lawyer Hank Palmer returns to his childhood home where his father, the town\'s judge, is suspected of murder. Hank sets out to discover the truth and, along the way, reconnects with his estranged family.',
   2014, 0, 'The Judge', 'http://res.cloudinary.com/dqiqkrbxp/image/upload/v1473789469/trrl7xzf0owjizcka20v.jpg'),
  (34, 116,
   'Жизни 800 человек общежития висят буквально на волоске из-за безразличия местных властей. В любую секунду здание может рухнуть. И кто бы мог подумать, что судьбы людей окажутся в руках простого сантехника. Но удастся ли ему что-то изменить и предотвратить катастрофу?',
   2014, 0, 'Дурак', 'http://res.cloudinary.com/dqiqkrbxp/image/upload/v1475090967/vijvllecukkypqmxo97m.jpg');
/*!40000 ALTER TABLE `films`
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `films_countries`
--

DROP TABLE IF EXISTS `films_countries`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `films_countries` (
  `film_id`      INT(11) NOT NULL,
  `country_code` CHAR(2) NOT NULL,
  PRIMARY KEY (`film_id`, `country_code`),
  KEY `fk_film_has_country_country1_idx` (`country_code`),
  KEY `fk_film_has_country_film1_idx` (`film_id`),
  CONSTRAINT `fk_film_has_country_country1` FOREIGN KEY (`country_code`) REFERENCES `countries` (`country_code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_film_has_country_film1` FOREIGN KEY (`film_id`) REFERENCES `films` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `films_countries`
--

LOCK TABLES `films_countries` WRITE;
/*!40000 ALTER TABLE `films_countries`
  DISABLE KEYS */;
INSERT INTO `films_countries`
VALUES (1, 'FR'), (2, 'GB'), (2, 'US'), (3, 'GB'), (3, 'US'), (4, 'US'), (5, 'US'), (6, 'US'), (7, 'US'), (8, 'AE'),
  (8, 'IN'), (8, 'US'), (9, 'DE'), (9, 'GB'), (9, 'US'), (10, 'GB'), (10, 'US'), (11, 'GB'), (11, 'US'), (12, 'US'),
  (13, 'NZ'), (13, 'US'), (14, 'GB'), (14, 'US'), (15, 'DE'), (15, 'FR'), (15, 'GB'), (15, 'PL'), (16, 'CA'),
  (16, 'US'), (17, 'NZ'), (17, 'US'), (18, 'HK'), (18, 'US'), (19, 'NZ'), (19, 'US'), (20, 'GB'), (20, 'US'),
  (24, 'US'), (28, 'DE'), (28, 'US'), (29, 'US'), (34, 'RU');
/*!40000 ALTER TABLE `films_countries`
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `films_genres`
--

DROP TABLE IF EXISTS `films_genres`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `films_genres` (
  `film_id`  INT(11) NOT NULL,
  `genre_id` INT(11) NOT NULL,
  PRIMARY KEY (`film_id`, `genre_id`),
  KEY `fk_film_has_genre_genre1_idx` (`genre_id`),
  KEY `fk_film_has_genre_film1_idx` (`film_id`),
  CONSTRAINT `fk_film_has_genre_film1` FOREIGN KEY (`film_id`) REFERENCES `films` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_film_has_genre_genre1` FOREIGN KEY (`genre_id`) REFERENCES `genres` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `films_genres`
--

LOCK TABLES `films_genres` WRITE;
/*!40000 ALTER TABLE `films_genres`
  DISABLE KEYS */;
INSERT INTO `films_genres`
VALUES (2, 2), (10, 2), (14, 2), (1, 3), (4, 3), (5, 3), (6, 3), (20, 3), (16, 4), (18, 4), (20, 4), (28, 4), (29, 4),
  (1, 5), (2, 5), (3, 5), (6, 5), (7, 5), (8, 5), (9, 5), (10, 5), (11, 5), (12, 5), (13, 5), (14, 5), (15, 5), (16, 5),
  (17, 5), (18, 5), (19, 5), (24, 5), (28, 5), (29, 5), (34, 5), (2, 6), (3, 6), (5, 6), (10, 6), (11, 6), (13, 6),
  (17, 6), (19, 6), (15, 7), (4, 10), (2, 20), (10, 20), (11, 20), (18, 20), (28, 20);
/*!40000 ALTER TABLE `films_genres`
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genres`
--

DROP TABLE IF EXISTS `genres`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genres` (
  `id`         INT(11)     NOT NULL AUTO_INCREMENT,
  `genre_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `genre_UNIQUE` (`genre_name`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 23
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genres`
--

LOCK TABLES `genres` WRITE;
/*!40000 ALTER TABLE `genres`
  DISABLE KEYS */;
INSERT INTO `genres`
VALUES (1, 'Absurdist'), (2, 'Action'), (3, 'Comedy'), (4, 'Crime'), (5, 'Drama'), (6, 'Fantasy'), (7, 'Historical'),
  (8, 'Historical fiction'), (9, 'Horror'), (10, 'Magical realism'), (11, 'Mystery'), (12, 'Paranoid'),
  (13, 'Philosophical'), (14, 'Political'), (15, 'Romance'), (16, 'Saga'), (17, 'Satire'), (18, 'Science fiction'),
  (19, 'Speculative'), (20, 'Thriller'), (21, 'Urban'), (22, 'Western');
/*!40000 ALTER TABLE `genres`
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marks`
--

DROP TABLE IF EXISTS `marks`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marks` (
  `film_id` INT(11)          NOT NULL,
  `user_id` INT(11)          NOT NULL,
  `mark`    INT(10) UNSIGNED NOT NULL,
  `date`    DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`film_id`, `user_id`),
  KEY `fk_watch_list_film1_idx` (`film_id`),
  KEY `fk_watch_list_user1_idx` (`user_id`),
  CONSTRAINT `fk_watch_list_film1` FOREIGN KEY (`film_id`) REFERENCES `films` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_watch_list_user1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marks`
--

LOCK TABLES `marks` WRITE;
/*!40000 ALTER TABLE `marks`
  DISABLE KEYS */;
INSERT INTO `marks`
VALUES (8, 11, 6, '2016-07-24 21:31:32'), (8, 31, 10, '2016-09-19 13:46:55'), (8, 32, 7, '2016-09-28 20:35:56'),
  (12, 25, 9, '2016-07-24 21:31:32'), (13, 13, 8, '2016-07-24 21:31:32'), (14, 5, 10, '2016-07-24 21:31:32'),
  (17, 12, 6, '2016-07-24 21:31:32'), (19, 14, 9, '2016-07-24 21:31:32');
/*!40000 ALTER TABLE `marks`
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id`              INT(11)     NOT NULL                    AUTO_INCREMENT,
  `username`        VARCHAR(16) NOT NULL,
  `password`        VARCHAR(64) NOT NULL,
  `is_admin`        TINYINT(1)  NOT NULL,
  `status`          ENUM ('AMATEUR', 'EXPERIENCED', 'GURU') DEFAULT NULL,
  `expiration_date` DATETIME                                DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 47
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users`
  DISABLE KEYS */;
INSERT INTO `users` VALUES (1, 'admin', 'c46a5b06de4a4fc83f2a1da157d1de8b', 1, 'GURU', NULL),
  (2, 'aerhaerh', '89656134d8d8acecc933a8ff70c17927', 0, 'AMATEUR', '2016-10-05 18:15:04'),
  (3, 'loris', 'e53896da202b1031a05e2bfb562055f3', 0, 'EXPERIENCED', NULL),
  (4, 'nathaniel', '7393fe18730e63b46c20876399513039', 0, 'EXPERIENCED', NULL),
  (5, 'mamadou', '01c9c53d3e87b08e157df68cc4c16913', 0, 'EXPERIENCED', NULL),
  (6, 'georginho', '78d76654fedd3f5de21bbabeadcc5e98', 0, 'EXPERIENCED', NULL),
  (7, 'dejan', '812ee18059d9b8e768db263e6051f569', 0, 'AMATEUR', NULL),
  (8, 'james', 'e9cd8043eb6780a7f4bce9336ce1caf2', 0, 'EXPERIENCED', NULL),
  (9, 'phlillipe', 'a8cee1e3ed36f3fba62863361530fd28', 0, 'AMATEUR', NULL),
  (10, 'roberto', '592f2d9b3b51b287bc042d6d0bcaf5ef', 0, 'AMATEUR', NULL),
  (11, 'joe-gomez', 'f6587c00baa091e20dbfc8ae7622f145', 0, 'AMATEUR', NULL),
  (12, 'alex', '207e97a3a388602d28923a3b49feaf7b', 0, 'EXPERIENCED', NULL),
  (13, 'jordan', '207e97a3a388602d28923a3b49feaf7b', 0, 'AMATEUR', NULL),
  (14, 'daniel', '03d19af4af5463d8c5587869a635a6fe', 0, 'AMATEUR', NULL),
  (15, 'marko', 'ca573fc020c6ef3dba68253716e6d05d', 0, 'AMATEUR', NULL),
  (16, 'ragnar', '1a25d83be5a3c2e66b367b38181849c3', 0, 'EXPERIENCED', NULL),
  (17, 'alberto', '90b0175e2be204df3c63a5a3471ca0f9', 0, 'AMATEUR', NULL),
  (18, 'sadio', '979c4fb43b4e660673ebdf31ba969112', 0, 'AMATEUR', NULL),
  (19, 'adam', '10df547be2fe0581cc14b837155844f7', 0, 'AMATEUR', NULL),
  (20, 'simon', '6b8c459dd513e5a46dc62d3869de83c4', 0, 'AMATEUR', NULL),
  (21, 'emre', 'f9e94bf850727b698cd6155d0b2a3866', 0, 'AMATEUR', NULL),
  (22, 'joe-allen', '64c071f115b3d650f0be3f195c6b7ae3', 0, 'AMATEUR', NULL),
  (23, 'cameron', '17d359202c7415de61a958d40a57a6b0', 0, 'AMATEUR', NULL),
  (24, 'divock', '780ecf8c6408b8367e3665ffedfd0aad', 0, 'AMATEUR', NULL),
  (25, 'danny', 'cadac1c76dee5743c05ad7b230325d42', 0, 'AMATEUR', NULL),
  (26, 'joel', '1fb5fa697c51c03c03571cdf72b22987', 0, 'AMATEUR', NULL),
  (27, 'steven', 'b246839187a6d9d8815d82b12b87582e', 0, 'AMATEUR', NULL),
  (28, 'admin2', '621c930f5e5610d03d023f01ffab442c', 1, 'GURU', NULL),
  (29, 'kevin', 'e3ceb591963fc8167b3e5b93b21fef1b', 0, 'AMATEUR', NULL),
  (30, 'xrhse', '357F8F74280D3F937C1B0399C92A31F90047CB8C984ACF67CEB581DC75D4DCA7', 1, 'GURU', NULL),
  (31, 'anton', 'E0B003F0E010D4C9C7D8899BD566E3239D54E649EA924AEFA52578B53145D957', 1, 'GURU', NULL),
  (32, 'user1', 'DB956359AC0868D28619FA544645F1D3C720A42E65988FF865F6EB1F9AFB02B9', 0, 'GURU', NULL),
  (33, 'veronica', '79442BE352D3E416FA2A4B6171FE2CB5FDDD0E5B3E59141E82984FF976575AC0', 0, 'GURU', NULL),
  (45, 'user7777', '1D231DCE0DF784541BCAE8821D81789CF7D1CF38478060D4241CB8E89DB47A51', 0, 'AMATEUR', NULL),
  (46, 'антон', '1D231DCE0DF784541BCAE8821D81789CF7D1CF38478060D4241CB8E89DB47A51', 0, 'AMATEUR', NULL);
/*!40000 ALTER TABLE `users`
  ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2016-09-29 23:26:37
